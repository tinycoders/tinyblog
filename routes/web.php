<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//admin routes
Route::view("/admin/login", "layouts.login");
Route::view("/admin/{path?}", "layouts.admin")->where("path", "[a-zA-Z@0-9/?=-]+");

//Important Routes
Auth::routes(['verify' => true]);

//Website Routes
Route::get("/", "HomeController@index")->name('home');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/pro-search', 'HomeController@proSearch')->name('pro_search');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'ContactController@getContact')->name('contactView');
Route::post('/contact', 'ContactController@saveContact')->name('contact');;
Route::post('/lang', 'HomeController@lang')->name('lang');

//Post Routes
Route::get('/post/{slug}', 'ArticleController@show')->name('post');

//Category Routes
Route::get('/categories', 'CategoryController@index')->name('categories');
Route::get('/category/{slug}', 'CategoryController@getArticlesCategory')->name('category');


//User Routes
Route::get("/profile", "UserController@show")->name('show_profile');
Route::put("/profile", "UserController@update")->name('update_profile');
Route::get("/favorite", "UserController@showFavorite")->name('show_favorite');
Route::post("/favorite", "UserController@addFavorite")->name('add_favorite');
Route::delete("/favorite", "UserController@deleteFavorite")->name('delete_favorite');
Route::get("/transaction", "UserController@showTransactions")->name('my_transactions');


//Comment Routes
Route::post("/comment", "UserController@addComment")->name('add_comment');


//Payment Routes
Route::get('/payment/{slug}/request', 'Payment\ZarinPalController@request')->name('request_payment');
Route::any('/payment/verify', 'Payment\ZarinPalController@verify')->name('verify_payment');
Route::get('/post/{slug}/pdf','ArticleController@downloadPDF')->name('post.pdf');
Route::get('/payment/result', 'Payment\ZarinPalController@showResult')->name('payment_result');


Route::get('/test', function (){
    return view('vendor.shetabitPayment.redirectForm');
});


