<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//editor URL
Route::middleware("adminApi")->get("/images", "Api\UploadController@gatAllImages");
Route::middleware("adminApi")->delete("/image/delete", "Api\UploadController@deleteImage");
Route::middleware("adminApi")->post("/file/upload", "Api\UploadController@uploadFile");
Route::middleware("adminApi")->post("/image/upload", "Api\UploadController@uploadImage");
Route::middleware("adminApi")->post("/video/upload", "Api\UploadController@uploadVideo");
