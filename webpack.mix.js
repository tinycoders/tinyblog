const mix = require("laravel-mix");
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react("resources/react/admin/app.js", "public/js/admin.js").sass(
    "resources/sass/admin/app.scss",
    "public/css/admin.css"
);

mix.sass("resources/sass/admin/login.scss", "public/css");
mix.js(
    ["resources/js/particles.min.js", "resources/js/login.js"],
    "public/js/login.js"
);

mix.scripts(
    [
        "resources/js/jquery.min.js",
        "resources/js/popper.min.js",
        "resources/js/bootstrap.min.js",
        "resources/js/mdb.min.js"
    ],
    "public/js/lib.js"
);

mix.sass("resources/sass/main/app.scss", "public/css");

mix.scripts(
    [
        "resources/js/jquery.min.js",
        "resources/js/popper.min.js",
        "resources/js/bootstrap.min.js",
        "resources/js/fontawesome.min.js"
    ],
    "public/js/app.js"
);

mix.scripts(
    ["resources/js/mdb.min.js", "resources/js/scripts.js"],
    "public/js/app2.js"
);

if (mix.inProduction()) {
    mix.version();
}
