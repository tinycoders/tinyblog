<?php

return [
    'mode' => 'utf-8',
    'format' => 'A4',
    //'format' => 'A4-L',
    //'orientation' => 'L',
    'default_font' => 'vazir',
    'author' => 'Tiny Blog',
    'subject' => '',
    'keywords' => '',
    'creator' => 'Tiny Blog',
    'display_mode' => 'fullpage',
    'tempDir' => public_path('uploads/pdf'),

    'font_path' => base_path('public/fonts/'),
    'font_data' => [
        'vazir' => [
            'R' => 'Vazir-FD.ttf',    // regular font
            //'B'  => '',       // optional: bold font
            //'I'  => '',     // optional: italic font
            //'BI' => '', // optional: bold-italic font
            'useOTL' => 0xFF,    // required for complicated langs like Persian, Arabic and Chinese
            'useKashida' => 75,  // required for complicated langs like Persian, Arabic and Chinese
        ]
    ]
];
