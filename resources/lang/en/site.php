<?php

return [


    'header' => [
        'brand' => 'Tiny Blog',
        'home' => 'Home',
        'categories' => 'Categories',
        'about_us' => 'About us',
        'contact_us' => 'Contact us',
        'search' => 'Search',
        'profile' => 'Profile',
        'my_account' => 'My Account',
        'my_favorite' => 'My Favorite',
        'logout' => 'Log out',
        'my_transactions' => 'My Transactions',
    ],

    'intro' => [
        'brand' => 'Tiny Blog',
        'message_favorite' => 'TinyBlog, your scientific and educational journal',
        'profile' => 'Profile',
        'register' => 'Register',
        'my_favorite' => 'My Favorite',
        'login' => 'Login',
    ],

    'sidebar' => [
        'categories' => 'Categories',
        'popular_articles' => 'Popular Articles',
        'top_views_articles' => 'Top View Articles',
    ],

    'post' => [
        'read_more' => 'Read More',
        'remove' => 'Remove',
    ],

    'about' => [
        'about_us' => 'About us',
        'about_us_text' => 'About Us Text',
    ],

    'contact' => [
        'title' => 'Send me your message',
        'name' => 'Name',
        'email' => 'Email',
        'phone' => 'Phone',
        'message_desc' => 'Write something here...',
        'subject' => 'Subject',
        'submit' => 'Submit',
        'send_success' => 'Your message has been successfully send',
    ],

    'footer' => [
        'language' => 'Language',
        'about_us' => 'About us',
        'contact_us' => 'Contact us',
        'copyright' => 'Copyright',
        'brand' => 'Tiny Blog',
        'social_networks_message' => 'Get connected with us on social networks!',
        'tehran_iran' => 'Tehran, Iran',
        'payment' => 'Payment gateway',
    ],

    'profile' => [
        'profile_information' => 'Profile Information:',
        'submit' => 'Submit',
        'gender' => 'Gender:',
        'male' => 'Male',
        'female' => 'Female',
        'other' => 'Other',
        'phone_number' => 'Phone Number',
        'education' => 'Education',
        'date_birth' => 'Date Of Birth',
        'name' => 'Name',
        'update_success' => 'Information was successfully edited.',
        'done' => 'Done',
        'error' => 'Error!'
    ],

    'favorite' => [
        'favorite_list' => 'Favorite List:',
        'delete_success' => 'Your favorite article has been successfully delete',
    ],

    'transaction' => [
        'transaction_list' => 'Transaction List:',
        'article_name' => 'Article Name',
        'amount' => 'Total Price (Toman)',
        'status' => 'Status',
        'transaction_id' => 'Transaction ID',
        'ref_id' => 'Tracking ID',
        'gate' => 'Gate',
        'date' => 'Date & Time',
        'ok' => 'Successful',
        'nok' => 'Unsuccessful',
        'article_show_link' => 'View article link',
    ],

    'search' => [
        'search_result_list' => 'Search Result List',
        'read_more' => 'Read More',
    ],

    'categories' => [
        'categories_list' => 'Categories List',
    ],

    'category' => [
        'category_list' => 'Category List',
        'read_more' => 'Read More',
        'category_not_found' => 'Category Not Found',
    ],

    'post_content' => [
        'toolbox' => 'Toolbox',
        'favorite' => 'Favorite',
        'download_article' => 'Download Article',
    ],

    'comment_list' => [
        'comments' => 'Comments',
    ],

    'comment_form' => [
        'leave_a_reply' => 'Leave a reply',
        'write_something_here' => 'Write something here...',
        'submit' => 'Submit',
        'register' => 'Register',
        'login' => 'Login',
        'add_favorite_status' => 'Your comment has been successfully send'
    ],

    'login' => [
        'login' => 'Login',
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'remember_me' => 'Remember Me',
        'forget_password' => 'Forgot Your Password?',
    ],

    'register' => [
        'register' => 'Register',
        'name' => 'Name',
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
    ],

    'verify' => [
        'title' => 'Verify Your Email Address',
        'message' => 'A fresh verification link has been sent to your email address.',
        'hint_1' => 'Before proceeding, please check your email for a verification link.',
        'hint_2' => 'If you did not receive the email',
        'message_submit' => 'click here to request another',
    ],

    'password_reset_email' => [
        'reset_password' => 'Reset Password',
        'email' => 'E-Mail Address',
        'send_pass_reset_link' => 'Send Password Reset Link',
    ],

    'password_reset_confirm' => [
        'reset_password' => 'Reset Password',
        'confirm_password_hint' => 'Please confirm your password before continuing.',
        'password' => 'Password',
        'forget_password' => 'Forgot Your Password?',
    ],

    'password_reset' => [
        'confirm_password' => 'Confirm Password',
        'reset_password' => 'Reset Password',
        'email' => 'E-Mail Address',
        'password' => 'Password',
    ],

    'email_views' => [
        'title' => 'Title:',
        'message' => 'Message:',
        'name' => 'Name:',
        'email' => 'Email:',
        'phone' => 'Phone:',
        'subject' => 'Subject:',
        'hello_admin' => 'Hello Admin,',
        'hello_admin_message' => 'You received an email from:',
        'thank_you' => 'Thank You',

    ],
];
