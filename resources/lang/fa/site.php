<?php

return [

    'header' => [
        'brand' => 'تاینی بلاگ',
        'home' => 'خانه',
        'categories' => 'دسته بندی ها',
        'about_us' => 'درباره ما',
        'contact_us' => 'تماس با ما',
        'search' => 'جستجو',
        'profile' => 'پروفایل',
        'my_account' => 'حساب من',
        'my_favorite' => 'مورد علاقه من',
        'logout' => 'خروج',
        'my_transactions' => 'تراکنش های من',
    ],

    'intro' => [
        'brand' => 'تاینی بلاگ',
        'message_favorite' => 'تاینی بلاگ ، مجله علمی و آموزشی شما',
        'profile' => 'پروفایل',
        'register' => 'ثبت نام',
        'login' => 'ورود',
        'my_favorite' => 'مورد علاقه من',
    ],

    'sidebar' => [
        'categories' => 'دسته بندی ها',
        'popular_articles' => 'مقاله های محبوب',
        'top_views_articles' => 'مقاله های پربازدید',
    ],

    'post' => [
        'read_more' => 'بیشتر بخوانید',
        'remove' => 'حذف',
    ],

    'about' => [
        'about_us' => 'درباره ما',
        'about_us_text'=>'متن درباره ما'
    ],

    'contact' => [
        'title' => 'پیامتو برای من ارسال کن',
        'name' => 'نام',
        'email' => 'ایمیل',
        'phone' => 'موبایل',
        'message_desc' => '...یه چیزی این جا بنویس',
        'subject' => 'عنوان',
        'submit' => 'ثبت',
        'send_success' => 'پیام شما با موفقیت ارسال شد.',
    ],

    'footer' => [
        'language' => 'زبان',
        'about_us' => 'درباره ما',
        'contact_us' => 'تماس با ما',
        'copyright' => 'کپی رایت',
        'brand' => 'تاینی بلاگ',
        'social_networks_message' => '!با ما در شبکه ها اجتماعی در ارتباط باشید',
        'tehran_iran' => 'ایران، تهران',
        'payment' => 'درگاه پرداخت',
    ],

    'profile' => [
        'profile_information' => 'اطلاعات پروفایل:',
        'submit' => 'ثبت',
        'gender' => 'جنسیت:',
        'male' => 'مرد',
        'female' => 'زن',
        'other' => 'دیگر',
        'phone_number' => 'شماره تلفن',
        'education' => 'تحصیلات',
        'date_birth' => 'تاریخ تولد',
        'name' => 'نام',
        'update_success' => '.اطلاعات با موفقیت ویرایش شد',
        'done' => 'انجام شد',
        'error' => 'خطا!'
    ],

    'favorite' => [
        'favorite_list' => 'لیست مورد علاقه',
        'delete_success' => 'مقاله مورد علاقه شما با موفقیت حذف شد',
    ],

    'transaction' => [
        'transaction_list' => ':لیست تراکنش ها',
        'article_name' => 'نام مقاله',
        'amount' => 'قیمت کل (تومان)',
        'status' => 'وضعیت',
        'transaction_id' => 'شناسه تراکنش',
        'ref_id' => 'شناسه پیگیری',
        'gate' => 'درگاه',
        'date' => 'تاریخ و زمان',
        'ok' => 'موفق',
        'nok' => 'ناموفق',
        'article_show_link' => 'لینک مشاهده مقاله',
    ],

    'search' => [
        'search_result_list' => 'لیست نتایج جستجو',
        'read_more' => 'بیشتر بخوانید',
    ],

    'categories' => [
        'categories_list' => 'لیست دسته بندی ها',
    ],

    'category' => [
        'category_list' => 'لیست دسته بندی',
        'read_more' => 'بیشتر بخوانید',
        'category_not_found' => 'دسته بندی پیدا نشد',
    ],

    'post_content' => [
        'toolbox' => 'جعبه ابزار',
        'favorite' => 'مورد علاقه',
        'download_article' => 'دانلود مقاله',
    ],

    'comment_list' => [
        'comments' => 'نظر',
    ],

    'comment_form' => [
        'leave_a_reply' => 'یه پاسخ بده',
        'write_something_here' => 'یه چیزی این جا بنویس ...',
        'submit' => 'ثبت',
        'register' => 'ثبت نام',
        'login' => 'ورود',
        'add_favorite_status' => 'نظر شما با موفقیت ارسال شد'
    ],

    'login' => [
        'login' => 'ورود',
        'email' => 'آدرس ایمیل',
        'password' => 'رمز عبور',
        'remember_me' => 'مرا به خاطر بسپار',
        'forget_password' => 'رمز عبور خود را فراموش کردید؟',
    ],

    'register' => [
        'register' => 'ثبت نام',
        'name' => 'نام',
        'email' => 'آدرس ایمیل',
        'password' => 'رمز عبور',
        'confirm_password' => 'تایید رمز عبور',
    ],

    'verify' => [
        'title' => 'آدرس ایمیل خود را تایید کنید',
        'message' => 'یک لینک تازه تایید به ایمیل شما ارسال شد.',
        'hint_1' => 'قبل از انجام مراحل، لطفا ایمیل خود را برای یک لینک تایید بررسی نمایید.',
        'hint_2' => 'اگر شما ایمیلی دریافت نکردید',
        'message_submit' => 'این جا را کلیک کنید تا یکی دیگر درخواست شود',
    ],

    'password_reset_email' => [
        'reset_password' => 'بازگردانی رمز عبور',
        'email' => 'آدرس ایمیل',
        'send_pass_reset_link' => 'ارسال لینک بازگردانی رمز عبور',
    ],

    'password_reset_confirm' => [
        'confirm_password' => 'تایید رمز عبور',
        'confirm_password_hint' => 'لطفا قبل از ادامه دادن رمز عبور خود را تایید کنید.',
        'password' => 'رمز عبور',
        'forget_password' => 'رمز عبور خود را فراموش کردید؟',
    ],

    'password_reset' => [
        'confirm_password' => 'تایید رمز عبور',
        'reset_password' => 'بازگردانی رمز عبور',
        'email' => 'آدرس ایمیل',
        'password' => 'رمز عبور',
    ],

    'email_views' => [
        'title' => 'عنوان:',
        'message' => 'پیام:',
        'name' => 'نام و نام خانوادگی:',
        'email' => 'ایمیل:',
        'phone' => 'شماره تماس:',
        'subject' => 'عنوان:',
        'hello_admin' => 'سلام مدیر،',
        'hello_admin_message' => 'شما یک ایمیل دریافت کردید از:',
        'thank_you' => 'متشکرم',
    ],

];
