@extends('errors::minimal')

@section('title','419 - Page Expired')
@section('message-brand','Oops!')
@section('message-title','419 - Page Expired')
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
