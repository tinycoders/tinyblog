@extends('errors::minimal')

@section('title','500 - Server Error')
@section('message-brand','Oops!')
@section('message-title','500 - Server Error')
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
