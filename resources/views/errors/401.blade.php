@extends('errors::minimal')


@section('title','401 - Unauthorized')
@section('message-brand','Oops!')
@section('message-title','401 - Unauthorized')
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
