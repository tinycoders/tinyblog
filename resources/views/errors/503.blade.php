@extends('errors::minimal')


@section('title','503 - Service Unavailable')
@section('message-brand','Oops!')
@section('message-title','503 - Service Unavailable')
@section('message', __($exception->getMessage() ?: ''))
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
