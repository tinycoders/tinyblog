@extends('errors::minimal')

@section('title','429 - Too Many Requests')
@section('message-brand','Oops!')
@section('message-title','429 - Too Many Requests')
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
