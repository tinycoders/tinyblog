@extends('errors::minimal')

@section('title','404 - Page not found')
@section('message-brand','Oops!')
@section('message-title','404 - Page not found')
@section('message-content','The page you are looking for might have been removed had its name changed or is temporarily unavailable.')
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
