@extends('errors::minimal')

@section('title','403 - Forbidden')
@section('message-brand','Oops!')
@section('message-title','403 - Forbidden')
@section('message-content',__($exception->getMessage() ?: ''))
@section('message-button')
    <a href="{{route('home')}}">Go To Homepage</a>
@endsection
