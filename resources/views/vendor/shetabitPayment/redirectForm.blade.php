@extends('web.layouts.master')


@section('title', 'Tiny Blog - Payment Redirect')



@section('styles')

    <style>
        /*        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');*/


        * {
            box-sizing: border-box;
        }

        body {
            background: #f6f5f7;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            height: 100vh;
            margin: -20px 0 50px;
        }


        h1 {
            font-weight: bold;
            margin: 0;
        }

        h2 {
            text-align: center;
        }

        p {
            font-size: 14px;
            font-weight: 100;
            line-height: 20px;
            letter-spacing: 0.5px;
            margin: 20px 0 30px;
        }

        span {
            font-size: 12px;
        }

        a {
            color: #333;
            font-size: 14px;
            text-decoration: none;
            margin: 15px 0;
        }

        button {
            border-radius: 20px;
            border: 1px solid #FF4B2B;
            background-color: #FF4B2B;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: bold;
            padding: 12px 45px;
            letter-spacing: 1px;
            text-transform: uppercase;
            transition: transform 80ms ease-in;
        }

        button:active {
            transform: scale(0.95);
        }

        button:focus {
            outline: none;
        }

        button.ghost {
            background-color: transparent;
            border-color: #FFFFFF;
        }

        input {
            background-color: #eee;
            border: none;
            padding: 12px 15px;
            margin: 8px 0;
            width: 100%;
        }

        .container {
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5) !important;
            overflow: hidden;
            padding: 0px !important;
            transition: all 0.3s ease-in-out;
        }

        .darkmode--activated .container:hover {
            transform: scale(1.01);
            z-index: 2;
        }

        .container:hover {

            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25) !important;
            transform: scale(1.01);
            z-index: 2;
        }

        .darkmode--activated span {
            color: black !important;
            background-color: #FFFFFF !important;
        }

        .darkmode--activated p {
            color: white !important;
        }

        .text-center {
            text-align: center;
        }

        .mt-2 {
            margin-top: 2em;
        }
    </style>

@endsection


@section('content')

    <div class="container h-100 d-flex justify-content-center">
        <div class="row jumbotron my-auto text-center px-5 " style="color:white; background: #ff416c;background: -webkit-linear-gradient(to right, #ff416c, #ff4b2b);background: linear-gradient(to right, #ff416c, #ff4b2b);">

            <div class="col">

                <form class="text-center mt-2" method="{{ $method }}" action="{{ $action }}">
                    <h1 class="mb-5">تاینی بلاگ</h1>
                    <h3>انتقال به درگاه پرداخت</h3>
                    <div class="fa-6x mt-3 mb-3">
                        <i class="fas fa-sync fa-spin"></i>
                        <h6 class="pr-3 pl-3"> در حال انتقال به درگاه پرداخت در
                              <span style="font-size: 16px;" id="countdown">10</span>
                            ثانیه
                        </h6>
                    </div>

                     @foreach($inputs as $name => $value)
                        <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                         <input type="hidden" name="" value="">
                     @endforeach

                    <button class="btn btn-rounded waves-effect waves-light"  style="background-color: #2AF598FF; font-weight: bold !important; font-size: medium !important;" type="submit">رفتن به درگاه</button>
                 </form>

                 <script>
                     // Total seconds to wait
                     var seconds = 10;

                     function submitForm() {
                         document.forms[0].submit();
                     }

                     function countdown() {
                         seconds = seconds - 1;
                         if (seconds <= 0) {
                             // submit the form
                             submitForm();
                         } else {
                             // Update remaining seconds
                             document.getElementById("countdown").innerHTML = seconds;
                             // Count down using javascript
                             window.setTimeout("countdown()", 1000);
                         }
                     }

                     // Run countdown function
                     countdown();
                 </script>

            </div>
        </div>

    </div>


@endsection
