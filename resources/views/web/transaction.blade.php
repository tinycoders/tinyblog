@extends('web.layouts.master')

@section('title', 'Tiny Blog - Transaction Page')

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection


@section('content')
    <!-- Blog section -->
    <section>

        <div class="container mt-5 pt-3">

            @if (app()->getLocale()=='en')

                <h1 class="title text-left"
                    style="font-size: x-large;">@lang('site.transaction.transaction_list')</h1>

            @elseif(app()->getLocale()=='fa')
                <h1 class="title text-right"
                    style="font-size: x-large;">@lang('site.transaction.transaction_list')</h1>

            @endif


            <hr class="my-5 hr_darkmode_handler">

            <!-- Blog -->
            <div class="row mt-5 pt-3">

                <div class="col-lg-9 col-12 mt-1">


                    @if (app()->getLocale()=='en')

                        <table class="table table-striped table-hover table-dark text-center"
                               style="direction: ltr; font-family: Helvetica, Arial, sans-serif">

                            @elseif(app()->getLocale()=='fa')

                                <table class="table table-striped table-hover table-dark text-center"
                                       style="direction: rtl">

                                    @endif

                                    <thead class="bg-default">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">@lang('site.transaction.article_name')</th>
                                        <th scope="col">@lang('site.transaction.amount')</th>
                                        <th scope="col">@lang('site.transaction.status')</th>
                                        <th scope="col">@lang('site.transaction.transaction_id')</th>
                                        <th scope="col">@lang('site.transaction.ref_id')</th>
                                        <th scope="col">@lang('site.transaction.gate')</th>
                                        <th scope="col">@lang('site.transaction.date')</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    @if (isset($transactions))

                                        @foreach($transactions as $transaction)




                                            <tr>
                                                <th scope="row">{{(($transactions->currentPage()-1)*$transactions->perpage())+$loop->index+1}}</th>
                                                <td><a class="text-primary"
                                                       href="{{route('post',[mb_strtolower(\App\Article::where('id',$transaction->article_id)->first()->slug,'UTF-8')])}}">@lang('site.transaction.article_show_link')</a>
                                                </td>
                                                <td>{{number_format($transaction->amount)}}</td>
                                                @if ($transaction->status == 1)
                                                    <td class="bg-success">@lang('site.transaction.ok')</td>
                                                @elseif($transaction->status == 0)
                                                    <td class="bg-danger">@lang('site.transaction.nok')</td>
                                                @endif
                                                <td>{{ltrim(ltrim($transaction->transaction_id,"A"),"0")}}</td>
                                                <td>{{(is_null($transaction->ref_id))?'-':$transaction->ref_id}}</td>
                                                <td>{{(is_null($transaction->driver))?'-':$transaction->driver}}</td>
                                                @if (app()->getLocale()=='en')

                                                    <td>{{$transaction->created_at_en}}</td>

                                                @elseif(app()->getLocale()=='fa')

                                                    <td>{{$transaction->created_at}}</td>

                                                @endif
                                            </tr>
                                        @endforeach


                                    @endif


                                    </tbody>
                                </table>

                            @if (isset($transactions))
                                <!-- Pagination dark grey -->
                                    <nav class="mb-5 pb-2 justify-content-center d-flex">
                                        {{$transactions->links()}}
                                    </nav>
                                    <!-- Pagination dark grey -->
                    @endif

                </div>

                <!-- Sidebar -->
                <div class="col-lg-3 col-12 mt-1">
                    @include('web.layouts.sidebar')
                </div>
                <!-- Sidebar -->
            </div>
            <!-- Blog -->

        </div>
        <!-- Main listing -->


    </section>
    <!-- Blog section -->

@endsection



@section('footer')
    @include('web.layouts.footer')
@endsection
