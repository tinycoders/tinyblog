
    @if (app()->getLocale()=='fa')
        <style>
            .modal{
                direction: rtl !important;
            }

            .modal-header > button {
                margin-left: 0px !important;
            }

            .modal-body {
                justify-content: flex-start !important;
            }

            .md-form > label {
                display: flex !important;
                left: unset !important;
                right: 0px !important;
            }

            .form-check{
                display: flex;
                margin-bottom: 1rem ;
            }

            .custom-select {
                font-family: Vazir , "B Yekan";
                margin-bottom: 1.5rem;
            }
        </style>
    @endif

    <style>
        .custom-select {
            font-family: unset;
            margin-bottom: 1.5rem;
        }

        .modal-body {
            padding: 20px;
            padding-top: 0px !important;
        }


    </style>

<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">

    <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
    <div class="modal-dialog modal-dialog-centered" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">جستجوی پیشرفته</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <!-- Material form group -->

                <form action="{{route('pro_search')}}" method="get" class="needs-validation" novalidate >
                    <div class="md-form form-group">
                        <input name="article_title" type="text" id="article_title" class="form-control">
                        <label for="article_title">عنوان مقاله</label>
                        <div class="invalid-feedback">
                          حداقل و حداکثر 5 - 20
                        </div>
                    </div>

                    <select name="article_type" class="browser-default custom-select">
                        <option value=""  >نوع قیمت مقاله</option>
                        <option value="all">همه</option>
                        <option value="free">رایگان</option>
                        <option value="pro">پولی</option>

                    </select>

                    <hr>
                    @php
                        $categories = \App\Category::get();
                    @endphp

                    <select name="article_category" class="browser-default custom-select">
                        <option value="" >دسته بندی</option>
                        @if (app()->getLocale()=='fa')
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name_fa}}</option>
                            @endforeach
                        @elseif (app()->getLocale()=='en')
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        @endif
                    </select>

                    <hr>

                    <select name="article_view" class="browser-default custom-select ">
                        <option value="" disabled >مرتب سازی بر اساس</option>
                        <option value="new">جدید ترین ها</option>
                        <option value="old">قدیمی ترین ها</option>
                        <option value="view">پر بازدید ترین ها</option>

                    </select>

                    <button  class="btn btn-primary btn-block " type="submit">جستجو</button>
                </form>
                    <!-- Material form group -->
            </div>

        </div>
    </div>
</div>


<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).ready(function() {
        $('.mdb-select').materialSelect();                                        // Initialize material styling for select dropdown
        $('.datepicker').pickadate();                                             // Initialize datepicker
        $('.timepicker').pickatime({ twelvehour: true,});
        // Initialize timepicker

        $('.datepicker').removeAttr('readonly');
        $('.mdb-select.select-wrapper .select-dropdown').val("").removeAttr('readonly').attr("placeholder", "Choose your country").prop('required', true).addClass('form-control');

    });
</script>

