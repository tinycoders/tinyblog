@extends('web.layouts.master')

@section('title', 'Tiny Blog - Favorite Page')

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection


@section('content')
    <!-- Blog section -->
    <section>

        <div class="container mt-5 pt-3">

            @if (app()->getLocale()=='en')

                <h1 class="title text-left"
                    style="font-size: x-large;">@lang('site.favorite.favorite_list')</h1>

            @elseif(app()->getLocale()=='fa')
                <h1 class="title text-right"
                    style="font-size: x-large;">@lang('site.favorite.favorite_list')</h1>

            @endif

            <hr class="my-5 hr_darkmode_handler">

            <!-- Blog -->
            <div class="row mt-5 pt-3">


                @include('web.layouts.post')

                <!-- Sidebar -->
                <div class="col-lg-3 col-12 mt-1">
                    @include('web.layouts.sidebar')
                </div>
                <!-- Sidebar -->
            </div>
            <!-- Blog -->

        </div>
        <!-- Main listing -->


    </section>
    <!-- Blog section -->

@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection

