@extends('web.layouts.master')


@section('title', 'Tiny Blog - Payment Result')

@section('styles')

    <style>
        /*        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');*/

        * {
            box-sizing: border-box;
        }

        body {
            background: #f6f5f7;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            height: 100vh;
            margin: -20px 0 50px;
        }


        h1 {
            font-weight: bold;
            margin: 0;
        }

        h2 {
            text-align: center;
        }

        p {
            font-size: 14px;
            font-weight: 100;
            line-height: 20px;
            letter-spacing: 0.5px;
            margin: 20px 0 30px;
        }

        span {
            font-size: 12px;
        }

        a {
            color: #333;
            font-size: 14px;
            text-decoration: none;
            margin: 15px 0;
        }

        button {
            border-radius: 20px;
            border: 1px solid #FF4B2B;
            background-color: #FF4B2B;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: bold;
            padding: 12px 45px;
            letter-spacing: 1px;
            text-transform: uppercase;
            transition: transform 80ms ease-in;
        }

        button:active {
            transform: scale(0.95);
        }

        button:focus {
            outline: none;
        }

        button.ghost {
            background-color: transparent;
            border-color: #FFFFFF;
        }

        input {
            background-color: #eee;
            border: none;
            padding: 12px 15px;
            margin: 8px 0;
            width: 100%;
        }

        .container {
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5) !important;
            position: relative;
            overflow: hidden;
            width: 768px;
            max-width: 100%;
            min-height: 350px;
            padding: 0px !important;
            transition: all 0.3s ease-in-out;
        }

        .darkmode--activated .container:hover {
            transform: scale(1.01);
            z-index: 2;
        }

        .container:hover {

            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25) !important;
            transform: scale(1.01);
            z-index: 2;
        }

        .content {
            left: 0;
            width: 50%;
            z-index: 2;
        }

        .sidebar-container {
            position: absolute;
            top: 0;
            right: -50%;
            width: 50%;
            height: 100%;
            z-index: 100;
            padding: 0px !important;
        }

        .sidebar {
            background: #FF416C;
            background: -webkit-linear-gradient(to right, #FF4B2B, #FF416C);
            background: linear-gradient(to right, #FF4B2B, #FF416C);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 0 0;
            color: #FFFFFF;
            position: relative;
            left: -100%;
            height: 100%;
            width: 200%;
        }

        .sidebar-panel {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 40px;
            text-align: center;
            top: 0;
            height: 100%;
            width: 50%;
        }

        .darkmode--activated span {
            color: black !important;
            background-color: #FFFFFF !important;
        }

        .darkmode--activated p {
            color: white !important;
        }

    </style>

@endsection



@section('content')

    <div class="container" id="container">
        <div class="row">
            <div class="col-lg-6 form-container content">

                @if(isset($transaction))
                    {{--status OK--}}
                    <section style="direction: rtl">
                        <figure class="title text-center pt-3">
                            <h3>جزئیات تراکنش</h3>
                        </figure>

                        <figure class="text-center pt-3  " style="display: flex !important; justify-content: center;">
                            <ul class="list-group list-group-flush p-0">

                                <li class="list-group-item">
                                    <label>نام محصول:</label>
                                    <a class="text-primary"
                                       href="{{route('post',[mb_strtolower(\App\Article::where('id',$transaction->article_id)->first()->slug,'UTF-8')])}}">@lang('site.transaction.article_show_link')</a>
                                </li>
                                <li class="list-group-item">
                                    <label>قیمت محصول (تومان):</label>
                                    {{number_format($transaction->amount)}}
                                </li>
                                @if ($transaction->status == 1)
                                    <li class="list-group-item text-success">
                                        <label>وضعیت تراکنش:</label>
                                        @lang('site.transaction.ok')
                                    </li>
                                @elseif($transaction->status == 0)
                                    <li class="list-group-item text-danger">
                                        <label>وضعیت تراکنش:</label>
                                        @lang('site.transaction.nok')
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    <label>شناسه تراکنش:</label>
                                    {{ltrim(ltrim($transaction->transaction_id,"A"),"0")}}
                                </li>
                                <li class="list-group-item">
                                    <label>شناسه پرداخت:</label>
                                    {{(is_null($transaction->ref_id))?'-':$transaction->ref_id}}
                                </li>
                                <li class="list-group-item">
                                    <label>درگاه پرداخت:</label>
                                    {{(is_null($transaction->driver))?'-':$transaction->driver}}
                                </li>
                                @if (app()->getLocale()=='en')

                                    <li class="list-group-item">
                                        <label>تاریخ پرداخت:</label>
                                        {{$transaction->created_at_en}}
                                    </li>

                                @elseif(app()->getLocale()=='fa')

                                    <li class="list-group-item">
                                        <label>تاریخ پرداخت:</label>
                                        {{$transaction->created_at}}
                                    </li>

                                @endif
                            </ul>
                        </figure>

                    </section>

                @endif

            </div>

            @if($transaction->status == 1)
                <div class="col-lg-6 sidebar-container">
                    <div class="sidebar"
                         style="background: #009efd;background: -webkit-linear-gradient(180deg, #2af598 0%, #009efd 100%);background: linear-gradient(180deg, #2af598 0%, #009efd 100%);">
                        <div class="sidebar-panel sidebar-left pt-5 ">

                            <h1 class="mb-5">تاینی بلاگ</h1>
                            <h3>تراکنش موفق</h3>
                            <div class="fa-7x mb-3">
                                <i class="far fa-check-circle"></i>
                            </div>
                            <figure class="text-center pt-3">
                                <form action="{{route('home')}}"
                                      method="get">
                                    @csrf
                                <button class="btn btn-rounded waves-effect waves-light"  style="background-color: #2AF598FF; font-weight: bold !important; font-size: medium !important;" type="submit">بازگشت به سایت</button>
                                </form>
                            </figure>

                        </div>
                    </div>
                </div>
            @elseif($transaction->status == 0)
                <div class="col-lg-6 sidebar-container">

                    <div class="sidebar" style="background: #FF416C;background: -webkit-linear-gradient(to right, #FF4B2B, #FF416C);background: linear-gradient(to right, #FF4B2B, #FF416C);">

                    <div class="sidebar-panel sidebar-left  pt-5">

                            <h1 class="mb-5">تاینی بلاگ</h1>
                            <h3>تراکنش ناموفق</h3>

                            <div class="fa-7x mb-3">
                                <i class="far fa-times-circle"></i>
                                <h5 style="direction: rtl">{{$exception}}</h5>
                            </div>
                            <figure class="text-center pt-3">
                                <form action="{{route('home')}}"
                                      method="get">
                                @csrf
                                <button class="btn btn-rounded waves-effect waves-light"  style="background-color: #2AF598FF; font-weight: bold !important; font-size: medium !important;" type="submit">بازگشت به سایت</button>
                                </form>
                            </figure>

                        </div>
                    </div>
                </div>
            @endif
        </div>

    </div>

@endsection

