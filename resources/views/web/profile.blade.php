@extends('web.layouts.master')

@section('title', 'Tiny Blog - Profile Page')

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection

@section('content')

    <section>
        <div class="container mt-5 pt-3">
            <h1 class="title z-index-100" style="font-size: x-large;">@lang('site.profile.profile_information')</h1>
            <hr class="my-5 hr_darkmode_handler">
            <div class="row mt-5 pt-3">

                <!-- Main listing -->
                <div class="col-lg-9 col-12 mt-1">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                @endforeach
                            </ul>
                        </div>
                    @elseif(session()->get('update_profile_status')==true)
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                @lang('site.profile.update_success')
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        {{--{{$request->session()->forget('update_profile_status')}}--}}
                    @endif
                    <form action="{{route('update_profile')}}" class="form-group" method="post">
                        @csrf
                        @method('put')

                        <div class="md-form mb-5">
                            <input type="text" value="{{$user->name}}" id="name" name="name"
                                   class="input_darkmode_handler form-control">
                            <label for="name">@lang('site.profile.name')</label>
                        </div>

                        <div class="md-form mb-5">
                            <label>@lang('site.profile.date_birth')</label>
                            <input
                                type="text" ,
                                id="date-dropper"
                                class="input_darkmode_handler form-control"
                                name="date_birth"
                                data-default-date="{{$user->date_birth}}"
                                data-format="Y-m-d"
                                data-theme="date-dropper-theme-blue"
                                data-lang="{{app()->getLocale()}}"
                                data-large-mode="true"
                                data-modal="true"
                                data-translate-mode="true"
								data-max-year="2050"
                                readonly
                            />

                        </div>


                        <div  class="md-form mb-5">
                            <input type="tel" value="{{$user->phone}}"  id="phone" name="phone"
                                    class="input_darkmode_handler form-control">
                            <label for="phone">@lang('site.profile.phone_number')</label>
                        </div>

                        <div class="mb-5">
                            <label class="lable_darkmode_handler">@lang('site.profile.gender')</label>
                            <div class="btn-group" data-toggle="buttons">
                                @if ($user->gender==0)
                                    <label class="btn btn-mdb-color form-check-label active">
                                        <input class="form-check-input" type="radio" name="gender" value="0" id="option1"
                                               autocomplete="off" checked>
                                        @lang('site.profile.male')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="1" id="option2"
                                               autocomplete="off">
                                        @lang('site.profile.female')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="2" id="option3"
                                               autocomplete="off">
                                        @lang('site.profile.other')
                                    </label>
                                @elseif($user->gender==1)
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="0" id="option1"
                                               autocomplete="off">
                                        @lang('site.profile.male')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label active">
                                        <input class="form-check-input" type="radio" name="gender" value="1" id="option2"
                                               autocomplete="off" checked>
                                        @lang('site.profile.female')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="2" id="option3"
                                               autocomplete="off">
                                        @lang('site.profile.other')
                                    </label>
                                @elseif($user->gender==2)
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="0" id="option1"
                                               autocomplete="off">
                                        @lang('site.profile.male')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label">
                                        <input class="form-check-input" type="radio" name="gender" value="1" id="option2"
                                               autocomplete="off">
                                        @lang('site.profile.female')
                                    </label>
                                    <label class="btn btn-mdb-color form-check-label active">
                                        <input class="form-check-input" type="radio" name="gender" value="2" id="option3"
                                               autocomplete="off" checked>
                                        @lang('site.profile.other')
                                    </label>
                                @endif

                            </div>
                        </div>

                        <button
                            class="btn btn-block purple-gradient rounded waves-effect">@lang('site.profile.submit')</button>
                    </form>


                </div>


                <!-- Sidebar -->
                <div class="col-lg-3 col-12 mt-1">
                    @include('web.layouts.sidebar')
                </div>
                <!-- Sidebar -->

            </div>
        </div>

    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {



            $("#date-dropper").dateDropper();



        });


    </script>
@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
