<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact With Us</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">


        <h2>@lang('site.email_views.hello_admin')</h2>
        @lang('site.email_views.hello_admin_message') {{$data['name'] ?? ''}}

        <br><br><br>


        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Title</th>
                <th scope="col">Message</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$data['name'] ?? ''}}</td>
                <td>{{$data['email'] ?? ''}}</td>
                <td>{{$data['phone'] ?? ''}}</td>
                <td>{{$data['subject'] ?? ''}}</td>
                <td>{{$data['message'] ?? ''}}</td>
            </tr>

            </tbody>
        </table>

        <br>

        <p>@lang('site.email_views.thank_you')</p>

    </div>
</div>
</body>
</html>
