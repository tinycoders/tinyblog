@extends('web.layouts.master')

@section('title', 'Tiny Blog - Category Page')

@section('styles')
    <style>
        .darkmode--activated .title{
            color:#000 !important;
        }


    </style>
@endsection

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection


@section('content')
    <!-- Blog section -->
    <section>

        <div class="container mt-5 pt-3">

            <!-- Main listing -->
            <div class=" mt-1">


                    @if ($category)
                        @if (app()->getLocale()=='en')

                            <p class="title text-left" style="font-size: x-large;">
                             @lang('site.category.category_list'):
                            {{ucfirst($category->name)}}
                            </p>
                        @elseif(app()->getLocale()=='fa')
                            <p class="title text-right" style="font-size: x-large;">
                                @lang('site.category.category_list'):
                                {{ucfirst($category->name_fa)}}
                            </p>
                        @endif
                    @else
                        @lang('site.category.category_not_found')
                    @endif



                <hr class="my-5">



            </div>

            <!-- Blog -->
            <div class="row mt-5 pt-3">




                @include('web.layouts.post')
                <!-- Main listing -->

                <!-- Sidebar -->
                <div class="col-lg-3 col-12 mt-1">
                    @include('web.layouts.sidebar')
                </div>
                <!-- Sidebar -->
            </div>
            <!-- Blog -->

        </div>
        <!-- Main listing -->


    </section>
    <!-- Blog section -->

@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
