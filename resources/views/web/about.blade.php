@extends('web.layouts.master')

@section('title', 'Tiny Blog - About Page')

@section('styles')
    <style>

        .about_row {
            background-color: #100f2c !important;
            border-radius: 8px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            direction: {{app()->getLocale() === 'en' ? 'ltr':'rtl'}}   !important;
        }

        .about_row * {
            color: #fff !important;
        }

        .about_row hr {
            background: #fff;
        }

        .darkmode--activated .about_row {
            background-color: #ccc !important;
        }

        .darkmode--activated .about_row {
            color: black !important;
        }

        .darkmode--activated .about_row * {
            color: #fff !important;
        }


    </style>
@endsection


@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection

@section('content')

    <section>
        <div class="container-fluid white">
            <hr class="mb-5 mt-0">
            <div class="container">

                <!-- Section: Blog v.3 -->
                <section class="mt-5 py-5 text-center text-lg-left">

                    <!-- Grid row -->
                    <div class="row my-xl-5 py-xl-4  about_row">

                        <!-- Grid column -->
                        <div class="col-sm-12 col-md-5 col-xl-5 m-4 about_main">

                            <!-- Image -->
                            <div class="view overlay">
                                <img style="border-radius: 5px" src="/images/about_us.png" class="img-fluid z-depth-1"
                                     alt="">
                                <div class="mask rgba-white-slight"></div>
                            </div>
                            <!-- Image -->

                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-sm-12 col-md-6 col-xl-6">

                            <h3 class="pb-2 font-weight-bold" style="text-align: {{app()->getLocale() === 'en' ? 'left':'right'}};">
                                <strong>@lang('site.about.about_us')</strong>
                            </h3>
                            <hr>

                            <p class=" mt-4 text-justify">@lang('site.about.about_us_text')</p>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                </section>
                <!-- Section: Blog v.3 -->

            </div>

        </div>

    </section>

@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
