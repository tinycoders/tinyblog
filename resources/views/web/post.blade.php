@extends('web.layouts.master')

@section('title', 'Tiny Blog - Post Page')

@section('styles')
    <style>

        .comment-title{
            background-color: #ff8700;
            border-radius: 8px;
            padding-top: 5px;
        }

        .darkmode--activated  .comment-title{
            background-color: blueviolet;
            border-radius: 8px;
            padding-top: 5px;
            color: white;
        }

        .comment-title-child{
            padding-top: 1rem;
        }
        .comment_main {
            border-radius: 15px;
            border: 1px solid #757575;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            background-color: #6351ce!important;
            color: white !important;
        }

        .darkmode--activated .comment_main {
            background-color: #212529 !important;
            color: black !important;
            border: unset !important;
            box-shadow: 0 2px 4px 0 rgba(146, 146, 146, 0.92);
        }

        .comment_content {
            color: white !important;
        }

        .darkmode--activated .comment_content {

            color: black !important;

        }


        .comment_profile ,.comment_content {
            margin-top: 16px;
        }

        .comment_profile img  {
            display: block;
            border-radius: 200px;
            box-sizing: border-box;
            border: 5px solid #00ff9f;
        }

        .darkmode--activated .comment_profile img  {
            display: block;
            border-radius: 200px;
            box-sizing: border-box;
            border: 5px solid #00ff9f;
        }

        p[data-f-id='pbf']{
            display:none !important;
        }

    </style>
@endsection

@section('header')


    <header class="header">

    @include('web.layouts.header')
    {{--@include('web.layouts.intro')--}}

    <!-- Intro Section -->
    <div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}'
         style="background-image: url('{{$article->cover}}'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <div class="mask rgba-black-strong">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="row smooth-scroll">
                    <div class="col-md-12 white-text text-center">
                        <div class=" animated zoomIn delay-1s">
                            <h2 class="display-3 font-weight-bold mb-2">@lang('site.intro.brand')</h2>
                            <hr class="hr-light">
                            <h3 class="subtext-header mt-4 mb-5" style="direction: rtl;text-align: right">{{$article->title}}</h3>
                        </div>

                        @auth
                            <a href="/profile" data-offset="100" class="btn purple-gradient animated lightSpeedIn delay-1s"
                               data-wow-delay="0.2s">@lang('site.intro.profile')</a>
                            <a href="/favorite" data-offset="100" class="btn aqua-gradient animated lightSpeedIn delay-1s"
                               data-wow-delay="0.2s">@lang('site.intro.my_favorite')</a>
                        @else
                            <a href="/login" data-offset="100" class="btn aqua-gradient animated lightSpeedIn delay-1s">@lang('site.intro.login')</a>
                            <a href="/register" data-offset="100" class="btn peach-gradient animated lightSpeedIn delay-1s"
                               data-wow-delay="0.2s">@lang('site.intro.register')</a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>

    </header>
@endsection


@section('content')

    <section>
        <div class="container-fluid">
            <hr class="my-5">
            <div class="container">

                <!-- Cover photo -->
                <div class="row mb-2 mt-1">

                    <div class="col-md-12">

                        <!-- Image -->
                        <div class="view">
                            <img src="{{$article->cover}}" class="img-fluid z-depth-1 wow fadeIn"
                                 data-wow-delay="0.2s"
                                 style="height: 600px; width: 100%; object-fit: cover; object-position: center;">
                        </div>

                    </div>

                </div>
                <!--  Cover photo -->

                <div class="row mt-4 pt-3">

                    <div class="col-lg-9 col-12 mt-1">
                        <!-- Section: Blog v.3 -->
                        <section class="extra-margins pb-5 text-lg-left">

                            <!-- Grid row -->
                            <div class="row mb-4">

                                <!-- Grid column -->
                                <div class="col-md-12">

                                    <!-- Title -->
                                    <h4 class="font-weight-bold wow fadeIn post-title" data-wow-delay="0.2s">
                                        <strong class="strong_darkmode_handler "
                                                style="direction: rtl;text-align: right;">{{$article->title}}</strong>
                                    </h4>
                                    <hr>
                                    <!-- Text -->

                                    <!-- Grid row -->
                                    <div class="mx-4 mt-3 wow fadeIn post-body" data-wow-delay="0.2s">

                                        {!!$article->body!!}

                                    </div>
                                    <!-- Grid row -->

                                    <hr>

                                @php
                                    use App\Article_Favorite;
                                        if (auth()->check()) {

                                            $isExists = Article_Favorite::where('article_id', $article->id)->where('user_id', auth()->user()->id)->count();
                                        }
                                @endphp

                                <!-- Grid row -->
                                    <div class="row mb-4 wow fadeIn" data-wow-delay="0.2s">

                                        <!-- Grid column -->
                                        <div class="col-md-12 text-center">

                                            <div>
                                                <strong>تعداد دانلود مقاله:</strong>
                                                <span>
                                                    {{(round($article->download_count/3))}}
                                                </span>
                                            </div>
                                            <hr>
                                            <h4 class="text-center  dark-grey-text mt-3 mb-3">
                                                <strong>@lang('site.post_content.toolbox')</strong>
                                            </h4>



                                            <div class="toolbox">
                                                 @auth
                                                 @if ($isExists==0)

                                                    <form  style="display: contents !important;" action="{{route('add_favorite')}}" method="post">
                                                        @csrf
                                                        <button name="article" value="{{$article->id}}"
                                                                class="btn btn-yellow btn-lg waves-effect waves-light">
                                                            <i class="fas fa-archive"></i> @lang('site.post_content.favorite')
                                                        </button>
                                                    </form>

                                                    @endif
                                                @endauth


                                                <form  style="display: contents !important;" action="{{route('post.pdf',[mb_strtolower($article->slug,'UTF-8')])}}"
                                                      method="get">
                                                    @csrf
                                                    <button class="btn btn-success btn-lg waves-effect waves-light">@lang('site.post_content.download_article')</button>
                                                </form>

                                                </div>
                                            <hr class="mt-5">

                                        </div>
                                        <!-- Grid column -->

                                    </div>
                                    <!-- Grid row -->


                                        <!-- Comments -->
                                        @include('web.layouts.comment-list')
                                        <!-- Comments -->

                                         <hr>


                                     <!-- Section: Leave a reply (Not Logged In User) -->
                                     @include('web.layouts.add-comment')
                                     <!-- Section: Leave a reply (Not Logged In User) -->

                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row -->

                        </section>
                        <!-- Section: Blog v.3 -->
                    </div>

                    <div class="col-lg-3 col-12 mt-1 wow fadeIn" data-wow-delay="0.2s">
                        @include('web.layouts.sidebar')
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
