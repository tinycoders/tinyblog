@extends('web.layouts.master')

@section('title', 'Tiny Blog - Home Page')

@section('header')
    <header class="header">
    @include('web.layouts.header')
    @include('web.layouts.intro')
    </header>
@endsection


@section('content')
    <!-- Blog section -->
    <section>

        <div class="container mt-5 pt-3">

            <!-- Blog -->
            <div class="row mt-5 pt-3">

                @include('web.layouts.post')

                <!-- Sidebar -->
                <div class="col-lg-3 col-12 mt-1">
                    @include('web.layouts.sidebar')
                </div>
                <!-- Sidebar -->
            </div>
            <!-- Blog -->

        </div>
        <!-- Main listing -->


    </section>
    <!-- Blog section -->

@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
