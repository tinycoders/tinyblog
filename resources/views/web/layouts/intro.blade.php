<!-- Intro Section -->
<div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}'
      style="background-image: url('../images/intro.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="mask rgba-black-strong">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row smooth-scroll">
                <div class="col-md-12 white-text text-center">
                    <div class=" animated zoomIn delay-1s">

                        @php

                          if(app()->getLocale() === 'en'){
                            $title = App\Setting::query()->where('key',\App\Setting::SITE_TITLE)->first();

                          }elseif(app()->getLocale() === 'fa'){
                            $title = App\Setting::query()->where('key',\App\Setting::SITE_TITLE_FA)->first();

                          }

                        @endphp

                        <h2 class="display-3 font-weight-bold mb-2">{{$title->value}}</h2>

                        <hr class="hr-light">
                        <h3 class="subtext-header mt-4 mb-5">@lang('site.intro.message_favorite')</h3>
                    </div>

                    @auth
                        <a href="/profile" data-offset="100" class="btn purple-gradient animated lightSpeedIn delay-1s"
                           data-wow-delay="0.2s">@lang('site.intro.profile')</a>
                        <a href="/favorite" data-offset="100" class="btn aqua-gradient animated lightSpeedIn delay-1s"
                           data-wow-delay="0.2s">@lang('site.intro.my_favorite')</a>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn peach-gradient animated lightSpeedIn delay-1s" data-toggle="modal" data-target="#searchModal">
                            جستجوی پیشرفته
                        </button>
                    @else
                        <a href="/login" data-offset="100" class="btn aqua-gradient animated lightSpeedIn delay-1s">@lang('site.intro.login')</a>
                        <a href="/register" data-offset="100" class="btn peach-gradient animated lightSpeedIn delay-1s"
                       data-wow-delay="0.2s">@lang('site.intro.register')</a>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn purple-gradient animated lightSpeedIn delay-1s" data-toggle="modal" data-target="#searchModal">
                            جستجوی پیشرفته
                        </button>
                    @endauth



                </div>
            </div>
        </div>
    </div>
</div>

@include('web.pro-search')
