

    <div class="progress-container m-0 p-0 fixed-top" style="z-index: 1040;">
        <div class="progress-bar m-0 p-0" id="myBar"></div>
    </div>

    <!-- Navbar -->
    <style>
        nav{
            padding-right: 16px !important;
        }
    </style>
    <nav
        class="animated delay-1s bounceInDown mb-1 navbar navbar-expand-lg fixed-top scrolling-navbar navbar-dark unique-color-dark"
        style="direction: {{app()->getLocale() === 'en' ? 'ltr' : 'rtl'}};">
        <div class="container-fluid">
            <a class="navbar-brand font-weight-bold" href="{{route('home')}}">@lang('site.header.brand')</a>
            <!-- Collapse button -->
            <button class="navbar-toggler first-button" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                    class="dark-blue-text">
                {{--<i class="fas fa-bars fa-1x"></i>--}}
                <div class="animated-icon1">
                    <span style="background-color:white !important;"></span>
                    <span style="background-color:white !important;"></span>
                    <span style="background-color:white !important;"></span>
                </div>

            </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav {{app()->getLocale() === 'en' ? 'mr-auto' : 'ml-auto'}}">
                    <li class="nav-item">
                        <a class="nav-link" style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                           href="{{route('home')}}">@lang('site.header.home')
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    @if (\App\Category::all()->count() <=10)
                        <li class="nav-item dropdown">
                            <a href="{{route('categories')}}" class="nav-link dropdown-toggle"
                               style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}};right:0"
                               id="navbarDropdownMenuLink-333"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">@lang('site.header.categories')
                            </a>
                            <div class="dropdown-menu dropdown-dark" aria-labelledby="navbarDropdownMenuLink-333">
                                @if (app()->getLocale()=='en')
                                    @foreach(\App\Category::all() as $category)
                                        <a class="dropdown-item text-left"
                                           href="{{route('category',[mb_strtolower($category->slug,'UTF-8')])}}">{{ucwords($category->name)}}</a>
                                    @endforeach
                                @elseif(app()->getLocale()=='fa')
                                    @foreach(\App\Category::all() as $category)
                                        <a class="dropdown-item text-right"
                                           href="{{route('category',[mb_strtolower($category->slug,'UTF-8')])}}">{{$category->name_fa}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </li>
                    @elseif(\App\Category::all()->count() >10)
                        <li class="nav-item">
                            <a class="nav-link" style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                               href="{{route('categories')}}">@lang('site.header.categories')
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    @endif


                    <li class="nav-item">
                        <a class="nav-link" style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                           href="{{route('about')}}">@lang('site.header.about_us')
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                           href="{{route('contactView')}}">@lang('site.header.contact_us')
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav {{app()->getLocale() === 'en' ? 'ml-auto' : 'mr-auto'}}">
                    <li class="nav-item">
                        <form class="form-group form-inline align-items-center" action="{{route('search')}}"
                              method="get">
                            <div class="md-form my-0 d-inline-flex">
                                <input id="search" required minlength="4" name="search" class="form-control mr-sm-2"
                                       type="text" placeholder="@lang('site.header.search')" aria-label="Search">
                            </div>
                            <button class="btn btn-outline-light-blue btn-md my-2 my-sm-0 ml-3"
                                    type="submit">@lang('site.header.search')
                            </button>
                        </form>
                    </li>
                    @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                               style="{{app()->getLocale() === 'en' ? 'text-align:left;' : 'text-align:right;'}};"
                               id="navbarDropdownMenuLink-4" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i> @lang('site.header.profile') </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-dark"
                                 style="{{app()->getLocale() === 'en' ? 'text-align:right; left: auto; right: 0' :'text-align:left; left: 0; right: auto' }};"
                                 aria-labelledby="navbarDropdownMenuLink-4">
                                <a class="dropdown-item"
                                   style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                                   href="{{route('show_profile')}}">@lang('site.header.my_account')</a>
                                <a class="dropdown-item"
                                   style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                                   href="{{route('show_favorite')}}">@lang('site.header.my_favorite')</a>
                                <a class="dropdown-item"
                                   style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}}"
                                   href="{{route('my_transactions')}}">@lang('site.header.my_transactions')</a>
                                <form action="{{route('logout')}}" method="POST"
                                      style="text-align: {{app()->getLocale() === 'en' ? 'left' : 'right'}};width: unset;">
                                    @csrf
                                    <button class="dropdown-item">@lang('site.header.logout')</button>
                                </form>

                            </div>
                        </li>
                    @endauth
                </ul>

            </div>


        </div>

    </nav>
    <!-- Navbar -->

