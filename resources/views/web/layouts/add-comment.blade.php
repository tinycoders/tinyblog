<section class="mb-4 wow fadeIn" data-wow-delay="0.2s">
    <h3 class=" p_darkmode_handler font-weight-bold text-center my-5">@lang('site.comment_form.leave_a_reply')</h3>

    @auth
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    @endforeach
                </ul>
            </div>
        @elseif(session()->get('add_favorite_status')==true)
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                @lang('site.comment_form.add_favorite_status')
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{--{{$request->session()->forget('add_favorite_status')}}--}}
        @endif
        <form action="{{route('add_comment')}}" method="post">
        @csrf

        <!-- Grid row -->



            <!-- Grid row -->
            <div class="row">

                <div class="col-12 mt-1">
                    <div class="form-group basic-textarea rounded-corners">

                        @if (app()->getLocale()=='en')
                            <textarea required minlength="10" name="message" class="message  textarea_darkmode_handler form-control" id="exampleFormControlTextarea6"
                                      rows="5"
                                      placeholder="@lang('site.comment_form.write_something_here')" style="direction: ltr"></textarea>

                        @elseif(app()->getLocale()=='fa')
                            <textarea required minlength="10" name="message" class="message  textarea_darkmode_handler form-control" id="exampleFormControlTextarea6"
                                      rows="5"
                                      placeholder="@lang('site.comment_form.write_something_here')" style="direction: rtl"></textarea>

                        @endif
                    </div>

                    <div class="text-right">
                        <button name="article" value="{{$article->id}}"
                            class="btn peach-gradient btn-block waves-effect waves-light">@lang('site.comment_form.submit')</button>
                    </div>

                </div>

            </div>
            <!-- Grid row -->
        </form>
    @else

        <div class="row justify-content-center">
            <a href="/login" data-offset="100" class="btn aqua-gradient wow fadeInLeft"
               data-wow-delay="0.2s">@lang('site.comment_form.login')</a>
            <a href="/register" data-offset="100" class="btn peach-gradient wow fadeInRight "
               data-wow-delay="0.2s">@lang('site.comment_form.register')</a>
        </div>

    @endauth
</section>
