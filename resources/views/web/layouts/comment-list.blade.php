<section>
    <!-- Main wrapper -->
    <div class="comments-list text-center text-md-left">
        @php
        $comments=\App\Comment::where('article_id',$article->id)->where('status',1)->paginate(10);
        @endphp
        <div class="row ">
            <div class="col comment-title text-center mt-3 mb-3">
                <h3 class="comment-title-child font-weight-bold">@lang('site.comment_list.comments')
                    <p class=" badge indigo">{{$comments->total()}}</p>
                </h3>
            </div>

        </div>

        @php
        foreach($comments as $comment){
        $user_id=$comment->user_id;
        $article_id=$comment->article_id;
        $created_at=$comment->created_at;
        $created_at_en=$comment->created_at_en;
        $level=$comment->level;
        $comment=$comment->comment;
        $user=\App\User::where('id',$user_id)->first();
        $article=\App\Article::where("id",$article_id)->first();

        if($level==0){
        echo '<div class="row mb-4 comment_main">';
            echo '<div class="col-sm-2 col-12 mb-3 comment_profile">';
                $url=$user->avatar;
                if($url=="/images/male.png" || $url=="/images/femail.png" || $url=="/images/other.png"){
                    if(Gravatar::exists($user->email)){
                        $url=Gravatar::get($user->email);
                    }else{
                        $url=$user->avatar;
                    }
                }else{
                     $url=$user->avatar;
                }
                echo '<img src="'.$url.'" class="avatar rounded-circle z-depth-1-half" alt="sample image">';
                echo '</div>';

            echo '<div class="col-sm-10 col-12 comment_content">';
                echo '<a>
                    <h5 class="p_darkmode_handler user-name font-weight-bold">'.$user->name.'</h5>
                </a>';
                echo '<div class="card-data">';
                    echo '<ul class="list-unstyled">';
                        echo '<i class="far fa-clock"></i> ';
                        if(app()->getLocale()=='fa'){
                        echo '<li class="li_darkmode_handler comment-date font-small"
                            style="direction: rtl; display: inline-block;">';
                            echo ' '.$created_at.' ';
                            echo '</li>';
                        }elseif (app()->getLocale()=='en'){
                        echo '<li class="li_darkmode_handler comment-date font-small"
                            style="direction: ltr; display: inline-block;">';
                            echo ' '.$created_at_en.' ';
                            echo '</li>';

                        }
                        echo '</ul>';
                    echo '<p class="">'.$comment.'</p>';

                    echo '</div>';
                echo '</div>';


            echo '</div>';
        }
        elseif($level>0){
        echo '<div class="row mb-4 comment_main">';


            echo '<div class="col-sm-10 col-12 comment comment_content">';
                echo '<a>
                    <h5 class="p_darkmode_handler user-name font-weight-bold">'.$user->name.'</h5>
                </a>';
                echo '<div class="card-data">';
                    echo '<ul class="list-unstyled">';


                        echo '<i class="far fa-clock"></i> ';
                        if(app()->getLocale()=='fa'){
                        echo '<li class="li_darkmode_handler comment-date font-small"
                            style="direction: rtl; display: inline-block;">';
                            echo ' '.$created_at.' ';
                            echo '</li>';
                        }elseif (app()->getLocale()=='en'){
                        echo '<li class="li_darkmode_handler comment-date font-small"
                            style="direction: ltr; display: inline-block;">';
                            echo ' '.$created_at_en.' ';
                            echo '</li>';
                        }
                        echo '</ul>';
                    echo '<p class="p_darkmode_handlerdark-grey-text article">'.$comment.'</p>';

                    echo '</div>';
                echo '</div>';

            echo '<div class="col-sm-2 col-12 mb-3 comment_profile">';
                $url=$user->avatar;
                if($url=="/images/male.png" || $url=="/images/femail.png" || $url=="/images/other.png"){
                if(Gravatar::exists($user->email)){
                $url=Gravatar::get($user->email);
                }else{
                $url=$user->avatar;
                }
                }else{
                $url=$user->avatar;
                }
                echo '<img src="'.$url.'" class="avatar rounded-circle z-depth-1-half" alt="sample image">';
                echo '</div>';

            echo '</div>';
        }
        }

        @endphp

        <!-- Pagination -->
        <nav class="mb-5 pb-2 justify-content-center d-flex">
            {{$comments->links()}}
        </nav>
        <!-- Pagination -->

    </div>
    <!-- Main wrapper -->
</section>
