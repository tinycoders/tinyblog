<!-- Main listing -->
<div class="col-lg-9 col-12 mt-1">

@if (isset($articles))

    <!-- Section: Blog v.3 -->
        <section class="pb-3 text-center text-md-left">

        @foreach($articles as $article)

            <!-- Grid row -->
                <div class="row post-animate pt-3 post-item">

                    <!-- Grid column -->
                    <div class=" col-lg-5 mb-4">
                        <!-- Featured image -->
                        <div class="view overlay z-depth-1 card-image">
                            <img src="{{$article->cover}}" class="img-fluid image-post"
                                 alt="Image Cover">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-6 ml-xl-4 mb-4">

                        <h5 class="mb-1 mt-0 post-title">
                            <strong>
                                <a href="{{route('post',[mb_strtolower($article->slug,'UTF-8')])}}">{{$article->title}}</a>
                            </strong>
                        </h5>
                        <!-- Grid column -->
                        <div
                            class="text-sm-center text-md-right text-lg-left  d-flex justify-content-between align-items-center">
                            <p
                                class="orange-text font-small font-weight-bold mb-1 spacing d-flex align-items-center">

                                @if (app()->getLocale()=="en")
                                    @foreach($article->categories()->get() as $category)
                                        <a class="">
                                            <strong>{{" ".strtoupper($category->name)." "}} </strong>
                                            {{!$loop->last?", ":""}}
                                        </a>
                                    @endforeach
                                @elseif(app()->getLocale()=='fa')
                                    @foreach($article->categories()->get() as $category)
                                        <a class="">
                                            <strong>{{" ".$category->name_fa." "}} </strong>
                                            {{!$loop->last?", ":""}}
                                        </a>
                                    @endforeach

                                @endif

                            </p>
                            <div class="article-date">
                                @if (app()->getLocale()=="en")
                                    <p class="font-small grey-text" style="direction: ltr">
                                        <em> {{$article->created_at_en}}</em>
                                    </p>
                                @elseif(app()->getLocale()=="fa")
                                    <p class="font-small grey-text">
                                        <em> {{$article->created_at}}</em>
                                    </p>
                                @endif
                            </div>

                        </div>
                        <!-- Grid column -->


                        <p class="" style="height: 80px; direction: rtl; text-align: right;">
                            {{mb_strlen($article->abstract,"UTF-8") > 159 ? mb_substr($article->abstract,0,159,"UTF-8")."...": $article->abstract}}
                        </p>


                        <div
                            class="row d-flex justify-content-between align-items-center fixed-bottom"
                            style="position: unset; z-index: unset !important;">

                        @if($article->isPaid())

                            <!-- Deep-orange -->
                                <a href="{{route('post',[mb_strtolower($article->slug,'UTF-8')])}}"
                                   class="btn btn-deep-orange btn-rounded btn-sm">@lang('site.post.read_more')</a>

                                @auth
                                    @if (\Route::current()->getName() == 'show_favorite')
                                        <form action="{{route('delete_favorite')}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button name="id" value="{{$article->id}}"
                                                    class="btn btn-danger btn-rounded btn-sm">@lang('site.post.remove')</button>
                                        </form>
                                    @endif

                                @endauth
                            @endif

                            @if($article->isFree())
                                <!-- Deep-orange -->
                                <a href="{{route('post',[mb_strtolower($article->slug,'UTF-8')])}}"
                                    class="btn btn-deep-orange btn-rounded btn-sm">@lang('site.post.read_more')</a>

                            @elseif(!$article->isFree() && !$article->isPaid())
                                <form action="{{route('request_payment',[mb_strtolower($article->slug,'UTF-8')])}}"
                                    method="get">
                                    @csrf
                                    <button class="btn btn-link btn-success btn-rounded btn-sm">خرید مقاله</button>
                                </form>
                            @endif

                            <p class="article-date text-right p_darkmode_handler pr-1"
                               style="color: grey; font-size: 0.9rem;">
                                {{$article->user->name}}
                            </p>
                        </div>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->


            @endforeach


        </section>
        <!-- Section: Blog v.3 -->

        <!-- Pagination dark grey -->
        <nav class="mb-5 pb-2 justify-content-center d-flex">
            {{$articles->links()}}
        </nav>
        <!-- Pagination dark grey -->

    @endif
</div>
<!-- Main listing -->
