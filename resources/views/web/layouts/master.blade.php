<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>@yield("title")</title>
    <link rel="stylesheet" href="{{mix("/css/app.css")}}">
    <link rel="stylesheet" href="/date-dropper/datedropper.css">
    <link rel="stylesheet" href="/date-dropper/date-dropper-theme-blue.css">

    @yield("styles")

    <style>
        .darkmode--activated .post-item {
            background-color: #1c2331 !important;
        }

        .post-item {
            border-radius: 16px;
            margin-top: 16px;
            box-shadow: 0 .300rem .70rem rgba(0, 0, 0, .200) !important;
            transition: all 0.3s ease-in-out;
        }

        .darkmode--activated .post-item:hover {
            box-shadow: 0 2px 4px 0 rgba(42, 95, 255, 0.68) !important;
            transform: scale(1.01);
            z-index: 2;
            position: relative !important;
        }

        .post-item:hover {
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5) !important;
            transform: scale(1.01);
            z-index: 2;
            position: relative !important;
        }

    </style>

</head>

<body>

{{--Navigation--}}
@yield('header')
{{--Navigation--}}



{{--Main Layout--}}
<main>
    @yield("content")
</main>
{{--Main Layout--}}

@yield('footer')

<script src="{{mix("/js/app.js")}}"></script>
<script src="/scrollmagic/ScrollMagic.js"></script>
<script src="/scrollmagic/debug.addIndicators.js"></script>
<script>

    ScrollMagic._util.addClass = function _patchedAddClass(elem, classname) {
        if (classname) {
            if (elem.classList) {
                classname.split(' ').forEach(function _addCls(c) {
                    elem.classList.add(c);
                });
            } else elem.className += ' ' + classname;
        }
    };
    ScrollMagic._util.removeClass = function _patchedRemoveClass(elem, classname) {
        if (classname) {
            if (elem.classList) {
                classname.split(' ').forEach(function _removeCls(c) {
                    elem.classList.remove(c);
                });
            } else elem.className = elem.className.replace(new RegExp('(^|\b)' + classname.split(' ').join('|') + '(\b|$)', 'gi'), ' ');
        }
    };


    var controller = new ScrollMagic.Controller();

    var scene = new ScrollMagic.Scene({
        triggerElement: '.post-animate',

    })
        .setClassToggle('.post-animate', 'animated slow fadeIn')
        /*.addIndicators()*/
        .addTo(controller);


</script>
<script src="/js/darkmode-js.min.js"></script>
<script src="{{mix("/js/app2.js")}}"></script>
<script src="/date-dropper/datedropper.min.js"></script>

<script>
    // When the user scrolls the page, execute myFunction
    window.onscroll = function () {
        myFunction()
    };

    function myFunction() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        var scrolled = (winScroll / height) * 100;
        document.getElementById("myBar").style.width = scrolled + "%";
    }
</script>


@yield("scripts")
</body>

</html>
