<!-- Popular posts -->
<section class="section widget-conten mb-5 ">

    <!--  Card -->
    <div class="card card-body pb-0 sidebar_card">
        <div class="single-post ">

            <p class="font-weight-bold dark-grey-text text-center spacing grey lighten-4 py-2 mb-4 sidebar_title">
                <strong class="strong_darkmode_handler">@lang('site.sidebar.popular_articles')</strong>
            </p>

        @php
            $articlesId=DB::table("article_favorite")->select(DB::raw("article_id, COUNT(article_id) as count"))
            ->groupBy("article_id")->limit(10)->orderBy("count","desc")->get();
            $articles=null;
            if($articlesId){
                foreach ($articlesId as $article){
                    $articles[]=\App\Article::where('id',$article->article_id)->first();
                }
            }
        @endphp
        @if($articles)
            <!-- Grid row -->

                @foreach($articles as $article)
                    <div class="row mb-4">
                        <div class="col-5">
                            <!-- Image -->
                            <div class="view overlay">
                                <img src="{{$article->cover}}"
                                     class="img-fluid z-depth-1 rounded-0" alt="sample image">
                                <a href="{{route('post',['slug' => mb_strtolower($article->slug,'UTF-8')])}}">
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>
                        </div>

                        <!-- Excerpt -->
                        <div class="col-7">
                            <h6 class="mt-0 font-small">
                                <a class="text-dark"
                                   href="{{route('post',['slug' => mb_strtolower($article->slug,'UTF-8')])}}">
                                    <strong class="strong_darkmode_handler">{{$article->title}}</strong>
                                </a>
                            </h6>

                            <div class="post-data">
                                @if(app()->getLocale()=="en")
                                    <p class="font-small mb-0" style="direction: ltr !important;">
                                        <i class="far fa-clock"></i>{{$article->created_at_en}}
                                    </p>
                                @elseif(app()->getLocale()=="fa")
                                    <p class="font-small mb-0" style="direction: rtl !important;">
                                        <i class="far fa-clock"></i>{{$article->created_at}}
                                    </p>
                                @endif


                            </div>
                        </div>
                        <!--  Excerpt -->
                    </div>
                    <!--  Grid row -->
                @endforeach


            @endif


        </div>

    </div>
</section>
<!-- Popular posts -->


<!-- Top Views posts -->
<section class="section widget-conten mb-5 ">

    <!--  Card -->
    <div class="card card-body pb-0 sidebar_card">

        <div class="single-post ">

            <p class="font-weight-bold dark-grey-text text-center spacing grey lighten-4 py-2 mb-4 sidebar_title">
                <strong class="strong_darkmode_handler">@lang('site.sidebar.top_views_articles')</strong>
            </p>

            @php
                use App\Article;
                $top_views=Article::where('view_count','>',0)
                ->orderBy("view_count","desc")
                ->limit(10)
                ->get();
            @endphp


            @if($top_views)
            <!-- Grid row -->

                @foreach($top_views as $top_view)
                    <div class="row mb-4">
                        <div class="col-5">
                            <!-- Image -->
                            <div class="view overlay">
                                <img src="{{$top_view->cover}}"
                                     class="img-fluid z-depth-1 rounded-0" alt="sample image">
                                <a href="{{route('post',['slug' => mb_strtolower($top_view->slug,'UTF-8')])}}">
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>
                        </div>

                        <!-- Excerpt -->
                        <div class="col-7">
                            <h6 class="mt-0 font-small">
                                <a class="text-dark"
                                   href="{{route('post',['slug' => mb_strtolower($top_view->slug,'UTF-8')])}}">
                                    <strong class="strong_darkmode_handler">{{$top_view->title}}</strong>
                                </a>
                            </h6>

                            <div class="post-data">
                                @if(app()->getLocale()=="en")
                                    <p class="font-small mb-0" style="direction: ltr !important;">
                                        <i class="far fa-clock"></i>{{$top_view->created_at_en}}
                                    </p>
                                @elseif(app()->getLocale()=="fa")
                                    <p class="font-small mb-0" style="direction: rtl !important;">
                                        <i class="far fa-clock"></i>{{$top_view->created_at}}
                                    </p>
                                @endif


                            </div>
                        </div>
                        <!--  Excerpt -->
                    </div>
                    <!--  Grid row -->
                @endforeach


            @endif


        </div>

    </div>

</section>
<!-- Top Views posts -->


<!-- Section: Categories -->
<section class=" section mb-5 ">

    <!--  Card -->
    <div class="card card-body pb-0 sidebar_card">
        <div class="single-post ">

            <p class="font-weight-bold dark-grey-text text-center spacing grey lighten-4 py-2 mb-4 sidebar_title">
                <strong class="strong_darkmode_handler">@lang('site.sidebar.categories')</strong>
            </p>

            <ul class="list-group my-4 sidebar_card">
                @if (app()->getLocale()=="en")

                    @foreach(\App\Category::all() as $category)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a class="text-dark"
                               href="{{route('category',['slug' => mb_strtolower($category->slug,'UTF-8')])}}">
                                <p class="mb-0 ">{{ucwords($category->name)}}</p>
                            </a>
                            <span class="badge orange badge-pill font-small">{{$category->articles()->count()}}</span>
                        </li>
                    @endforeach

                @elseif(app()->getLocale()=="fa")

                    @foreach(\App\Category::take(10)->get() as $category)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a class="text-dark"
                               href="{{route('category',['slug' => mb_strtolower($category->slug,'UTF-8')])}}">
                                <p class="mb-0">{{$category->name_fa}}</p>
                            </a>
                            <span class="badge orange badge-pill font-small">{{$category->articles()->count()}}</span>
                        </li>
                    @endforeach

                @endif

            </ul>
        </div>

    </div>

</section>
<!-- Section: Categories -->
