<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tiny Blog</title>

    <style>
        @page {
            header: page-header;
            footer: page-footer;
        }


    </style>
</head>
        <body dir="rtl">

        <htmlpageheader name="page-header">
            TinyBolg
        </htmlpageheader>



        <htmlpagefooter name="page-footer">
            {PAGENO}
        </htmlpagefooter>



        <div class="pdf-image-cover">
            <img src="{{$article->cover}}">
        </div>


        <div class="content-title">


            <h4 class="font-weight-bold post-title">
                <strong>{{$article->title}}</strong>
            </h4>
            <hr>



            <div class="post-body">

                {!!$article->body!!}

            </div>


            <hr>

        </div>


</body>
</html>
