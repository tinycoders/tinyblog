{{--Footer--}}
<footer style="padding: 0px !important; isolation: isolate; "
        class="page-footer font-small unique-color-dark stylish-color-dark text-center text-md-left mt-4 pt-4">

    <div style="background-color: #6351ce;">
        <div class="container">

            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0">@lang('site.footer.social_networks_message')</h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-7 text-center text-md-right">

                    <!-- Facebook -->
                    <a class="fb-ic">
                        <i class="fab fa-facebook-f white-text mr-4"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic">
                        <i class="fab fa-twitter white-text mr-4"> </i>
                    </a>
                    <!-- Google +-->
                    <a class="gplus-ic">
                        <i class="fab fa-google-plus-g white-text mr-4"> </i>
                    </a>
                    <!--Linkedin -->
                    <a class="li-ic">
                        <i class="fab fa-linkedin-in white-text mr-4"> </i>
                    </a>
                    <!--Instagram-->
                    <a class="ins-ic">
                        <i class="fab fa-instagram white-text"> </i>
                    </a>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row-->

        </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-4 col-xl-3 mx-auto mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">@lang('site.footer.about_us')</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>@lang('site.about.about_us_text')</p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">@lang('site.footer.contact_us')</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fas fa-home mr-3"></i>@lang('site.footer.tehran_iran')</p>
                <p>
                    <i class="fas fa-envelope mr-3"></i> info@tinyblog.ir</p>
                <p>
                    <i class="fas fa-phone mr-3"></i> + 98 903 123 45</p>
                <p>
                    <i class="fas fa-print mr-3"></i> + 98 903 123 54</p>

            </div>
            <!-- Grid column -->


            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">@lang('site.footer.language')</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <br>
                <form action="/lang" method="post">
                    @csrf
                    <button name="lang" value="en" style="background-color: #6351ce;" class="btn btn-sm">
                        <i class="fas fa-lg fa-language mr-3"></i>
                        English, US
                    </button>
                    <button name="lang" value="fa" style="background-color: #6351ce;" class="btn btn-sm">
                        <i class="fas fa-lg fa-language mr-3"></i>
                        فارسی, Persian
                    </button>

                </form>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">@lang('site.footer.payment')</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <br>

                <style>#zarinpal {
                        margin: auto
                    }

                    #zarinpal img {
                        width: 80px;
                    }</style>
                <div id="zarinpal">
                    <script src="https://www.zarinpal.com/webservice/TrustCode" type="text/javascript"></script>
                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© {{now()->year}} @lang('site.footer.copyright')
        <a class="text-warning" href="/">@lang('site.footer.brand')</a>
    </div>
    <!-- Copyright -->

</footer>
{{--Footer--}}
