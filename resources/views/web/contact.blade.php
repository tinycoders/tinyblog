@extends('web.layouts.master')

@section('title', 'Tiny Blog - Contact Us Page')

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection

@section('content')

    <section class="mb-4 wow fadeIn">
        <h3 class="font-weight-bold text-center my-5">@lang('site.contact.title')</h3>

        <div class="container mt-5 mb-5">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        @endforeach
                    </ul>
                </div>
            @elseif(session()->get('send_contact_status')==true)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    @lang('site.contact.send_success')
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{--{{$request->session()->forget('send_contact_status')}}--}}
            @endif
            <form action="{{route('contact')}}" method="post">
                @csrf

                <!-- Grid row -->
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-12 mb-4">

                        <div class="input-group md-form form-sm form-3 pl-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text purple-gradient  white-text" id="basic-addon8">1</span>
                            </div>
                            <input name="name"  minlength="5" maxlength="50" type="text"
                                   class="input_darkmode_handler form-control mt-0 black-border rgba-white-strong"
                                   placeholder="@lang('site.contact.name')" aria-describedby="basic-addon9">
                        </div>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-6 mb-4">

                        <div class="input-group md-form form-sm form-3 pl-0">

                            <div class="input-group-prepend">
                                <span class="input-group-text purple-gradient  white-text" id="basic-addon9">2</span>
                            </div>
                            <input name="email" type="email"
                                   class="input_darkmode_handler form-control mt-0 black-border rgba-white-strong"
                                   placeholder="@lang('site.contact.email')" aria-describedby="basic-addon9">
                        </div>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-6 mb-4">

                        <div class="input-group md-form form-sm form-3 pl-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text purple-gradient  white-text" id="basic-addon10">3</span>
                            </div>
                            <input name="phone" type="tel"
                                   class="input_darkmode_handler form-control mt-0 black-border rgba-white-strong"
                                   placeholder="@lang('site.contact.phone')" aria-describedby="basic-addon9">
                        </div>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

                <!-- Grid row -->
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-12 mt-1">

                        <div class="input-group md-form form-sm form-3 pl-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text purple-gradient  white-text" id="basic-addon10">4</span>
                            </div>

                            @if (app()->getLocale()=='en')

                                <input name="subject" type="text"
                                       class="text-left input_darkmode_handler form-control mt-0 black-border rgba-white-strong"
                                       placeholder="@lang('site.contact.subject')" aria-describedby="basic-addon9">

                            @elseif(app()->getLocale()=='fa')
                                <input name="subject" type="text"
                                       class="text-right input_darkmode_handler form-control mt-0 black-border rgba-white-strong"
                                       placeholder="@lang('site.contact.subject')" aria-describedby="basic-addon9">

                            @endif
                        </div>

                    </div>
                    <!-- Grid column -->

                    <div class="col-12 mt-1">

                        <div class="form-group basic-textarea rounded-corners">

                            @if (app()->getLocale()=='en')

                                <textarea  type="text" name="message" minlength="10" class="text-left textarea_darkmode_handler form-control" id="message" rows="14"
                                           placeholder="@lang('site.contact.message_desc')"></textarea>

                            @elseif(app()->getLocale()=='fa')
                                <textarea  type="text" name="message" minlength="10" class="text-right textarea_darkmode_handler form-control" id="message" rows="14"
                                           placeholder="@lang('site.contact.message_desc')"></textarea>

                            @endif

                        </div>

                        <div class="text-right">
                            <button class="btn purple-gradient btn-block">@lang('site.contact.submit')</button>
                        </div>

                    </div>

                </div>
                <!-- Grid row -->

            </form>

        </div>

    </section>


@endsection


@section('footer')
    @include('web.layouts.footer')
@endsection
