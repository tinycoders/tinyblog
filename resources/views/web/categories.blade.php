@extends('web.layouts.master')

@section('title', 'Tiny Blog - Categories Page')

@section('header')
    <header class="header">
        @include('web.layouts.header')
        @include('web.layouts.intro')
    </header>
@endsection

@section('content')

    <section>
        <div class="container-fluid">

            <div class="container mt-5 pt-3">

                @if (app()->getLocale()=='en')

                    <h1 class="title font-weight-bold text-left"
                        style="font-size: xxx-large;">@lang('site.categories.categories_list')</h1>

                @elseif(app()->getLocale()=='fa')
                    <h1 class="title font-weight-bold text-left"
                        style="font-size:xxx-large;">@lang('site.categories.categories_list')</h1>

                @endif
                <hr class="my-5">
                <div class="row mt-4 pt-3">
                    <div class="col-lg-9 col-12 mt-1">
                        <div class="row">

                            <!-- Card -->
                            @foreach($categories as $category)
                                <div class="col-md-3 col-sm-6 col-12 mb-4">
                                    <a href="{{route('category',[mb_strtolower($category->slug,'UTF-8')])}}">
                                        <div class="card card-image"
                                             style="background-image: url(./images/photo8.jpg); border-radius: 30px;">

                                            <!-- Content -->
                                            <div
                                                class="text-white text-center d-flex align-items-center rgba-black-light py-5 px-4"
                                                style="border-radius: 30px;">
                                                <div>
                                                    <h3 class="card-title pt-2">
                                                        @if (app()->getLocale()=='en')
                                                            <strong class="p_darkmode_handler">{{ucwords($category->name)}}</strong>
                                                        @elseif(app()->getLocale()=='fa')
                                                            <strong class="p_darkmode_handler">{{$category->name_fa}}</strong>
                                                        @endif
                                                    </h3>
                                                </div>
                                            </div>

                                        </div>

                                    </a>
                                </div>
                        @endforeach
                        <!-- Card -->

                        </div>
                    </div>

                    <div class="col-lg-3 col-12 mt-1 wow fadeIn" data-wow-delay="0.2s">
                        @include('web.layouts.sidebar')
                    </div>

                </div>
            </div>
        </div>
    </section>



@endsection

@section('footer')
    @include('web.layouts.footer')
@endsection
