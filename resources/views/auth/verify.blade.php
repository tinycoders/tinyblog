@extends('web.layouts.master')

@section('title', 'Tiny Blog - Verify Page')

@section('header')
    @include('web.layouts.header')
    @include('web.layouts.intro')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 pt-5">
            <div class="card">
                <div class="card-header">@lang('site.verify.title')</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            @lang('site.verify.message')
                        </div>
                    @endif

                        @lang('site.verify.hint_1')
                        @lang('site.verify.hint_2')
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">@lang('site.verify.message_submit')</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
