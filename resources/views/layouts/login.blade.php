<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>پنل ادمین</title>
    <link rel="stylesheet" href="{{mix("/css/login.css")}}">
    <script src="{{mix("/js/lib.js")}}"></script>
    <script src="{{mix("/js/login.js")}}"></script>
</head>

<body>
    <img src="/images/bg-login.jpg" class="bg-img" alt="">
    <div id="particles-js"></div>
    <div class="card">
        <div class="top-overflow rounded aqua-gradient">
            <h5>ورود به پنل ادمین</h5>
        </div>
        <div class="card-body">
            <form action="#" id="loginForm">
                <div class="md-form">
                    <input type="email" id="email" class="form-control">
                    <label for="form1">ایمیل</label>
                </div>
                <div class="md-form">
                    <input type="password" id="password" class="form-control">
                    <label for="form1">رمز عبور</label>
                </div>
                <div class="bottom-overflow">
                    <button class="btn-floating btn-lg btn-default blue-gradient border-0"><i
                            class="fas fa-check"></i></button>
                </div>
            </form>

        </div>

    </div>
</body>

</html>