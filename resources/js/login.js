import auth from "./../react/admin/utils/auth";
if (auth.checkAuth()) {
    location.href = "/admin";
}

$(document).ready(function() {
    initParticles();
    initLoginForm();
});

const initParticles = () => {
    if (
        !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
        )
    ) {
        particlesJS.load("particles-js", "/json/particles-config.json");
    }
};
const initLoginForm = () => {
    const form = document.querySelector("#loginForm");
    form.addEventListener("submit", async e => {
        e.preventDefault();
        const email = document.querySelector("#email").value;
        const password = document.querySelector("#password").value;
        const res = await auth.login(email, password);
        if (res) {
            location.href = "/admin";
        }
    });
};
