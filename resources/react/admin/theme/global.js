import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
body{
    background:${({ theme }) => theme.global.background} !important;
}

.my-content *{
    transition: all 0.2s ease-in-out;
}

.card{
    background-color: ${({ theme }) => theme.card.background} !important;
    box-shadow: 0 2px 5px 0 ${({ theme }) =>
        theme.card.shadowOne}, 0 2px 10px 0 ${({ theme }) =>
    theme.card.shadowTwo} !important;
}

.card ul small,.card ul i,.card h6{
    color: ${({ theme }) => theme.global.color} !important;
}

.card .card-header{
    background-color:${({ theme }) => theme.global.lightDark};
    border-bottom:${({ theme }) => theme.cardHeader.borderBottom};
}

.upload-zone{
    border-color:${({ theme }) => theme.uploadZone.borderColor};
    color: ${({ theme }) => theme.global.color};
}

.md-form input{
    color: ${({ theme }) => theme.global.color} !important;
}

.md-form label{
    color: ${({ theme }) => theme.global.formLabel};
}

.switch + span{
    color: ${({ theme }) => theme.global.color};
}

.switch .switch-content{
    background-color:${({ theme }) => theme.global.lightDark};
}

.css-yk16xz-control, .css-1pahdxg-control{
    background-color:${({ theme }) => theme.global.background} !important;
}
.css-1uccc91-singleValue{
    color: ${({ theme }) => theme.global.color} !important;
}

.border-text{
    color: ${({ theme }) => theme.global.color} !important;
    background-color:${({ theme }) => theme.global.background} !important;
}

.fr-toolbar,.second-toolbar{
    background-color:${({ theme }) => theme.global.background} !important;
}

.fr-wrapper{
    background: transparent !important;
}

.fr-wrapper .fr-element.fr-view{
    color: ${({ theme }) => theme.global.color};
}

.fr-buttons.fr-tabs{
    background:${({ theme }) => theme.froalaEditor.tabs} !important;
}

.fr-toolbar svg path,.fr-modal-close svg path,.fr-svg path{
    fill: ${({ theme }) => theme.froalaEditor.iconColor} !important;
}
.fr-command.fr-btn.fr-dropdown::after{
    border-top-color: ${({ theme }) => theme.froalaEditor.iconColor} !important;

}

.fr-desktop .fr-command:hover:not(.fr-table-cell), .fr-desktop .fr-command:focus:not(.fr-table-cell), .fr-desktop .fr-command.fr-btn-hover:not(.fr-table-cell), .fr-desktop .fr-command.fr-expanded:not(.fr-table-cell){
     background: ${({ theme }) => theme.froalaEditor.buttonHover} !important; 
}

.fr-toolbar .fr-command.fr-btn.fr-open:not(:hover):not(:focus):not(:active),.fr-btn-active-popup {
    background: ${({ theme }) => theme.froalaEditor.buttonClicked} !important;
}
.fr-more-toolbar,.fr-more-toolbar.fr-expanded{
    background: ${({ theme }) => theme.froalaEditor.buttonClicked} !important;
}
.fr-btn.fr-active,.fr-popup.fr-desktop.fr-rtl.fr-active,.fr-file-upload-layer.fr-layer.fr-active,.fr-image-upload-layer.fr-layer.fr-active,.fr-video-upload-layer.fr-layer.fr-active{
    background: ${({ theme }) => theme.froalaEditor.tabContent} !important; 
}
.fr-command.fr-btn.fr-active,.fr-command.fr-btn.fr-active.fr-active-tab,.fr-toolbar .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active, .fr-toolbar .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active-tab, .fr-popup .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active, .fr-popup .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active-tab, .fr-modal .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active, .fr-modal .fr-tabs .fr-command.fr-btn:not(:hover):not(:focus).fr-active-tab {
    background: ${({ theme }) => theme.froalaEditor.activeTab} !important; 
}
.fr-image-upload-layer.fr-layer.fr-active,.fr-file-upload-layer.fr-layer.fr-active,.fr-video-upload-layer.fr-layer.fr-active{
    color:${({ theme }) => theme.froalaEditor.iconColor} !important;
}
.fr-dropdown-menu{
    background: ${({ theme }) => theme.global.background} !important; 
    color:${({ theme }) => theme.global.color} !important;
}

.fr-input-line input,textarea{
    background:${({ theme }) => theme.froalaEditor.inputBackground} !important;
    color:${({ theme }) => theme.froalaEditor.inputColor} !important;
    transition: all 0.2s ease-in-out !important;
}

textarea{
    transition: background 0.2s ease-in-out !important;
}

.fr-input-line label{
    background:${({ theme }) =>
        theme.froalaEditor.inputLabelBackground} !important;
    color:${({ theme }) => theme.froalaEditor.inputLabelColor} !important;
}

.fr-checkbox-line label{
    color:${({ theme }) => theme.froalaEditor.inputColor} !important;
}

.fr-checkbox svg path{
    fill:#fff !important;
}
.fr-command,a.fr-command{
    color: ${({ theme }) => theme.global.color} !important;
}

.fr-modal-wrapper{
    background-color: ${({ theme }) => theme.card.background} !important;
}

.fr-modal-wrapper .fr-modal-head{
    background-color:${({ theme }) => theme.global.lightDark} !important;
    color:${({ theme }) => theme.global.color};
}

.fr-modal-body svg path{
    fill: #212121 !important;
}

.fr-floating-btn{
    background:${({ theme }) => theme.froalaEditor.floatingBtn} !important;
}
.fr-box.fr-rtl.fr-basic.fr-top{
    background-color:${({ theme }) => theme.global.background} !important;
}

.gender{
    background:${({ theme }) => theme.gender.background} !important;
}

.category span{
    color: ${({ theme }) => theme.global.color} !important;
}

.sidebar{
    background:${({ theme }) => theme.sidebar.background};
    border-left:${({ theme }) => theme.sidebar.border};
    transition: right 0.5s linear, background 0.2s ease-in-out;

}

.menu-link{
    color: ${({ theme }) => theme.sidebar.linkDefault};
}
.menu-link:hover{
    color: ${({ theme }) => theme.sidebar.linkDefault};
}
.menu-item .menu-link + .submenu .menu-link{
    color: ${({ theme }) => theme.sidebar.submenuLinkDefault};
}

.menu-item > .menu-link{
    background: ${({ theme }) => theme.sidebar.linkBackground};
}

.menu-item .submenu .menu-link{
    background: ${({ theme }) => theme.sidebar.submenuLinkBackground};
}

.menu-item .menu-link + .submenu .menu-link.active,
.submenu .menu-link.active i{
    color: ${({ theme }) => theme.sidebar.submenuLinkActive};
}

.menu-item .menu-link + .submenu .menu-link.active:hover{
    color: ${({ theme }) => theme.sidebar.submenuLinkActive} !important;
}

.user-info{
    color: ${({ theme }) => theme.sidebar.linkDefault};
}
@media screen and (min-width: 768px){
    .sidebar{
        transition: width 0.5s linear, background 0.2s ease-in-out;
    }
    .menu-item .menu-link + .submenu .menu-link.active::before {
        background: ${({ theme }) =>
            theme.sidebar.submenuLinkActive} !important;
    }
}

.btn-search i{
    color: ${({ theme }) => theme.global.color};
}

.selectPerPage{
    color: ${({ theme }) => theme.global.color} !important;
    background-color:${({ theme }) => theme.global.background} !important;
}

.comment{
    background:${({ theme }) => theme.global.background};
}
.comment-header, .comment-body{
    color:${({ theme }) => theme.global.color};
}
.comment-body{
    background:${({ theme }) => theme.commentBody} !important;
}

.card-header i{
    color:${({ theme }) => theme.global.color};
}

.table{
    color:${({ theme }) => theme.global.color};
    transition: none !important;
}

.table tr,.table td,.table th{
    transition: none !important;
}

.dashboard h3{
    color:${({ theme }) => theme.global.color};
    transition: none !important;
}

.table.table-hover tbody tr:hover {
    background:${({ theme }) => theme.table.hover} !important;
}
.table-hover tbody tr:hover{
    color:${({ theme }) => theme.global.color} !important;
}

.dashboard-body{
    color:${({ theme }) => theme.global.color};
}

hr{
    border-top-color:${({ theme }) => theme.hr} !important;
}
`;
