export const lightTheme = {
    global: {
        background: "#fff",
        color: "#212529",
        formLabel: "#757575",
        lightDark: "rgba(0,0,0,.03)"
    },
    card: {
        background: "#fff",
        shadowOne: "rgba(0,0,0,0.16)",
        shadowTwo: "rgba(0,0,0,0.12)"
    },
    cardHeader: {
        borderBottom: "1px solid rgba(0,0,0,.125)"
    },
    uploadZone: {
        borderColor: "#252525"
    },
    froalaEditor: {
        iconColor: "#333333",
        buttonHover: "#ebebeb",
        buttonClicked: "#fdfdfd",
        tabs: "#f5f5f5",
        tabContent: "#fff",
        inputBackground: "#fff",
        inputColor: "#212529",
        inputLabelBackground: "#fff",
        inputLabelColor: "grey",
        activeTab: "#fff",
        floatingBtn: "#fff"
    },
    gender: {
        background: "#fff"
    },
    sidebar: {
        background: "#fff",
        submenuLinkActive: "#25476a",
        submenuLinkDefault: "#252525",
        submenuLinkBackground: "#f6f6f6",
        linkBackground: "#fff",
        linkDefault: "#252525",
        border: "none"
    },
    commentBody: "whitesmoke",
    table: {
        hover: "rgba(0,0,0,0.075)"
    },
    hr: "rgba(0,0,0,.1)"
};

export const darkTheme = {
    global: {
        background: "#252525",
        color: "#fff",
        formLabel: "#ced4da",
        lightDark: "#303030"
    },
    card: {
        background: "#252525",
        shadowOne: "rgba(255,255,255,0.16)",
        shadowTwo: "rgba(255,255,255,0.12)"
    },
    cardHeader: {
        borderBottom: "1px solid #fff"
    },
    uploadZone: {
        borderColor: "#fff"
    },
    froalaEditor: {
        iconColor: "#fff",
        buttonHover: "#303030",
        buttonClicked: "#303030",
        tabs: "#303030",
        tabContent: "#252525",
        inputBackground: "#252525",
        inputColor: "#fff",
        inputLabelBackground: "#252525",
        inputLabelColor: "#ccc",
        activeTab: "#252525",
        floatingBtn: "#303030"
    },
    gender: {
        background: "#303030"
    },
    sidebar: {
        background: "#303030",
        submenuLinkActive: "#fff",
        submenuLinkDefault: "#fff",
        submenuLinkBackground: "#454545",
        linkBackground: "#252525",
        linkDefault: "#fff",
        border: "1px solid #fff"
    },
    commentBody: "#303030",
    table: {
        hover: "#303030"
    },
    hr: "#ccc"
};
