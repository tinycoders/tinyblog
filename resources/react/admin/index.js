import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import * as Sentry from "@sentry/browser";
import Application from "./Application";
import auth from "./utils/auth";

if (!auth.checkAuth()) {
    location.href = "/admin/login";
} else {
    auth.axios.setAuthorizationToken(auth.getToken());
    auth.axios.defaults.baseURL = "/graphql/admin";

    Sentry.init({
        dsn: "https://0f5a4a9a96744fedbcf5bfbe03ba5244@sentry.io/1861402"
    });

    if (document.getElementById("app")) {
        ReactDOM.render(
            <BrowserRouter>
                <Application />
            </BrowserRouter>,
            document.getElementById("app")
        );
    }
}
