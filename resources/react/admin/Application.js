import React from "react";
import NavBar from "./components/common/navBar";
import SideBar from "./components/common/sideBar";
import Content from "./components/common/content";
import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from "./theme/theme";
import { GlobalStyle } from "./theme/global";

const { useState } = React;
const Application = props => {
    const [theme, setTheme] = useState("light");
    return (
        <ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
            <GlobalStyle />
            <NavBar
                theme={theme}
                onThemeChange={() =>
                    setTheme(theme === "light" ? "dark" : "light")
                }
            />
            <div className="sidebar">
                <SideBar />
            </div>
            <main className="my-content">
                <Content />
            </main>
        </ThemeProvider>
    );
};

export default Application;
