import React from "react";
import Dropzone from "react-dropzone";

const imgSize = 10000000;
const imgAcceptedTypes = "image/jpeg,image/jpeg,image/svg+xml,image/png";
const imgAcceptedTypesArray = imgAcceptedTypes
    .split(",")
    .map(mime => mime.trim());

const isValid = files => {
    const currentFile = files[0];
    const currentFileType = currentFile.type;
    const currentFileSize = currentFile.size;
    if (currentFileSize > imgSize) return false;

    if (!imgAcceptedTypesArray.includes(currentFileType)) return false;
    return true;
};

const CustomDropZone = ({
    cover: imgSrc,
    placeHolder,
    onFileUpload,
    className
}) => {
    const handleDrop = (acceptedFiles, rejectedFiles) => {
        if (rejectedFiles.length > 0) if (!isValid(rejectedFiles)) return;

        if (acceptedFiles.length > 0) {
            if (!isValid(acceptedFiles)) return;
        } else return;
        onFileUpload(acceptedFiles[0]);
    };

    return (
        <Dropzone
            onDrop={handleDrop}
            multiple={false}
            accept={imgAcceptedTypes}
            maxSize={imgSize}
        >
            {({ getRootProps, getInputProps }) => (
                <div
                    {...getRootProps()}
                    className={`upload-zone mb-3 ${className}`}
                >
                    <input {...getInputProps()} />
                    {imgSrc != "" ? (
                        <img
                            src={imgSrc}
                            alt="Error"
                            className="upload-zone-selected"
                        />
                    ) : (
                        <span>{placeHolder}</span>
                    )}
                </div>
            )}
        </Dropzone>
    );
};

export default CustomDropZone;
