import React from "react";
import "froala-editor/js/froala_editor.pkgd.min.js";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import "froala-editor/js/plugins.pkgd.min.js";
import FroalaEditor from "react-froala-wysiwyg";
import Swal from "sweetalert2";
import auth from "./../utils/auth";

const CustomFroalaEditor = ({ model, onModelChange, placeHolder = "" }) => {
    return (
        <FroalaEditor
            tag="textarea"
            model={model}
            onModelChange={onModelChange}
            config={{
                placeholderText: placeHolder,
                direction: "rtl",
                fontFamily: {
                    "Arial,Helvetica,sans-serif": "Arial",
                    "Georgia,serif": "Georgia",
                    "Impact,Charcoal,sans-serif": "Impact",
                    "Tahoma,Geneva,sans-serif": "Tahoma",
                    "'Times New Roman',Times,serif": "Times New Roman",
                    Vazir: "Vazir",
                    "Verdana,Geneva,sans-serif": "Verdana"
                },
                fontFamilySelection: true,
                fileUploadURL: `/api/file/upload?token=${auth.getToken()}`,
                fileUploadParam: {},
                imageUploadURL: `/api/image/upload?token=${auth.getToken()}`,
                videoUploadURL: `/api/video/upload?token=${auth.getToken()}`,
                videoResponsive: true,
                heightMin: 200,
                imageManagerDeleteMethod: "DELETE",
                imageManagerDeleteURL: `/api/image/delete?token=${auth.getToken()}`,
                imageManagerLoadURL: `/api/images?token=${auth.getToken()}`,
                events: {
                    "image.uploaded": function(response) {
                        Swal.fire({
                            title: "هوراااا",
                            text: "عکس با موفقیت آپلود شد.",
                            icon: "success",
                            confirmButtonText: "باشه"
                        });
                    },
                    "image.error": function(error, response) {
                        Swal.fire({
                            title: "وای نه",
                            text: `وای نه! خطایی رخ داد. کد خطا: ${error.code}`,
                            icon: "error",
                            confirmButtonText: "باشه"
                        });
                    },

                    "video.uploaded": function(response) {
                        Swal.fire({
                            title: "هوراااا",
                            text: "ویدئو با موفقیت آپلود شد.",
                            icon: "success",
                            confirmButtonText: "باشه"
                        });
                    },
                    "video.error": function(error, response) {
                        Swal.fire({
                            title: "وای نه",
                            text: `وای نه! خطایی رخ داد. کد خطا: ${error.code}`,
                            icon: "error",
                            confirmButtonText: "باشه"
                        });
                    },

                    "file.uploaded": function(response) {
                        Swal.fire({
                            title: "هوراااا",
                            text: "فایل با موفقیت آپلود شد.",
                            icon: "success",
                            confirmButtonText: "باشه"
                        });
                    },
                    "file.error": function(error, response) {
                        Swal.fire({
                            title: "وای نه",
                            text: `وای نه! خطایی رخ داد. کد خطا: ${error.code}`,
                            icon: "error",
                            confirmButtonText: "باشه"
                        });
                    }
                }
            }}
        />
    );
};

export default CustomFroalaEditor;
