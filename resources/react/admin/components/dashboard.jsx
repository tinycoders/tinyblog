import React, { Component } from "react";
import anime from "animejs/lib/anime.es.js";
import axios from "./../utils/httpRequest";
import ReactHtmlParser from "react-html-parser";
import Loading from "./common/loading";
import { Line } from "react-chartjs-2";
import { getDashboardData } from "./../utils/queries";
import ExportCSV from "./common/exportCSV";

class Dashboard extends Component {
    state = {
        users: [],
        articles: [],
        comments: [],
        usersChart: {},
        articlesChart: {},
        loading: true
    };
    usersCount = React.createRef();
    articlesCount = React.createRef();
    commentsCount = React.createRef();
    categoriesCount = React.createRef();
    async componentDidMount() {
        const { data } = await axios.post("", getDashboardData());
        const { dashboard } = data.data;
        anime({
            targets: this.usersCount.current,
            innerHTML: [0, dashboard.usersCount],
            round: 1,
            easing: "easeInOutExpo",
            duration: 2000
        });
        anime({
            targets: this.articlesCount.current,
            innerHTML: [0, dashboard.articlesCount],
            round: 1,
            easing: "easeInOutExpo",
            duration: 2000
        });
        anime({
            targets: this.commentsCount.current,
            innerHTML: [0, dashboard.commentsCount],
            round: 1,
            easing: "easeInOutExpo",
            duration: 2000
        });
        anime({
            targets: this.categoriesCount.current,
            innerHTML: [0, dashboard.categoriesCount],
            round: 1,
            easing: "easeInOutExpo",
            duration: 2000
        });
        this.setState({
            users: dashboard.users,
            articles: dashboard.articles,
            comments: dashboard.comments,
            usersChart: dashboard.usersChart,
            articlesChart: dashboard.articlesChart,
            loading: false
        });
    }
    renderUI = () => {
        const {
            usersChart,
            articlesChart,
            users,
            articles,
            comments
        } = this.state;
        return (
            <div className="dashboard">
                <div className="row">
                    <div className="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-4">
                        <div className="card">
                            <div className="dashboard-header aqua-gradient">
                                <span ref={this.usersCount}>0</span>
                            </div>
                            <div className="card-body dashboard-body">
                                <span>کاربر</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-4">
                        <div className="card">
                            <div className="dashboard-header blue-gradient">
                                <span ref={this.articlesCount}>0</span>
                            </div>
                            <div className="card-body dashboard-body">
                                <span>مقاله</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-4">
                        <div className="card">
                            <div className="dashboard-header purple-gradient">
                                <span ref={this.commentsCount}>0</span>
                            </div>
                            <div className="card-body dashboard-body">
                                <span>کامنت</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-4">
                        <div className="card">
                            <div className="dashboard-header peach-gradient">
                                <span ref={this.categoriesCount}>0</span>
                            </div>
                            <div className="card-body dashboard-body">
                                <span>دسته بندی</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-3">
                    {usersChart && (
                        <div className="col-12 col-lg-6">
                            <Line
                                data={{
                                    labels: usersChart.labels,
                                    datasets: [
                                        {
                                            label: "تعداد کاربران ثبت نام شده",
                                            data: usersChart.values,
                                            backgroundColor:
                                                "rgba(144,202,249 ,0.2)",
                                            borderColor: "rgba(13,71,161 ,0.5)",
                                            borderWidth: 3
                                        }
                                    ]
                                }}
                                options={{
                                    legend: {
                                        labels: {
                                            fontFamily: "Vazir"
                                        }
                                    },
                                    responsive: true,
                                    maintainAspectRatio: true
                                }}
                            />
                        </div>
                    )}
                    {articlesChart && (
                        <div className="col-12 col-lg-6">
                            <Line
                                data={{
                                    labels: articlesChart.labels,
                                    datasets: [
                                        {
                                            label: "تعداد مقالات ثبت شده",
                                            data: articlesChart.values,
                                            backgroundColor:
                                                "rgba(165,214,167 ,0.2)",
                                            borderColor: "rgba(27,94,32 ,0.5)",
                                            borderWidth: 3
                                        }
                                    ]
                                }}
                                options={{
                                    legend: {
                                        labels: {
                                            fontFamily: "Vazir"
                                        }
                                    },
                                    responsive: true,
                                    maintainAspectRatio: true
                                }}
                            />
                        </div>
                    )}
                </div>
                {users.length > 0 ? (
                    <React.Fragment>
                        <hr />
                        <div className="mt-3">
                            <h3>آخرین کاربران ثبت نام شده</h3>
                            <div className="table-responsive">
                                <table className="table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>نام</th>
                                            <th>ایمیل</th>
                                            <th>نوع کاربر</th>
                                            <th>وضعیت</th>
                                            <th>تاریخ ثبت نام</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {users.map((user, index) => (
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{user.name}</td>
                                                <td>{user.email}</td>
                                                <td>{user.role_fa}</td>
                                                <td>{user.status_fa}</td>
                                                <td>{user.created_at}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    ""
                )}
                {articles.length > 0 ? (
                    <React.Fragment>
                        <hr />
                        <div className="mt-3">
                            <h3>آخرین مقالات ثبت شده</h3>
                            <div className="table-responsive">
                                <table className="table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>عنوان</th>
                                            <th>نوشته</th>
                                            <th>وضعیت</th>
                                            <th>تاریخ ثبت</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {articles.map((article, index) => (
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{article.title}</td>
                                                <td>{article.user.name}</td>
                                                <td>{article.status_fa}</td>
                                                <td>{article.created_at}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    ""
                )}
                {comments.length > 0 ? (
                    <React.Fragment>
                        <hr />
                        <div className="mt-3">
                            <h3>آخرین کامنت ها</h3>
                            <div className="table-responsive">
                                <table className="table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>کامنت</th>
                                            <th>کاربر</th>
                                            <th>مقاله</th>
                                            <th>وضعیت</th>
                                            <th>تاریخ ثبت</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {comments.map((comment, index) => (
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>
                                                    {ReactHtmlParser(
                                                        comment.comment
                                                    )}
                                                </td>
                                                <td>
                                                    {comment.user.role_fa}/{" "}
                                                    {comment.user.name}
                                                </td>
                                                <td>{comment.article.title}</td>
                                                <td>{comment.status_fa}</td>
                                                <td>{comment.created_at}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    ""
                )}
            </div>
        );
    };

    render() {
        const { loading, users } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-2">
                    <div className="card-header d-flex justify-content-between align-items-center py-1">
                        <h6 className="mb-0">داشبورد</h6>
                        <div>
                            <ExportCSV fileName="users" />
                        </div>
                    </div>
                    <div className="card-body">{this.renderUI()}</div>
                </div>
            </React.Fragment>
        );
    }
}

export default Dashboard;
