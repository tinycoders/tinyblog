import React from "react";

const Input = ({
    type = "text",
    name,
    label = "",
    className = "",
    error = "",
    labelClassName = "",
    value,
    ...rest
}) => {
    return (
        <React.Fragment>
            <input
                type={type}
                id={name}
                name={name}
                className={`form-control ${className}`}
                value={value}
                {...rest}
            />
            {label && (
                <label
                    htmlFor={name}
                    className={`${value ? "cactive" : ""} ${labelClassName}`}
                >
                    {label}
                </label>
            )}
            {error && <p className="error-text">{error}</p>}
        </React.Fragment>
    );
};

export default Input;
