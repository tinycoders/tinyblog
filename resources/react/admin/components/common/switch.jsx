import React from "react";

const Switch = ({
    content,
    name,
    onChange,
    size = "",
    className = "",
    checked = false,
    value = "",
    type = "checkbox"
}) => {
    return (
        <div className={`d-flex align-items-center flex-wrap ${className}`}>
            <div className={`switch ${size !== "" ? `switch-${size}` : ""}`}>
                <span className="switch-content"></span>
                <input
                    type={type}
                    name={name}
                    onChange={onChange}
                    checked={checked}
                    value={value}
                />
                <span className="switch-button"></span>
            </div>
            <span>{content}</span>
        </div>
    );
};

export default Switch;
