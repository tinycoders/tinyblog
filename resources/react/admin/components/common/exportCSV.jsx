import React from "react";
import axios from "./../../utils/httpRequest";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import { getUsersArticles } from "./../../utils/queries";

const ExportCSV = ({ fileName }) => {
    let [loading, setLoading] = React.useState(false);
    const fileType =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";

    const exportToCSV = async fileName => {
        if (loading) return;
        setLoading(true);
        const { data: res } = await axios.post("", getUsersArticles());
        const { usersArticles } = res.data;
        const articles = usersArticles.articles.map(article => {
            return {
                ...article,
                user: article.user.name,
                email: article.user.email
            };
        });
        const users = usersArticles.users;
        const usersSheet = XLSX.utils.json_to_sheet(users);
        const articlesSheet = XLSX.utils.json_to_sheet(articles);
        formatUsersSheetData(usersSheet);
        formatArticlesSheetData(articlesSheet);

        const wb = {
            Sheets: { کاربران: usersSheet, مقالات: articlesSheet },
            SheetNames: ["کاربران", "مقالات"]
        };
        const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
        setLoading(false);
    };
    const formatUsersSheetData = us => {
        for (const ai in us) {
            switch (us[ai].v) {
                case "name":
                    us[ai].v = "نام";
                    break;
                case "email":
                    us[ai].v = "ایمیل";
                    break;
                case "status_fa":
                    us[ai].v = "وضعیت کاربر";
                    break;
                case "role_fa":
                    us[ai].v = "نوع کاربر";
                    break;
                case "created_at":
                    us[ai].v = "تاریخ ثبت نام";
                    break;
            }
        }
    };

    const formatArticlesSheetData = ars => {
        for (const ai in ars) {
            switch (ars[ai].v) {
                case "title":
                    ars[ai].v = "عنوان";
                    break;
                case "user":
                    ars[ai].v = "کاربر ثبت کننده";
                    break;
                case "email":
                    ars[ai].v = "ایمیل کاربر ثبت کننده";
                    break;
                case "status_fa":
                    ars[ai].v = "وضعیت انتشار";
                    break;
                case "created_at":
                    ars[ai].v = "تاریخ ثبت";
                    break;
            }
        }
    };
    return (
        <button
            type="button"
            className="btn btn-flat waves-effect nav-btn rounded-circle"
            onClick={() => exportToCSV(fileName)}
        >
            {loading ? (
                <i className="fas fa-sync-alt"></i>
            ) : (
                <i className="fas fa-file-excel"></i>
            )}
        </button>
    );
};

export default ExportCSV;
