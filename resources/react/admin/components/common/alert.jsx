import React from "react";

const Alert =({type,content,className})=>{
  return <div className={`alert alert-${type} ${className}`}>{content}</div>
};

export default Alert;
