import React from "react";
import auth from "./../../utils/auth";

const NavBar = ({ theme, onThemeChange }) => {
    return (
        <nav className="navbar navbar-dark primary-color d-flex justify-content-between align-items-center sticky-top">
            <div className="hamburger">
                <i className="fas fa-bars"></i>
                <i className="fas fa-times"></i>
            </div>

            <div className="d-flex justify-content-center align-items-center">
                <button
                    type="button"
                    className="btn btn-flat waves-effect waves-light nav-btn rounded-circle text-white"
                    onClick={onThemeChange}
                    title="تغییر تم"
                >
                    {theme === "light" ? (
                        <i className="fas fa-moon"></i>
                    ) : (
                        <i className="fas fa-sun"></i>
                    )}
                </button>

                <button
                    type="button"
                    className="btn btn-flat waves-effect waves-light nav-btn rounded-circle text-white"
                    title="خروج"
                    onClick={auth.logout}
                >
                    <i className="fas fa-sign-out-alt"></i>
                </button>
            </div>
        </nav>
    );
};

export default NavBar;
