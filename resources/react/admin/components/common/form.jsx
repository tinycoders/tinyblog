import React, { Component } from "react";

class Form extends Component {
    handleOnInputChange = ({ currentTarget: input }) => {
        const data = { ...this.state.data };
        data[input.name] =
            input.type === "checkbox" || input.type === "radio"
                ? input.checked
                : input.value;
        console.log({ data })
        this.setState({ data });
    };

    handleOnMultiSelectChange = (name, data) => {
        const sData = { ...this.state.data };
        if (data != null) sData[name] = data;
        else sData[name] = "";
        this.setState({ data: sData });
    };

    validate = () => {
        const { error } = this.schema.validate(this.state.data, {
            abortEarly: false
        });
        const errors = {};
        if (error) {
            for (const detail of error.details) {
                errors[detail.path[0]] = detail.message;
            }
        }
        this.setState({ errors });
        return error ? false : true;
    };

    handleSubmit = e => {
        e.preventDefault();
        if (!this.validate()) return;
        this.onSubmit(e);
    };

    onSubmit = e => {};
}

export default Form;
