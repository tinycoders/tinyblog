import React, { Component } from "react";
import { Link } from "react-router-dom";
import auth from "./../../utils/auth";
class SideBar extends Component {
    state = {
        items: [
            {
                _id: 1,
                title: "پیشخوان",
                to: "/admin",
                icon: "fas fa-chart-line",
                submenu: []
            },
            {
                _id: 2,
                title: "مقالات",
                to: "#",
                icon: "fas fa-newspaper",
                submenu: [
                    {
                        _id: "2-1",
                        title: "افزودن مقاله",
                        to: "/admin/article",
                        icon: "fas fa-plus"
                    },
                    {
                        _id: "2-2",
                        title: "لیست مقالات",
                        to: "/admin/articles",
                        icon: "fas fa-list"
                    }
                ]
            },
            {
                _id: 3,
                title: "کاربران",
                to: "#",
                icon: "fas fa-users",
                submenu: [
                    {
                        _id: "3-1",
                        title: "افزودن کاربر",
                        to: "/admin/user",
                        icon: "fas fa-plus"
                    },
                    {
                        _id: "3-2",
                        title: "لیست کاربران",
                        to: "/admin/users",
                        icon: "fas fa-address-book"
                    }
                ]
            },
            {
                _id: 4,
                title: "کامنت ها",
                to: "/admin/comments",
                icon: "fas fa-comments",
                submenu: []
            },
            {
                _id: 5,
                title: "دسته بندی ها",
                to: "/admin/categories",
                icon: "fas fa-book",
                submenu: []
            },
            {
                _id: 6,
                title: "تراکنش ها",
                to: "/admin/transactions",
                icon: "fas fa-money-bill-wave",
                submenu: []
            },
            {
                _id: 7,
                title: "تنظیمات",
                to: "/admin/settings",
                icon: "fas fa-cog",
                submenu: []
            }
        ],

        selectedItem: null,
        selectedSub: null
    };

    constructor(props) {
        super(props);
        const to = location.pathname;
        const items = this.state.items;
        let selectedItem = null,
            selectedSub = null;

        for (let item of items) {
            if (item.to != "#") {
                if (to == item.to) {
                    if (item.submenu.length > 0) selectedItem = item._id;
                    else selectedSub = item._id;
                    break;
                }
            } else {
                for (let sub of item.submenu) {
                    if (to == sub.to) {
                        selectedSub = sub._id;
                        selectedItem = item._id;
                        break;
                    }
                }
            }
        }

        this.state = { ...this.state, selectedItem, selectedSub };
    }

    componentDidMount() {
        const hamburger = document.querySelector(".hamburger");
        const sidebar = document.querySelector(".sidebar");
        hamburger.addEventListener("click", () => {
            hamburger.classList.toggle("open");
            sidebar.classList.toggle("open");
        });
    }

    handleSideBarItemChange = item => {
        if (item._id === this.state.selectedItem) {
            this.setState({
                selectedItem: this.state.selectedItem ? null : item._id
            });
            return;
        }
        this.setState({
            selectedItem: item._id
        });
    };

    handleSubItemChange = sub => {
        if (sub._id === this.state.selectedSub) return;
        this.setState({ selectedSub: sub._id });
    };

    render() {
        const { items, selectedItem, selectedSub } = this.state;
        const user = auth.getUser();
        return (
            <ul className="menu">
                <li className="menu-item">
                    <a href="#" className="user-info">
                        <img
                            className="user-avatar"
                            src={user.avatar}
                            alt="Avatar"
                        />
                        <span>{user.name}</span>
                    </a>
                </li>
                {items.map(item => (
                    <li key={item._id} className={"menu-item"}>
                        <Link
                            onClick={() =>
                                item.submenu.length > 0
                                    ? this.handleSideBarItemChange(item)
                                    : this.handleSubItemChange(item)
                            }
                            to={item.to}
                            className={`menu-link ${
                                item._id == selectedItem ||
                                item._id == selectedSub
                                    ? "active"
                                    : ""
                            }`}
                        >
                            <span>
                                <i className={item.icon}></i>
                                {item.title}
                            </span>
                            {item.submenu.length > 0 ? (
                                <i className="fas fa-chevron-left"></i>
                            ) : (
                                ""
                            )}
                        </Link>
                        {item.submenu.length > 0 ? (
                            <ul className="submenu">
                                {item.submenu.map(sub => (
                                    <li key={sub._id}>
                                        <Link
                                            onClick={() =>
                                                this.handleSubItemChange(sub)
                                            }
                                            to={sub.to}
                                            className={`menu-link ${
                                                sub._id == selectedSub
                                                    ? "active"
                                                    : ""
                                            }`}
                                        >
                                            <i className={sub.icon}></i>
                                            {sub.title}
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        ) : (
                            ""
                        )}
                    </li>
                ))}
            </ul>
        );
    }
}

export default SideBar;
