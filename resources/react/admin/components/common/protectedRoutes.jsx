import React from "react";
import { Route } from "react-router-dom";
import auth from "./../../utils/auth";

const ProtectedRoute = ({ path, component: Component, ...rest }) => {
    return (
        <Route
            path={path}
            {...rest}
            render={props => {
                if (!auth.checkAuth()) {
                    location.href = "/admin/login";
                    return null;
                } else return <Component {...props} />;
            }}
        />
    );
};

export default ProtectedRoute;
