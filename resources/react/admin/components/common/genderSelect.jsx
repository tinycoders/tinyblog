import React from "react";
import Select from "react-select";

const GenderSelect = ({ options, value, onChange }) => {
    return (
        <div className="gender mx-auto mb-4">
            <img src={value.src} alt={value.label} />
            <div className="position-relative w-50">
                <span className="border-text">جنسیت</span>
                <Select
                    options={options}
                    value={value}
                    className="w-100"
                    onChange={data => onChange(data)}
                />
            </div>
        </div>
    );
};

export default GenderSelect;
