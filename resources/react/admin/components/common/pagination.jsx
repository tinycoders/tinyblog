import React from "react";
import { paginate } from "./../../utils/paginate";

const Pagination = ({ total, perPage, currentPage, onPageChange }) => {
    let pageCount = Math.ceil(total / perPage);
    if (pageCount === 1) return null;
    pageCount = paginate(currentPage, pageCount);
    return (
        <ul className="pagination pg-blue d-flex justify-content-center mb-0 p-0">
            {pageCount.map((page, index) =>
                page === "..." ? (
                    <li key={index} className="page-item">
                        <button
                            type="button"
                            className="page-link waves-effect"
                        >
                            {page}
                        </button>
                    </li>
                ) : (
                    <li
                        key={index}
                        className={`page-item ${
                            currentPage === page ? "active" : ""
                        }`}
                    >
                        <button
                            className="page-link waves-effect"
                            onClick={() => onPageChange(page)}
                            type="button"
                        >
                            {page}
                        </button>
                    </li>
                )
            )}
        </ul>
    );
};

export default Pagination;
