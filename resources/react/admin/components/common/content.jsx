import React from "react";
import {Switch} from "react-router-dom";
import ProtectedRoute from "./protectedRoutes";
import Dashboard from "./../dashboard";
import Articles from "../articles/articles";
import AddArticle from "./../articles/addArticle";
import EditArticle from "./../articles/editArticle";
import Categories from "./../categories/categories";
import Users from "./../users/users";
import AddUser from "../users/addUser";
import EditUser from "../users/editUser";
import Comments from "./../comments/comments";
import Transactions from "../transactions";
import Settings from "../settings";

const Content = props => {
    return (
        <Switch>
            <ProtectedRoute path={"/admin/articles"} component={Articles}/>
            <ProtectedRoute
                path={"/admin/article/:id"}
                component={EditArticle}
            />
            <ProtectedRoute path={"/admin/article"} component={AddArticle}/>
            <ProtectedRoute path={"/admin/users"} component={Users}/>
            <ProtectedRoute path={"/admin/user/:id"} component={EditUser}/>
            <ProtectedRoute path={"/admin/user"} component={AddUser}/>
            <ProtectedRoute path={"/admin/comments"} component={Comments}/>
            <ProtectedRoute path={"/admin/categories"} component={Categories}/>
            <ProtectedRoute path={"/admin/transactions"} component={Transactions}/>
            <ProtectedRoute path={"/admin/settings"} component={Settings}/>
            <ProtectedRoute path={"/admin"} component={Dashboard}/>
        </Switch>
    );
};

export default Content;
