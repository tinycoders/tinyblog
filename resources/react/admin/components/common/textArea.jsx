import React from "react";

const TextArea = ({
    name,
    label = "",
    className = "",
    error = "",
    labelClassName = "",
    value,
    ...rest
}) => {
    return (
        <React.Fragment>
            <textarea
                id={name}
                name={name}
                className={`md-textarea form-control ${className}`}
                value={value}
                {...rest}
            ></textarea>
            {label && (
                <label
                    htmlFor={name}
                    className={`${value ? "cactive" : ""} ${labelClassName}`}
                >
                    {label}
                </label>
            )}
            {error && <p className="error-text">{error}</p>}
        </React.Fragment>
    );
};

export default TextArea;
