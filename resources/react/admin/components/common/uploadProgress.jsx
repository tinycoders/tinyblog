import React from "react";

const UploadProgress = ({ percent }) => {
    return (
        <div
            className="progress md-progress w-100"
            style={{ height: 20 + "px" }}
        >
            <div
                className="progress-bar"
                role="progressbar"
                style={{
                    width: percent + "%",
                    height: 20 + "px"
                }}
            >
                {percent}%
            </div>
        </div>
    );
};

export default UploadProgress;
