import React from "react";

const Category = ({ item, onEdit, onDelete }) => {
    return (
        <div className="row mx-0 mb-2">
            <div className="col-12 col-sm-6 d-flex justify-content-between align-items-center border rounded shadow-sm category">
                <span>
                    {item.name_fa} ({item.name})
                </span>
                <div className="d-flex justify-content-center align-items-center">
                    <button
                        type="button"
                        className="border-0 btn-floating btn-sm mr-2 ml-0 blue-gradient waves-effect waves-light"
                        onClick={() => onEdit(item)}
                    >
                        <i className="fas fa-edit"></i>
                    </button>
                    <button
                        type="button"
                        className="border-0 btn-floating btn-sm ml-2 peach-gradient waves-effect waves-light"
                        onClick={() => onDelete(item)}
                    >
                        <i className="fas fa-trash-alt"></i>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Category;
