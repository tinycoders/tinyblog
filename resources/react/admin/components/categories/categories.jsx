import React from "react";
import Joi from "joi-persian";
import Swal from "sweetalert2";
import axios from "./../../utils/httpRequest";
import Form from "./../common/form";
import Alert from "./../common/alert";
import Category from "./category";
import Input from "./../common/input";
import Loading from "../common/loading";
import {
    getCategories,
    addEditCategory,
    deleteCategory
} from "./../../utils/queries";

class Categories extends Form {
    state = {
        categories: [],
        data: {
            categoryName: "",
            categoryNameFa: ""
        },
        errors: {},
        edit: null,
        loading: true
    };

    schema = Joi.object({
        categoryName: Joi.string()
            .min(3)
            .max(50)
            .required()
            .label("نام دسته بندی (انگلیسی)"),
        categoryNameFa: Joi.string()
            .min(3)
            .max(50)
            .required()
            .label("نام دسته بندی (فارسی)")
    });

    async componentDidMount() {
        const {data} = await axios.post("", getCategories());
        const categories = data.data.categories;
        this.setState({categories, loading: false});

    }

    onSubmit = async () => {
        try {
            const {edit: id, data, categories: allCategories} = this.state;
            this.setState({loading: true});
            const {data: res} = await axios.post(
                "",
                addEditCategory(
                    data.categoryName,
                    data.categoryNameFa,
                    id ? id : null
                )
            );
            const category = res.data.AECategory;
            if (category) {
                let categories = [...allCategories];
                if (id) {
                    Swal.fire("نتیجه", "دسته بندی با موفقیت ویرایش شد", "success");
                    categories = categories.map(cat =>
                        category.id === cat.id ? category : cat
                    );
                } else {
                    Swal.fire("نتیجه", "دسته بندی با موفقیت ایجاد شد", "success");
                    categories.push(category);
                }
                this.setState({
                    categories
                });
            }
            this.handleOnNotEdit();
        } catch (e) {
            console.log(e);
        }
        this.setState({loading: false});
    };

    handleOnEdit = item => {
        const data = {categoryName: item.name, categoryNameFa: item.name_fa};
        const edit = item.id;
        this.setState({data, edit, errors: {}});
    };

    handleOnNotEdit = () => {
        this.setState({
            data: {categoryName: "", categoryNameFa: ""},
            edit: null,
            errors: {}
        });
    };

    handleOnDelete = item => {
        Swal.fire({
            title: "آیا مطمئنید؟",
            text: "این دسته بندی حذف بشه؟",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "آره، حذفش کن",
            cancelButtonText: "بی خیالش"
        }).then(result => {
            if (result.value) {
                this.deleteItem(item);
            }
        });
    };

    deleteItem = async item => {
        this.setState({loading: true});
        const {edit, categories: allCategories} = this.state;
        if (edit) this.handleOnNotEdit();
        const {data} = await axios.post("", deleteCategory(item.id));
        const res = data.data.DCategory;
        if (res) {
            const categories = allCategories.filter(cat => cat.id !== item.id);
            this.setState({categories});
            Swal.fire(
                "حذف شد!",
                "دسته بندی مورد نظر با موفقیت حذف شد",
                "success"
            );
        }
        this.setState({loading: false});
    };

    renderTopUI = () => {
        const {data, errors, edit} = this.state;
        return (
            <form action="#" className="row mb-3" onSubmit={this.handleSubmit}>
                <div
                    className="col-12 col-md-3 mb-3 mb-md-0 d-flex flex-column justify-content-around align-items-center">
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="categoryNameFa"
                            label="نام دسته بندی (فارسی)"
                            onChange={this.handleOnInputChange}
                            value={data.categoryNameFa}
                            error={errors["categoryNameFa"]}
                        />
                    </div>
                </div>
                <div
                    className="col-12 col-md-3 mb-3 mb-md-0 d-flex flex-column justify-content-around align-items-center">
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="categoryName"
                            label="نام دسته بندی (انگلیسی)"
                            onChange={this.handleOnInputChange}
                            value={data.categoryName}
                            error={errors["categoryName"]}
                        />
                    </div>
                </div>
                <div className="col-12 col-md-3 mt-2 d-flex justify-content-center">
                    <button className="btn aqua-gradient btn-rounded add-btn small-btn w-50 w-md-100">
                        {edit ? (
                            <React.Fragment>
                                <i className="fas fa-edit"></i>ویرایش
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <i className="fas fa-plus"></i>افزودن
                            </React.Fragment>
                        )}
                    </button>
                </div>
                {edit && (
                    <div className="col-12 col-md-3 mt-2 d-flex justify-content-center">
                        <button
                            onClick={this.handleOnNotEdit}
                            type="button"
                            className="btn burning-gradient btn-rounded add-btn small-btn w-50 w-md-100"
                        >
                            <i className="fas fa-times"></i>لغو
                        </button>
                    </div>
                )}
            </form>
        );
    };

    renderUI = () => {
        const {categories} = this.state;
        if (categories.length === 0) {
            return (
                <Alert
                    content="دسته بندی ای جهت نمایش یافت نشد"
                    type="warning"
                    className="mb-0"
                />
            );
        }
        return categories.map(category => (
            <Category
                key={category.id}
                item={category}
                onEdit={this.handleOnEdit}
                onDelete={this.handleOnDelete}
            />
        ));
    };

    render() {
        const {loading} = this.state;
        return (
            <React.Fragment>
                {loading && <Loading/>}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">دسته بندی ها</h6>
                    </div>
                    <div className="card-body p-2">
                        {this.renderTopUI()}
                        {this.renderUI()}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Categories;
