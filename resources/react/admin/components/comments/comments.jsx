import React, { Component } from "react";
import Swal from "sweetalert2";
import axios from "./../../utils/httpRequest";
import Alert from "./../common/alert";
import Pagination from "./../common/pagination";
import Comment from "./comment";
import Switch from "./../common/switch";
import Loading from "./../common/loading";
import Input from "./../common/input";
import {
    getComments,
    confirmComment,
    deleteComment,
    replyComment,
    editComment
} from "./../../utils/queries";

class Comments extends Component {
    state = {
        data: {
            commentContent: ""
        },
        comments: [],
        selectedFilter: "all",
        currentPage: 1,
        perPage: 30,
        total: 0,
        search: "",
        searching: false,
        edit: null,
        reply: null,
        loading: true
    };

    getCommentsFromServer = async (
        page = 1,
        limit = 30,
        search = "",
        filter = "all"
    ) => {
        this.setState({ loading: true });
        const { data: commentD } = await axios.post(
            "",
            getComments(page, limit, search, filter)
        );
        const commentsData = JSON.parse(commentD.data.comments);
        const comments = commentsData.data;
        const currentPage = commentsData.current_page;
        const perPage = commentsData.per_page;
        const total = commentsData.total;
        this.setState({
            comments,
            currentPage,
            perPage,
            total,
            loading: false
        });
    };

    componentDidMount() {
        this.getCommentsFromServer();
    }

    handleCommentDelete = comment => {
        Swal.fire({
            title: "آیا مطمئنید؟",
            text: "این کامنت حذف بشه؟",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "آره، حذفش کن",
            cancelButtonText: "بی خیالش"
        }).then(result => {
            if (result.value) {
                this.deleteItem(comment);
            }
        });
    };

    deleteItem = async comment => {
        this.setState({ loading: true });
        const { data } = await axios.post("", deleteComment(comment.id));
        if (data.data.DComment) {
            const { currentPage } = this.state;
            const comments = this.state.comments.filter(
                c => c.id !== comment.id
            );
            Swal.fire("حذف شد!", "کامنت مورد نظر با موفقیت حذف شد", "success");
            if (currentPage > 1 && comments.length === 0)
                this.handlePageChange(currentPage - 1);
            else {
                const { perPage, search, selectedFilter } = this.state;
                await this.getCommentsFromServer(
                    currentPage,
                    perPage,
                    search,
                    selectedFilter
                );
            }
        }
        this.setState({ loading: false });
    };

    handleCommentReply = async comment => {
        const {
            reply: id,
            data: sData,
            currentPage,
            perPage,
            search,
            selectedFilter
        } = this.state;
        if (id === comment.id) {
            this.setState({ loading: true });
            await axios.post(
                "",
                replyComment(comment.id, sData.commentContent)
            );
            await this.getCommentsFromServer(
                currentPage,
                perPage,
                search,
                selectedFilter
            );
            this.setState({
                reply: null,
                edit: null,
                data: { commentContent: "" },
                loading: false
            });
            return;
        }
        this.setState({
            reply: comment.id,
            edit: null,
            data: { commentContent: "" }
        });
    };

    handleCommentEdit = async comment => {
        this.setState({ loading: true });
        const {
            edit: id,
            data,
            currentPage,
            perPage,
            search,
            selectedFilter
        } = this.state;
        if (id === comment.id) {
            await axios.post("", editComment(comment.id, data.commentContent));
            await this.getCommentsFromServer(
                currentPage,
                perPage,
                search,
                selectedFilter
            );
            this.setState({
                reply: null,
                edit: null,
                data: { commentContent: "" },
                loading: false
            });
            return;
        }
        this.setState({
            edit: comment.id,
            reply: null,
            data: { commentContent: comment.comment },
            loading: false
        });
    };

    handleCommentNotEditReply = () => {
        this.setState({
            reply: null,
            edit: null,
            data: { commentContent: "" }
        });
    };

    handleCommentChange = data => {
        this.setState({ data: { commentContent: data } });
    };

    handleCommentConfirmation = async comment => {
        this.setState({ loading: true });
        const { currentPage, perPage, search, selectedFilter } = this.state;
        await axios.post("", confirmComment(comment.id));
        await this.getCommentsFromServer(
            currentPage,
            perPage,
            search,
            selectedFilter
        );
        this.setState({ loading: false });
    };

    handleOnPageChange = page => {
        const { perPage, search, selectedFilter } = this.state;
        this.getCommentsFromServer(page, perPage, search, selectedFilter);
        this.setState({ currentPage: page });
    };

    handleOnSearchChange = ({ currentTarget: input }) => {
        this.setState({ search: input.value });
        if (input.value === "" && this.state.searching) {
            this.getCommentsFromServer(
                1,
                this.state.perPage,
                "",
                this.state.selectedFilter
            );
            this.setState({ searching: false });
        }
    };

    handleKeyPress = ({ keyCode }) => {
        if (keyCode === 13 && this.state.search !== "") {
            this.getCommentsFromServer(
                1,
                this.state.perPage,
                this.state.search,
                this.state.selectedFilter
            );
            this.setState({ searching: true });
        }
    };

    handleSearchClick = () => {
        const { search, perPage, selectedFilter } = this.state;
        if (search !== "") {
            this.getCommentsFromServer(1, perPage, search, selectedFilter);
            this.setState({ searching: true });
        }
    };

    handleFilterChange = ({ currentTarget: input }) => {
        const { search, perPage } = this.state;
        this.setState({ selectedFilter: input.value });
        this.getCommentsFromServer(1, perPage, search, input.value);
    };

    renderTopUI = () => {
        const { search } = this.state;
        return (
            <div className="row mb-3">
                <div className="col-12 col-md-6 mb-3 mb-md-0">
                    <div className="md-form m-0 w-100" dir="ltr">
                        <Input
                            type="text"
                            name="search"
                            placeholder="متن خود را جهت جست و جو وارد نمایید..."
                            value={search}
                            onChange={this.handleOnSearchChange}
                            onKeyUp={this.handleKeyPress}
                        />
                        <button
                            type="button"
                            className="btn btn-flat btn-search waves-effect"
                            onClick={this.handleSearchClick}
                        >
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div className="col-12 col-md-6 mb-3 d-flex flex-wrap justify-content-around align-items-center">
                    <Switch
                        content="همه"
                        checked={this.state.selectedFilter === "all"}
                        type="radio"
                        value="all"
                        onChange={this.handleFilterChange}
                    />
                    <Switch
                        content="تایید شده ها"
                        checked={this.state.selectedFilter === "confirmed"}
                        type="radio"
                        value="confirmed"
                        onChange={this.handleFilterChange}
                    />
                    <Switch
                        content="تایید نشده ها"
                        checked={this.state.selectedFilter === "not-confirmed"}
                        type="radio"
                        value="not-confirmed"
                        onChange={this.handleFilterChange}
                    />
                </div>
            </div>
        );
    };

    renderUI = () => {
        const { comments, data, edit, reply } = this.state;
        if (comments.length === 0) {
            return (
                <Alert
                    content="کامنتی جهت نمایش وجود ندارد"
                    type="warning"
                    className="mb-0"
                />
            );
        }

        return (
            <div className="row">
                {comments.map(comment => (
                    <Comment
                        key={comment.id}
                        comment={comment}
                        onCommentDelete={this.handleCommentDelete}
                        onCommentReply={this.handleCommentReply}
                        onCommentEdit={this.handleCommentEdit}
                        onCommentConfirmation={this.handleCommentConfirmation}
                        onCommentNotEditReply={this.handleCommentNotEditReply}
                        onCommentChange={this.handleCommentChange}
                        commentContent={data.commentContent}
                        edit={edit}
                        reply={reply}
                    />
                ))}
            </div>
        );
    };

    render() {
        const { currentPage, perPage, total, loading } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">کامنت ها</h6>
                    </div>
                    <div className="card-body p-2">
                        {this.renderTopUI()}
                        {this.renderUI()}
                        <Pagination
                            perPage={perPage}
                            currentPage={currentPage}
                            total={total}
                            onPageChange={this.handleOnPageChange}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Comments;
