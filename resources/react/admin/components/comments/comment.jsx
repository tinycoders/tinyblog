import React from "react";
import CustomFroalaEditor from "./../../libs/customFroalaEditor";
import ReactHtmlParser from "react-html-parser";

const Comment = ({
    comment,
    onCommentDelete,
    onCommentReply,
    onCommentEdit,
    onCommentConfirmation,
    onCommentNotEditReply,
    onCommentChange,
    edit,
    reply,
    commentContent
}) => {
    return (
        <div className="col-12 mb-3">
            <div className="comment">
                <div className="comment-header d-flex flex-wrap">
                    <p className="ml-3 mb-0">
                        {comment.user.role === 1 ? "ادمین" : "کاربر"}:{" "}
                        {comment.user.name}
                    </p>
                    {comment.article && (
                        <p className="ml-3 mb-0">
                            مقاله: {comment.article.title}
                        </p>
                    )}
                    <p className="ml-3 mb-0">تاریخ ثبت: {comment.created_at}</p>
                </div>
                <div className="comment-body">
                    {edit && edit === comment.id ? (
                        <div>
                            <CustomFroalaEditor
                                model={commentContent}
                                onModelChange={onCommentChange}
                            />
                            <button
                                type="button"
                                className="btn btn-sm btn-danger"
                                onClick={onCommentNotEditReply}
                            >
                                لغو
                            </button>
                        </div>
                    ) : (
                        ReactHtmlParser(comment.comment)
                    )}
                    {reply && reply === comment.id ? (
                        <div className="mt-3 pr-5">
                            <CustomFroalaEditor
                                model={commentContent}
                                placeHolder="پاسخ را وارد کنید..."
                                onModelChange={onCommentChange}
                            />
                            <button
                                type="button"
                                className="btn btn-sm btn-danger"
                                onClick={onCommentNotEditReply}
                            >
                                لغو
                            </button>
                        </div>
                    ) : (
                        ""
                    )}
                    {comment.inner_comment && comment.inner_comment.length > 0
                        ? comment.inner_comment.map(comment => (
                              <Comment
                                  key={comment.id}
                                  comment={comment}
                                  onCommentDelete={onCommentDelete}
                                  onCommentReply={onCommentReply}
                                  onCommentEdit={onCommentEdit}
                                  onCommentConfirmation={onCommentConfirmation}
                                  onCommentNotEditReply={onCommentNotEditReply}
                                  onCommentChange={onCommentChange}
                                  commentContent={commentContent}
                                  edit={edit}
                                  reply={reply}
                              />
                          ))
                        : ""}
                </div>
                <div className="comment-footer">
                    <button
                        type="button"
                        className="btn btn-success btn-sm ml-3"
                        onClick={() => onCommentConfirmation(comment)}
                    >
                        {comment.status ? "عدم تایید" : "تایید"}
                    </button>

                    <button
                        type="button"
                        className="btn btn-primary btn-sm ml-3"
                        onClick={() => onCommentEdit(comment)}
                    >
                        {edit && edit === comment.id ? "ثبت ویرایش" : "ویرایش"}
                    </button>

                    <button
                        type="button"
                        className="btn btn-warning btn-sm ml-3"
                        onClick={() => onCommentReply(comment)}
                    >
                        {reply && reply === comment.id ? "ثبت پاسخ" : "پاسخ"}
                    </button>

                    <button
                        type="button"
                        className="btn btn-danger btn-sm ml-3"
                        onClick={() => onCommentDelete(comment)}
                    >
                        حذف
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Comment;
