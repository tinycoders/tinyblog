import React from "react";
import UserForm from "./userForm";
import Loading from "./../common/loading";

class AddUser extends UserForm {
    render() {
        const { loading } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">افزودن کاربر جدید</h6>
                    </div>
                    <div className="card-body p-2">{this.renderForm()}</div>
                </div>
            </React.Fragment>
        );
    }
}

export default AddUser;
