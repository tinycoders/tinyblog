import React, { Component } from "react";
import Swal from "sweetalert2";
import axios from "./../../utils/httpRequest";
import { getUsers, deleteUser } from "./../../utils/queries";
import Alert from "./../common/alert";
import User from "./user";
import Input from "./../common/input";
import Loading from "./../common/loading";
import Pagination from "./../common/pagination";

class Users extends Component {
    state = {
        users: [],
        currentPage: 1,
        perPage: 10,
        total: 0,
        search: "",
        isSearching: false,
        loading: true
    };

    componentDidMount() {
        this.getUsersFromServer();
    }

    getUsersFromServer = async (
        current_page = 1,
        per_page = 10,
        search = ""
    ) => {
        this.setState({ loading: true });
        const { data } = await axios.post(
            "",
            getUsers(current_page, per_page, search)
        );
        const userData = data.data.users;
        const users = userData.data;
        const currentPage = userData.current_page;
        const perPage = userData.per_page;
        const total = userData.total;
        this.setState({
            users,
            currentPage,
            perPage,
            total,
            loading: false
        });
    };

    handleSearchChange = ({ currentTarget: input }) => {
        this.setState({ search: input.value });
        if (input.value === "" && this.state.isSearching) {
            this.getUsersFromServer();
            this.setState({ isSearching: false });
        }
    };

    handleEnterKey = ({ keyCode }) => {
        if (keyCode === 13) {
            const { perPage, search } = this.state;
            if (search !== "") {
                this.getUsersFromServer(1, perPage, search);
                this.setState({ isSearching: true });
            }
        }
    };

    handleSearch = () => {
        const { perPage, search } = this.state;
        if (search !== "") {
            this.getUsersFromServer(1, perPage, search);
            this.setState({ isSearching: true });
        }
    };

    handlePerPageChange = ({ currentTarget: input }) => {
        this.setState({ perPage: input.value });
        this.getUsersFromServer(1, input.value, this.state.search);
    };

    handleOnDelete = item => {
        Swal.fire({
            title: "آیا مطمئنید؟",
            text:
                "این کاربر حذف بشه؟ حذف این کاربر منجر به حذف تمامی مقالات این کاربر می شود",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "آره، حذفش کن",
            cancelButtonText: "بی خیالش"
        }).then(result => {
            if (result.value) {
                this.setState({ loading: true });
                this.deleteItem(item);
            }
        });
    };

    deleteItem = async item => {
        const { users: allUsers, currentPage } = this.state;
        const { data } = await axios.post("", deleteUser(item.id));
        const res = data.data.DUser;
        if (res) {
            const users = allUsers.filter(user => user.id !== item.id);
            Swal.fire("حذف شد!", "کاربر مورد نظر با موفقیت حذف شد", "success");
            if (currentPage > 1 && users.length == 0)
                this.handleOnPageChange(currentPage - 1);
            else {
                const { perPage, search } = this.state;
                this.getUsersFromServer(currentPage, perPage, search);
            }
        }
        this.setState({ loading: false });
    };

    handleOnPageChange = page => {
        const { perPage, search, currentPage } = this.state;
        if (page === currentPage) return;
        this.getUsersFromServer(page, perPage, search);
    };

    renderTopUI = () => {
        return (
            <div className="row mb-3">
                <div className="col-12 col-sm-9 d-flex align-items-center">
                    <div className="md-form m-0 w-100" dir="ltr">
                        <Input
                            type="text"
                            name="search"
                            placeholder="نام یا ایمیل کاربر را جهت جست و جو وارد کنید..."
                            value={this.state.search}
                            onChange={this.handleSearchChange}
                            onKeyUp={this.handleEnterKey}
                        />
                        <button
                            type="button"
                            className="btn btn-flat btn-search waves-effect"
                            onClick={this.handleSearch}
                        >
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div className="col-12 col-sm-3 d-flex align-items-center">
                    <select
                        className="form-control selectPerPage"
                        name="perPage"
                        id="perPage"
                        defaultValue={this.state.perPage}
                        onChange={this.handlePerPageChange}
                    >
                        <option value="10">10 کاربر در صفحه</option>
                        <option value="30">30 کاربر در صفحه</option>
                        <option value="50">50 کاربر در صفحه</option>
                        <option value="100">100 کاربر در صفحه</option>
                    </select>
                </div>
            </div>
        );
    };

    renderUI = () => {
        const { users } = this.state;
        if (users.length === 0) {
            return (
                <Alert
                    type="warning"
                    content="کاربری جهت نمایش یافت نشد"
                    className="mb-0"
                />
            );
        }
        return (
            <div className="row">
                {users.map(user => (
                    <User
                        key={user.id}
                        item={user}
                        onDelete={this.handleOnDelete}
                    />
                ))}
            </div>
        );
    };

    render() {
        const { loading, perPage, currentPage, total } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">مقالات</h6>
                    </div>
                    <div className="card-body p-2">
                        {this.renderTopUI()}
                        {this.renderUI()}
                        <Pagination
                            perPage={perPage}
                            currentPage={currentPage}
                            total={total}
                            onPageChange={this.handleOnPageChange}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Users;
