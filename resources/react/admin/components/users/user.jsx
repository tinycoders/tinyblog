import React from "react";
import { Link } from "react-router-dom";

const User = ({ item, onDelete }) => {
    return (
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
            <div className="card overflow-hidden user h-100">
                <div className="user-img-container">
                    <img className="user-img-top" src={item.avatar} alt="" />
                </div>
                <div className="btn-action d-flex justify-content-between">
                    <Link
                        to={`/admin/user/${item.id}`}
                        className="btn-floating btn-sm mr-2 ml-0 blue-gradient waves-effect waves-light"
                    >
                        <i className="fas fa-edit"></i>
                    </Link>
                    <button
                        type="button"
                        className="btn-floating btn-sm ml-2 peach-gradient waves-effect waves-light border-0"
                        onClick={() => onDelete(item)}
                    >
                        <i className="fas fa-trash-alt"></i>
                    </button>
                </div>
                <div className="card-body p-2 pt-3">
                    <ul className="list-unstyled d-flex flex-wrap mb-2">
                        <li>
                            <small>
                                <i className="fas fa-newspaper"></i>{" "}
                                {item.articles ? item.articles.length : 0}
                            </small>
                        </li>
                        <li className="mr-3">
                            <small>
                                <i className="fas fa-clock"></i>{" "}
                                {item.created_at}
                            </small>
                        </li>
                    </ul>
                    <h6 style={{ fontWeight: "bold", textAlign: "center" }}>
                        {item.name}
                    </h6>
                    <h6 style={{ fontWeight: "bold", textAlign: "center" }}>
                        {item.email}
                    </h6>
                </div>
            </div>
        </div>
    );
};

export default User;
