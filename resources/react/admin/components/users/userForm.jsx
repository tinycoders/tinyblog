import React from "react";
import Swal from "sweetalert2";
import Select from "react-select";
import Joi from "joi-persian";
import moment from "moment-jalaali";
import DatePicker from "react-datepicker2";
import axios from "./../../utils/httpRequest";
import Input from "./../common/input";
import CustomDropZone from "./../../libs/customDropZone";
import Switch from "./../common/switch";
import Form from "./../common/form";
import GenderSelect from "../common/genderSelect";
import UploadProgress from "../common/uploadProgress";
import {uploadUserAvatar, getUser, addEditUser} from "./../../utils/queries";

class UserForm extends Form {
    state = {
        data: {
            userRole: {value: 2, label: "کاربر"},
            userAvatar: "",
            userFullName: "",
            userGender: {value: 0, label: "مرد", src: "/images/male.png"},
            userEmail: "",
            userPassword: "",
            userConfirmPassword: "",
            userPhone: "",
            userDateBirth: null,
            userStatus: true
        },
        errors: {},
        roles: [
            {value: 1, label: "ادمین"},
            {value: 2, label: "کاربر"}
        ],
        genders: [
            {value: 0, label: "مرد", src: "/images/male.png"},
            {value: 1, label: "زن", src: "/images/female.png"},
            {value: 2, label: "دیگر", src: "/images/other.png"}
        ],
        uploadProgress: 0,
        loading: false
    };

    rules = {
        userAvatar: Joi.allow(),
        userFullName: Joi.string()
            .min(5)
            .max(50)
            .required()
            .label("نام و نام خانوادگی"),
        userEmail: Joi.string()
            .email({tlds: false})
            .required()
            .label("ایمیل"),
        userPassword: Joi.string()
            .min(8)
            .max(20)
            .pattern(new RegExp("^[a-zA-Z0-9]{8,20}$"))
            .required()
            .label("رمز عبور"),
        userConfirmPassword: Joi.string()
            .min(8)
            .max(20)
            .pattern(new RegExp("^[a-zA-Z0-9]{8,20}$"))
            .required()
            .label("تایید رمز عبور"),
        userPhone: Joi.number()
            .empty("")
            .label("شماره موبایل"),
        userDateBirth: Joi.allow(),
        userRole: Joi.allow(),
        userGender: Joi.allow(),
        userStatus: Joi.allow()
    };

    schema = Joi.object(this.rules);

    constructor(props) {
        super(props);
        const {id} = props.match.params;
        this.state = {...this.state, loading: id ? true : false};
    }

    async componentDidMount() {
        const {id} = this.props.match.params;
        if (id) {
            const {data} = await axios.post("", getUser(id));
            this.mapUserToView(data.data.user);
            this.rules.userPassword = Joi.string()
                .min(8)
                .max(20)
                .pattern(new RegExp("^[a-zA-Z0-9]{8,20}$"))
                .empty("")
                .label("رمز عبور");
            this.rules.userConfirmPassword = Joi.string()
                .min(8)
                .max(20)
                .pattern(new RegExp("^[a-zA-Z0-9]{8,20}$"))
                .empty("")
                .label("تایید رمز عبور");
            this.schema = Joi.object(this.rules);
        }
    }

    mapUserToView = user => {
        const {roles, genders, data: sData} = this.state;
        const data = {...sData};
        console.log(user);
        data.userRole = roles.find(role => role.value === user.role);
        data.userGender = genders.find(gender => gender.value === user.gender);
        data.userAvatar = user.avatar;
        data.userFullName = user.name;
        data.userEmail = user.email;
        data.userPhone = user.phone ? user.phone : "";
        data.userStatus = user.status;
        data.userDateBirth = user.date_birth
            ? moment(user.date_birth).add(1, "day")
            : null;
        this.setState({data, loading: false});
    };

    handleFileUpload = async file => {
        const fd = new FormData();
        const map = {0: ["variables.file"]};
        fd.append("operations", JSON.stringify(uploadUserAvatar()));
        fd.append("map", JSON.stringify(map));
        fd.append(0, file, file.name);
        const {data} = await axios.post("", fd, {
            onUploadProgress: e => {
                const uploadProgress = Math.round((e.loaded * 100) / e.total);
                this.setState({uploadProgress});
            }
        });
        const sData = {...this.state.data};
        sData.userAvatar = data.data.uploadUserAvatar;
        this.setState({data: sData});
        setTimeout(() => this.setState({uploadProgress: 0}), 1000);
    };

    handleDateChange = date => {
        const data = {...this.state.data};
        data.userDateBirth = date;
        this.setState({data});
    };

    validatePassword = () => {
        const {data, errors: allErrors} = this.state;
        const password = data.userPassword;
        const c_password = data.userConfirmPassword;
        const errors = {...allErrors};
        if (password != c_password) {
            errors.userConfirmPassword =
                "تایید رمز عبور باید با رمز عبور یکسان باشد";
            this.setState({errors});
            return false;
        }
        return true;
    };

    onSubmit = async () => {
        if (!this.validatePassword()) return;
        const {id} = this.props.match.params;
        const {data: inputData} = this.state;
        this.setState({loading: true});
        const {data} = await axios.post(
            "",
            addEditUser(inputData, id ? id : null)
        );
        const userData = data.data.AEUser;
        this.setState({loading: false});
        if (userData) {
            if (id) {
                Swal.fire("نتیجه", "کاربر با موفقیت ویرایش شد", "success");
                const {history} = this.props;
                history.push("/admin/users");
            } else {
                Swal.fire("نتیجه", "کاربر با موفقیت ایجاد شد", "success");
            }
        }
    };

    renderForm = () => {
        const {data, roles, genders, errors, uploadProgress} = this.state;
        const {id} = this.props.match.params;

        return (
            <form action="#" className="row" onSubmit={this.handleSubmit}>
                <div className="col-12 col-md-6 d-flex align-items-center flex-column">
                    <CustomDropZone
                        placeHolder="آواتار کاربر را درگ و دراپ کنید یا کلیک کنید"
                        cover={data.userAvatar}
                        className="upload-zone-avatar"
                        onFileUpload={this.handleFileUpload}
                    />
                    {uploadProgress ? (
                        <UploadProgress percent={uploadProgress}/>
                    ) : (
                        ""
                    )}

                    <div className="md-form mb-0 w-100">
                        <Input
                            name="userFullName"
                            label="نام و نام خانوادگی"
                            onChange={this.handleOnInputChange}
                            value={data.userFullName}
                            error={errors.userFullName}
                        />
                    </div>
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="userEmail"
                            label="ایمیل"
                            type="email"
                            onChange={this.handleOnInputChange}
                            value={data.userEmail}
                            error={errors.userEmail}
                        />
                    </div>
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="userPassword"
                            label="رمز عبور"
                            type="password"
                            onChange={this.handleOnInputChange}
                            value={data.userPassword}
                            error={errors.userPassword}
                        />
                    </div>
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="userConfirmPassword"
                            label="تایید رمز عبور"
                            type="password"
                            onChange={this.handleOnInputChange}
                            value={data.userConfirmPassword}
                            error={errors.userConfirmPassword}
                        />
                    </div>
                </div>
                <div className="col-12 col-md-6 d-flex align-items-center flex-column">
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="userPhone"
                            label="شماره موبایل (اختیاری)"
                            onChange={this.handleOnInputChange}
                            value={data.userPhone}
                            type="number"
                            error={errors.userPhone}
                        />
                    </div>
                    <div className="md-form mb-0 w-100">
                        <label className="datepicker-label">
                            تاریخ تولد (اختیاری)
                        </label>
                        <DatePicker
                            name="userDateBirth"
                            showTodayButton={false}
                            onChange={date => this.handleDateChange(date)}
                            value={data.userDateBirth}
                            isGregorian={false}
                            timePicker={false}
                        />
                    </div>
                    <div className="my-4 w-100 position-relative">
                        <span className="border-text" style={{zIndex: 3}}>
                            نوع دسترسی کاربر
                        </span>
                        <Select
                            options={roles}
                            value={data.userRole}
                            onChange={data =>
                                this.handleOnMultiSelectChange("userRole", data)
                            }
                            className="z-index-2"
                        />
                    </div>
                    <div className="mb-0 w-100">
                        <GenderSelect
                            options={genders}
                            value={data.userGender}
                            onChange={data =>
                                this.handleOnMultiSelectChange(
                                    "userGender",
                                    data
                                )
                            }
                        />
                    </div>
                    <Switch
                        name="userStatus"
                        content="کاربر فعال است"
                        checked={data.userStatus}
                        onChange={this.handleOnInputChange}
                    />
                </div>

                <div className="col-12 mt-5 d-flex justify-content-center">
                    <button className="btn aqua-gradient btn-rounded add-btn">
                        {id ? (
                            <React.Fragment>
                                <i className="fas fa-edit"></i>ویرایش
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <i className="fas fa-plus"></i>افزودن
                            </React.Fragment>
                        )}
                    </button>
                </div>
            </form>
        );
    };
}

export default UserForm;
