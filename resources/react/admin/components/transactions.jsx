import React, {Component} from 'react';
import Loading from "./common/loading";
import axios from "../utils/httpRequest";
import {getTransactions, getUsersArticles} from "../utils/queries";
import DatePicker from "react-datepicker2";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";

class Transactions extends Component {

    state = {
        loading: true,
        data: {
            createdAtFrom: null,
            createdAtTo: null
        },
        transactions: [],
        exportModal: false
    }

    async componentDidMount() {
        const {data} = await axios.post("", getTransactions());
        const transactions = data.data.transactions;
        console.log(transactions);
        this.setState({transactions, loading: false});
    }

    exportTransaction = async () => {
        this.setState({loading: true});
        const {data} = await axios.post("", getTransactions(this.state.data));
        await this.exportToCSV(data.data.transactions)
        this.setState({loading: false});
    }

    exportToCSV = async transactions => {
        const fileType =
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";

        transactions = transactions.map(t => ({
            id: t.id,
            user: t.user.name,
            article: t.article.title,
            amount: t.amount,
            ref_id: t.ref_id || '-------',
            driver: t.ref_id || '-------',
            status: t.status === 1 ? 'پرداخت شده' : 'پرداخت نشده',
            created_at: t.created_at
        }))

        const transactionsSheet = XLSX.utils.json_to_sheet(transactions);

        this.formatTransactionsSheetData(transactionsSheet);

        const wb = {
            Sheets: {transactions: transactionsSheet},
            SheetNames: ["transactions"]
        };
        const excelBuffer = XLSX.write(wb, {bookType: "xlsx", type: "array"});
        const data = new Blob([excelBuffer], {type: fileType});
        FileSaver.saveAs(data, 'transactions.xlsx');
    };
    formatTransactionsSheetData = us => {
        for (const ai in us) {
            switch (us[ai].v) {
                case "id":
                    us[ai].v = "شناسه";
                    break;
                case "user":
                    us[ai].v = "نام کاربر";
                    break;
                case "article":
                    us[ai].v = "عنوان مقاله";
                    break;
                case "amount":
                    us[ai].v = "مبلغ";
                    break;
                case "ref_id":
                    us[ai].v = "شماره رهگیری";
                    break;
                case "driver":
                    us[ai].v = "درگاه پرداخت";
                    break;
                case "status":
                    us[ai].v = "وضعیت";
                    break;
                case "created_at":
                    us[ai].v = "تاریخ تراکنش";
                    break;
            }
        }
    };


    render() {
        const {loading, transactions, data} = this.state;

        return <React.Fragment>
            {loading && <Loading/>}
            <div className="card my-1">
                <div className="card-header d-flex justify-content-between align-items-center py-1">
                    <h6 className="mb-0">تراکنش ها</h6>
                </div>
                <div className="card-body p-2">
                    <div className="row mb-3">
                        <div className="col-12 col-sm-4 mb-3">
                            <div className="md-form mb-0 w-100">
                                <label className="datepicker-label">
                                    از تاریخ (اختیاری)
                                </label>
                                <DatePicker
                                    showTodayButton={false}
                                    onChange={date => this.setState({data: {...data, createdAtFrom: date}})}
                                    value={data.createdAtFrom}
                                    isGregorian={false}
                                    timePicker={false}
                                />
                            </div>
                        </div>

                        <div className="col-12 col-sm-4 mb-3">
                            <div className="md-form mb-0 w-100">
                                <label className="datepicker-label">
                                    تا تاریخ (اختیاری)
                                </label>
                                <DatePicker
                                    showTodayButton={false}
                                    onChange={date => this.setState({data: {...data, createdAtTo: date}})}
                                    value={data.createdAtTo}
                                    isGregorian={false}
                                    timePicker={false}
                                />
                            </div>
                        </div>
                        <div className="col-12 col-sm-4 d-flex justify-content-center">
                            <button
                                type="button"
                                className="btn aqua-gradient btn-rounded add-btn small-btn w-50"
                                onClick={this.exportTransaction}
                            >
                                <i className="fas fa-file-excel"></i>دریافت خروجی
                            </button>
                        </div>
                    </div>

                    <div className="table-responsive">
                        <table className="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>نام کاربر</th>
                                <th>عنوان مقاله</th>
                                <th>شماره رهگیری</th>
                                <th>درگاه پرداخت</th>
                                <th>وضعیت</th>
                                <th>تاریخ تراکنش</th>
                            </tr>
                            </thead>
                            <tbody>
                            {transactions.map((transaction, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{transaction.user.name}</td>
                                    <td>{transaction.article.title}</td>
                                    <td>{transaction.ref_id || '-----'}</td>
                                    <td>{transaction.driver || '-----'}</td>
                                    <td>{transaction.status ? <span className="text-success">پرداخت موفق</span> :
                                        <span className="text-danger">پرداخت نشده</span>}</td>
                                    <td>{transaction.created_at}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </React.Fragment>
    }
}

export default Transactions;
