import React from 'react';
import Joi from "joi-persian";
import axios from "./../utils/httpRequest";
import Loading from "./common/loading";
import Input from "./common/input";
import Form from "./common/form";
import {updateSettings, getSettings} from "../utils/queries";
import Swal from "sweetalert2";

export default class Settings extends Form {
    state = {
        data: {
            siteTitle: '',
            siteTitleFa: '',
        },
        errors: {},
        loading: true
    };

    schema = Joi.object({
        siteTitle: Joi.string()
            .min(5)
            .max(255)
            .required()
            .label("عنوان سایت"),
        siteTitleFa: Joi.string()
            .min(5)
            .max(255)
            .required()
            .label("عنوان فارسی سایت"),
    });


    componentDidMount = async () => {
        const {data} = await axios.post("", getSettings());
        const jsonData = JSON.parse(data.data.settings);
        this.setState({
            data: jsonData, loading: false
        });
    }

    onSubmit = async e => {
        this.setState({loading: true});
        const {data} = this.state;
        const {data: res} = await axios.post(
            "",
            updateSettings(data)
        );
        const settingsData = res.data.USetting;
        this.setState({loading: false});
        if (settingsData) {
            Swal.fire("نتیجه", "تنظیمات با موفقیت ویرایش شد", "success");
        }
    };


    render() {
        const {loading, errors, data} = this.state;
        return (
            <React.Fragment>
                {loading && <Loading/>}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">تنظیمات</h6>
                    </div>
                    <div className="card-body p-2">
                        <form action="#" className="row" onSubmit={this.handleSubmit}>
                            <div
                                className="col-12 col-md-6 mb-3 mb-md-0 d-flex flex-column justify-content-around align-items-center">
                                <div className="md-form mb-0 w-100">
                                    <Input
                                        name="siteTitle"
                                        label="عنوان سایت"
                                        onChange={this.handleOnInputChange}
                                        value={data.siteTitle}
                                        error={errors["siteTitle"]}
                                    />
                                </div>
                            </div>
                            <div
                                className="col-12 col-md-6 mb-3 mb-md-0 d-flex flex-column justify-content-around align-items-center">
                                <div className="md-form mb-0 w-100">
                                    <Input
                                        name="siteTitleFa"
                                        label="عنوان فارسی سایت"
                                        onChange={this.handleOnInputChange}
                                        value={data.siteTitleFa}
                                        error={errors["siteTitleFa"]}
                                    />
                                </div>
                            </div>

                            <div className="col-12 mt-2 d-flex justify-content-center">
                                <button type="submit" className="btn aqua-gradient btn-rounded add-btn">
                                    <React.Fragment>
                                        <i className="fas fa-edit"></i>ویرایش
                                    </React.Fragment>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
