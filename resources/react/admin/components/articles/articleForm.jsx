import React from "react";
import Swal from "sweetalert2";
import Joi from "joi-persian";
import Select from "react-select";
import axios from "./../../utils/httpRequest";
import Form from "./../common/form";
import Input from "./../common/input";
import TextArea from "./../common/textArea";
import CustomDropZone from "./../../libs/customDropZone";
import Switch from "./../common/switch";
import CustomFroalaEditor from "./../../libs/customFroalaEditor";
import UploadProgress from "../common/uploadProgress";
import {
    uploadArticleCover,
    getCategories,
    addEditArticle,
    getArticle
} from "./../../utils/queries";

class ArticleForm extends Form {
    state = {
        data: {
            articleTitle: "",
            articleCategory: "",
            articleAbstract: "",
            articleBody: "",
            articleCover: "",
            articleIsReleased: true,
            articleIsFree: true,
            articlePrice: '',
        },
        categories: [],
        uploadProgress: 0,
        errors: {},
        loading: true
    };

    schema = Joi.object({
        articleTitle: Joi.string()
            .min(5)
            .max(150)
            .required()
            .label("عنوان مقاله"),
        articleCategory: Joi.array()
            .min(1)
            .required()
            .label("دسته بندی مقاله"),
        articleAbstract: Joi.string()
            .min(5)
            .required()
            .label("چکیده مقاله"),
        articleBody: Joi.string()
            .min(5)
            .required()
            .label("متن مقاله"),
        articleCover: Joi.string()
            .required()
            .label("کاور مقاله"),
        articleIsReleased: Joi.allow(),
        articleIsFree: Joi.allow(),
        articlePrice: Joi.allow(),
    });

    componentDidMount = async () => {
        const { id } = this.props.match.params;
        if (id) {
            const { data: article } = await axios.post("", getArticle(id));
            const articleData = article.data.article;
            if (articleData) {
                this.mapArticleToView(articleData);
            }
        }
        const { data } = await axios.post("", getCategories());
        const categoriesData = data.data.categories;
        let categories = [];
        if (categoriesData) {
            categories = categoriesData.map(cat => {
                return { value: cat.id, label: `${cat.name_fa} (${cat.name})` };
            });
        }
        this.setState({ categories, loading: false });
    };

    mapArticleToView = article => {
        const data = { ...this.state.data };
        data.articleTitle = article.title;
        data.articleCategory = article.categories.map(cat => {
            return { value: cat.id, label: `${cat.name_fa} (${cat.name})` };
        });
        data.articleBody = article.body;
        data.articleCover = article.cover;
        data.articleAbstract = article.abstract;
        data.articleIsReleased = article.status;
        data.articleIsFree = article.is_free;
        data.articlePrice = article.currency || '';
        this.setState({ data });
    };

    handleFileUpload = async file => {
        const fd = new FormData();
        const map = { 0: ["variables.file"] };
        fd.append("operations", JSON.stringify(uploadArticleCover()));
        fd.append("map", JSON.stringify(map));
        fd.append(0, file, file.name);
        const { data } = await axios.post("", fd, {
            onUploadProgress: e => {
                const uploadProgress = Math.round((e.loaded * 100) / e.total);
                this.setState({ uploadProgress });
            }
        });
        const sData = { ...this.state.data };
        sData.articleCover = data.data.uploadArticleCover;
        this.setState({ data: sData });
        setTimeout(() => this.setState({ uploadProgress: 0 }), 1000);
    };

    handleBodyChange = data => {
        const sData = { ...this.state.data };
        sData.articleBody = data;
        this.setState({ data: sData });
    };

    onSubmit = async e => {
        this.setState({ loading: true });
        const { id } = this.props.match.params;
        const { data } = this.state;
        const articleCategory = data.articleCategory.map(cat => cat.value);
        const newData = { ...data, articleCategory };
        const { data: res } = await axios.post(
            "",
            addEditArticle(newData, id ? id : null)
        );
        const articleData = res.data.AEArticle;
        this.setState({ loading: false });
        if (articleData) {
            if (id) {
                Swal.fire("نتیجه", "مقاله با موفقیت ویرایش شد", "success");
                const { history } = this.props;
                history.push("/admin/articles");
            } else {
                Swal.fire("نتیجه", "مقاله با موفقیت ایجاد شد", "success");
            }
        }
    };

    renderForm = () => {
        const { id } = this.props.match.params;
        const { data, uploadProgress, errors, categories } = this.state;
        return (
            <form action="#" className="row" onSubmit={this.handleSubmit}>
                <div className="col-12 col-md-6 mb-3 mb-md-0 d-flex flex-column justify-content-around align-items-center">
                    <div className="md-form mb-0 w-100">
                        <Input
                            name="articleTitle"
                            label="عنوان مقاله"
                            onChange={this.handleOnInputChange}
                            value={data.articleTitle}
                            error={errors["articleTitle"]}
                        />
                    </div>
                    <div className="md-form mb-0 w-100">
                        <Select
                            options={categories}
                            value={data.articleCategory}
                            isMulti
                            placeholder="دسته بندی مقاله"
                            onChange={data =>
                                this.handleOnMultiSelectChange(
                                    "articleCategory",
                                    data
                                )
                            }
                            className="z-index-100"
                        />
                        {errors["articleCategory"] && (
                            <p className="error-text">
                                {errors["articleCategory"]}
                            </p>
                        )}
                    </div>
                    <div className="md-form mb-0 w-100">
                        <TextArea
                            name="articleAbstract"
                            onChange={this.handleOnInputChange}
                            error={errors["articleAbstract"]}
                            label="چکیده مقاله"
                            value={data.articleAbstract}
                        />
                    </div>
                </div>
                <div className="col-12 col-md-6">
                    <CustomDropZone
                        placeHolder="کاور مقاله را درگ و دراپ کنید یا کلیک کنید"
                        cover={data.articleCover}
                        onFileUpload={this.handleFileUpload}
                    />
                    {errors["articleCover"] && (
                        <p className="error-text">{errors["articleCover"]}</p>
                    )}
                    {uploadProgress ? (
                        <UploadProgress percent={uploadProgress} />
                    ) : (
                        ""
                    )}
                    <div className="d-flex justify-content-center align-items-center mt-2">
                        <Switch
                            name="articleIsReleased"
                            content="مقاله منتشر شود"
                            checked={data.articleIsReleased}
                            onChange={this.handleOnInputChange}
                        />
                    </div>
                    <div className="d-flex justify-content-center align-items-center mt-2">
                        <Switch
                            name="articleIsFree"
                            content="مقاله رایگان است"
                            checked={data.articleIsFree}
                            onChange={this.handleOnInputChange}
                        />
                    </div>
                    { !data.articleIsFree && <div className="md-form mb-0 w-100">
                        <Input
                            name="articlePrice"
                            type="number"
                            label="قیمت مقاله"
                            onChange={this.handleOnInputChange}
                            value={data.articlePrice}
                            error={errors["articlePrice"]}
                        />
                    </div>}
                </div>
                <div className="col-12 my-3">
                    <CustomFroalaEditor
                        model={data.articleBody}
                        onModelChange={this.handleBodyChange}
                    />
                    {errors["articleBody"] && (
                        <p className="error-text">{errors["articleBody"]}</p>
                    )}
                </div>
                <div className="col-12 col-md-6 mt-2 d-flex justify-content-center">
                    <button className="btn aqua-gradient btn-rounded add-btn">
                        {id ? (
                            <React.Fragment>
                                <i className="fas fa-edit"></i>ویرایش
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <i className="fas fa-plus"></i>افزودن
                            </React.Fragment>
                        )}
                    </button>
                </div>
            </form>
        );
    };
}

export default ArticleForm;
