import React from "react";
import ArticleForm from "./articleForm";
import Loading from "./../common/loading";

class AddArticle extends ArticleForm {
    render() {
        const { loading } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">افزودن مقاله جدید</h6>
                    </div>
                    <div className="card-body p-2">{this.renderForm()}</div>
                </div>
            </React.Fragment>
        );
    }
}

export default AddArticle;
