import React, { Component } from "react";
import Swal from "sweetalert2";
import axios from "./../../utils/httpRequest";
import { getArticles, deleteArticle } from "../../utils/queries";
import Alert from "../common/alert";
import Article from "./article";
import Input from "../common/input";
import Loading from "./../common/loading";
import Pagination from "../common/pagination";

class Articles extends Component {
    state = {
        articles: [],
        currentPage: 1,
        perPage: 10,
        total: 0,
        search: "",
        isSearching: false,
        loading: true
    };

    componentDidMount() {
        this.getArticlesFromServer();
    }

    getArticlesFromServer = async (
        current_page = 1,
        per_page = 10,
        search = ""
    ) => {
        this.setState({ loading: true });
        const { data } = await axios.post(
            "",
            getArticles(current_page, per_page, search)
        );
        const articleData = data.data.articles;
        const articles = articleData.data;
        const currentPage = articleData.current_page;
        const perPage = articleData.per_page;
        const total = articleData.total;
        this.setState({
            articles,
            currentPage,
            perPage,
            total,
            loading: false
        });
    };

    handleSearchChange = ({ currentTarget: input }) => {
        this.setState({ search: input.value });
        if (input.value === "" && this.state.isSearching) {
            this.getArticlesFromServer();
            this.setState({ isSearching: false });
        }
    };

    handleEnterKey = ({ keyCode }) => {
        if (keyCode === 13) {
            const { perPage, search } = this.state;
            if (search !== "") {
                this.getArticlesFromServer(1, perPage, search);
                this.setState({ isSearching: true });
            }
        }
    };

    handleSearch = () => {
        const { perPage, search } = this.state;
        if (search !== "") {
            this.getArticlesFromServer(1, perPage, search);
            this.setState({ isSearching: true });
        }
    };

    handlePerPageChange = ({ currentTarget: input }) => {
        this.setState({ perPage: input.value });
        this.getArticlesFromServer(1, input.value, this.state.search);
    };

    handleOnDelete = item => {
        Swal.fire({
            title: "آیا مطمئنید؟",
            text: "این مقاله حذف بشه؟",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "آره، حذفش کن",
            cancelButtonText: "بی خیالش"
        }).then(result => {
            if (result.value) {
                this.deleteItem(item);
            }
        });
    };

    deleteItem = async item => {
        this.setState({ loading: true });
        const { articles: allArticles, currentPage } = this.state;
        const { data } = await axios.post("", deleteArticle(item.id));
        const res = data.data.DArticle;
        if (res) {
            const articles = allArticles.filter(
                article => article.id !== item.id
            );
            Swal.fire("حذف شد!", "مقاله مورد نظر با موفقیت حذف شد", "success");
            if (currentPage > 1 && articles.length == 0)
                this.handleOnPageChange(currentPage - 1);
            else {
                const { perPage, search } = this.state;
                this.getArticlesFromServer(currentPage, perPage, search);
            }
        }
        this.setState({ loading: false });
    };

    handleOnPageChange = page => {
        const { perPage, search, currentPage } = this.state;
        if (page === currentPage) return;
        this.getArticlesFromServer(page, perPage, search);
    };

    renderTopUI = () => {
        return (
            <div className="row mb-3">
                <div className="col-12 col-sm-9 d-flex align-items-center">
                    <div className="md-form m-0 w-100" dir="ltr">
                        <Input
                            type="text"
                            name="search"
                            placeholder="متن خود را جهت جست و جو وارد نمایید..."
                            value={this.state.search}
                            onChange={this.handleSearchChange}
                            onKeyUp={this.handleEnterKey}
                        />
                        <button
                            type="button"
                            className="btn btn-flat btn-search waves-effect"
                            onClick={this.handleSearch}
                        >
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div className="col-12 col-sm-3 d-flex align-items-center">
                    <select
                        className="form-control selectPerPage"
                        name="perPage"
                        id="perPage"
                        defaultValue={this.state.perPage}
                        onChange={this.handlePerPageChange}
                    >
                        <option value="10">10 مقاله در صفحه</option>
                        <option value="30">30 مقاله در صفحه</option>
                        <option value="50">50 مقاله در صفحه</option>
                        <option value="100">100 مقاله در صفحه</option>
                    </select>
                </div>
            </div>
        );
    };

    renderUI = () => {
        const { articles } = this.state;
        if (articles.length === 0) {
            return (
                <Alert
                    type="warning"
                    content="مقاله ای جهت نمایش یافت نشد"
                    className="mb-0"
                />
            );
        }
        return (
            <div className="row">
                {articles.map(article => (
                    <Article
                        key={article.id}
                        item={article}
                        onDelete={this.handleOnDelete}
                    />
                ))}
            </div>
        );
    };

    render() {
        const { loading, perPage, currentPage, total } = this.state;
        return (
            <React.Fragment>
                {loading && <Loading />}
                <div className="card my-1">
                    <div className="card-header">
                        <h6 className="card-title mb-0">مقالات</h6>
                    </div>
                    <div className="card-body p-2">
                        {this.renderTopUI()}
                        {this.renderUI()}
                        <Pagination
                            perPage={perPage}
                            currentPage={currentPage}
                            total={total}
                            onPageChange={this.handleOnPageChange}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Articles;
