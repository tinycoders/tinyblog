import React from "react";
import { Link } from "react-router-dom";

const Article = ({ item, onDelete }) => {
    return (
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
            <div className="card overflow-hidden article h-100">
                <div className="article-img-container">
                    <img className="article-img-top" src={item.cover} alt="" />
                </div>
                <div className="btn-action d-flex justify-content-between">
                    <Link
                        to={`/admin/article/${item.id}`}
                        className="btn-floating btn-sm mr-2 ml-0 blue-gradient waves-effect waves-light"
                    >
                        <i className="fas fa-edit"></i>
                    </Link>
                    <button
                        type="button"
                        className="btn-floating btn-sm ml-2 peach-gradient waves-effect waves-light border-0"
                        onClick={() => onDelete(item)}
                    >
                        <i className="fas fa-trash-alt"></i>
                    </button>
                </div>
                <div className="card-body p-2 pt-3">
                    <ul className="list-unstyled d-flex flex-wrap mb-2">
                        <li>
                            <small>
                                <i className="fas fa-user"></i> {item.user.name}
                            </small>
                        </li>
                        <li className="mr-3">
                            <small>
                                <i className="fas fa-comments"></i>{" "}
                                {item.comments ? item.comments.length : 0}
                            </small>
                        </li>
                        <li className="mr-3">
                            <small>
                                <i className="fas fa-clock"></i>{" "}
                                {item.created_at}
                            </small>
                        </li>
                    </ul>
                    <h6 style={{ fontWeight: "bold" }}>{item.title}</h6>
                </div>
            </div>
        </div>
    );
};

export default Article;
