//dashboard
export const getDashboardData = () => {
    const data = {
        query: `query getDashboardData{
            dashboard{
                usersCount,
                articlesCount,
                commentsCount,
                categoriesCount,
                usersChart{
                    values,
                    labels
                },
                articlesChart{
                    values,
                    labels
                },
                users{
                    name,
                    email,
                    role_fa,
                    status_fa,
                    created_at
                },
                articles{
                    title,
                    user{
                        name
                    },
                    status_fa
                    created_at
                },
                comments{
                    comment,
                    user{
                        name,
                        role_fa
                    },
                    article{
                        title
                    },
                    status_fa
                    created_at
                }
            }
        }`,
        variables: {}
    };
    return data;
};

export const getUsersArticles = () => {
    const data = {
        query: `query getUsersArticles{
           usersArticles{
               users{
                   name,
                   email,
                   role_fa,
                   status_fa,
                   created_at
               },
               articles{
                   title,
                   user{
                       name,
                       email
                   }
                   status_fa,
                   created_at
               }
           }
        }`,
        variables: {}
    };
    return data;
};

//categories queries
export const getCategories = () => {
    const data = {
        query: `query getAllCategories{
            categories{
                id,
                name,
                name_fa
            }
        }`,
        variables: {}
    };
    return data;
};

export const addEditCategory = (categoryName, categoryNameFa, id = null) => {
    const data = {
        query: `mutation addEditCategory($id:Int,$categoryName:String!,$categoryNameFa:String!){
            AECategory(id:$id,categoryName:$categoryName,categoryNameFa:$categoryNameFa){
                id,
                name,
                name_fa
            }
        }`,
        variables: {
            id,
            categoryName,
            categoryNameFa
        }
    };
    return data;
};

export const deleteCategory = id => {
    const data = {
        query: `mutation deleteCategory($id:Int!){
            DCategory(id:$id)
        }`,
        variables: {
            id
        }
    };
    return data;
};

//articles queries
export const getArticles = (currentPage = 1, perPage = 10, search = "") => {
    const data = {
        query: `query getAllArticles($page:Int,$limit:Int,$search:String){
            articles(page:$page,limit:$limit,search:$search){
                data{
                    id
                    title,
                    cover,
                    created_at,
                    user{
                        id,
                        name
                    },
                    comments{
                        id
                    }
                }
                current_page,
                per_page,
                total
            }
        }`,
        variables: {
            page: currentPage,
            limit: perPage,
            search
        }
    };
    return data;
};

export const uploadArticleCover = () => {
    const data = {
        query: `mutation uploadArticleCover($file:Upload!){
            uploadArticleCover(file:$file)
        }`,
        variables: {
            file: null
        }
    };
    return data;
};

export const addEditArticle = (inputData, id = null) => {
    const data = {
        query: `mutation addEditArticle($id:Int,$articleTitle:String!,$articleAbstract:String!,$articleCategory:[Int!]!,$articleBody:String!,$articleCover:String!,$articleIsReleased:Boolean!,$articleIsFree:Boolean!,$articlePrice:Int){
            AEArticle(id:$id,articleTitle:$articleTitle,articleAbstract:$articleAbstract,articleCategory:$articleCategory,articleBody:$articleBody,articleCover:$articleCover,articleIsReleased:$articleIsReleased,articleIsFree:$articleIsFree,articlePrice:$articlePrice){
                id
            }
        }`,
        variables: {
            id,
            articleTitle: inputData.articleTitle,
            articleCategory: inputData.articleCategory,
            articleAbstract: inputData.articleAbstract,
            articleBody: inputData.articleBody,
            articleCover: inputData.articleCover,
            articleIsReleased: inputData.articleIsReleased,
            articleIsFree: inputData.articleIsFree,
            articlePrice: inputData.articlePrice,
        }
    };
    return data;
};

export const getArticle = id => {
    const data = {
        query: `query getArticle($id:Int!){
            article(id:$id){
                    id
                    title,
                    cover,
                    abstract,
                    body,
                    created_at,
                    categories{
                        id,
                        name_fa,
                        name
                    },
                    is_free,
                    currency,
                    status
            }
        }`,
        variables: {
            id
        }
    };
    return data;
};

export const deleteArticle = id => {
    const data = {
        query: `mutation deleteArticle($id:Int!){
            DArticle(id:$id)
        }`,
        variables: {
            id
        }
    };
    return data;
};

//users queries
export const uploadUserAvatar = () => {
    const data = {
        query: `mutation uploadUserAvatar($file:Upload!){
            uploadUserAvatar(file:$file)
        }`,
        variables: {
            file: null
        }
    };
    return data;
};

export const getUsers = (currentPage = 1, perPage = 10, search = "") => {
    const data = {
        query: `query getAllUsers($page:Int,$limit:Int,$search:String){
            users(page:$page,limit:$limit,search:$search){
                data{
                    id,
                    avatar,
                    name,
                    email,
                    articles{
                        title
                    },
                    created_at
                }
                current_page,
                per_page,
                total
            }
        }`,
        variables: {
            page: currentPage,
            limit: perPage,
            search
        }
    };
    return data;
};

export const addEditUser = (inputData, id = null) => {
    const data = {
        query: `mutation addEditUser($id:Int,$userRole:Int!,$userAvatar:String,$userFullName:String!,$userGender:Int!,$userEmail:String!,$userPassword:String,$userPassword_confirmation:String,$userPhone:String,$userDateBirth:String,$userStatus:Boolean!){
            AEUser(id:$id,userRole:$userRole,userAvatar:$userAvatar,userFullName:$userFullName,userGender:$userGender,userEmail:$userEmail,userPassword:$userPassword,userPassword_confirmation:$userPassword_confirmation,userPhone:$userPhone,userDateBirth:$userDateBirth,userStatus:$userStatus){
                id
            }
        }`,
        variables: {
            id,
            userRole: inputData.userRole.value,
            userAvatar: inputData.userAvatar,
            userFullName: inputData.userFullName,
            userGender: inputData.userGender.value,
            userEmail: inputData.userEmail,
            userPassword: inputData.userPassword,
            userPassword_confirmation: inputData.userConfirmPassword,
            userPhone: inputData.userPhone,
            userDateBirth: inputData.userDateBirth ? inputData.userDateBirth._d : '',
            userStatus: inputData.userStatus
        }
    };
    return data;
};

export const getUser = id => {
    const data = {
        query: `query getUser($id:Int!){
            user(id:$id){
                id,
                role,
                avatar,
                name,
                gender,
                email,
                phone,
                date_birth,
                status
            }
        }`,
        variables: {
            id
        }
    };
    return data;
};

export const deleteUser = id => {
    const data = {
        query: `mutation deleteUser($id:Int!){
            DUser(id:$id)
        }`,
        variables: {
            id
        }
    };
    return data;
};

export const loginUser = (email, password) => {
    const data = {
        query: `mutation loginUser($email:String,$password:String){
            login(email:$email,password:$password){
                access_token,
                token_type,
                expires_in
            }
        }`,
        variables: {
            email,
            password
        }
    };
    return data;
};

export const logOutUser = () => {
    const data = {
        query: `mutation logOutUser{
            logout
        }`,
        variables: {}
    };
    return data;
};

export const getComments = (
    page = 1,
    limit = 30,
    search = "",
    filter = "all"
) => {
    const data = {
        query: `
            query getComments($page:Int,$limit:Int,$search:String,$filter:String){
                comments(page:$page,limit:$limit,search:$search,filter:$filter)
            }
      `,
        variables: {
            page,
            limit,
            search,
            filter
        }
    };
    return data;
};

export const confirmComment = id => {
    const data = {
        query: `
    mutation confirmComment($id:Int!){
        confirmComment(id:$id)
    }
`,
        variables: {
            id
        }
    };
    return data;
};

export const deleteComment = id => {
    const data = {
        query: `
    mutation deleteComment($id:Int!){
        DComment(id:$id)
    }
`,
        variables: {
            id
        }
    };
    return data;
};

export const replyComment = (id, comment) => {
    const data = {
        query: `
            mutation replyComment($id:Int!,$comment:String!){
                replyComment(id:$id,comment:$comment) {
                    id,
                    comment,
                    status,
                    level
                    user{
                        name,
                        role
                    },
                    article{
                        title
                    },
                    created_at
                }
            }
      `,
        variables: {
            id,
            comment
        }
    };
    return data;
};

export const editComment = (id, comment) => {
    const data = {
        query: `
            mutation editComment($id:Int!,$comment:String!){
                editComment(id:$id,comment:$comment)
            }
      `,
        variables: {
            id,
            comment
        }
    };
    return data;
};


//categories queries
export const getTransactions = (filter) => {
    const data = {
        query: `query getAllTransactions($createdAtFrom:String,$createdAtTo:String){
            transactions(createdAtFrom:$createdAtFrom,createdAtTo:$createdAtTo){
                id,
                user{
                    id,
                    name
                },
                article{
                    id,
                    title
                },
                amount,
                ref_id,
                driver,
                status,
                created_at,
            }
        }`,
        variables: {
            createdAtFrom: filter && filter.createdAtFrom ? filter.createdAtFrom._d : null,
            createdAtTo: filter && filter.createdAtTo ? filter.createdAtTo._d : null,
        }
    };
    return data;
};

export const updateSettings = (inputData) => {
    const data = {
        query: `mutation addEditArticle($siteTitle:String!,$siteTitleFa:String!){
            USetting(siteTitle:$siteTitle,siteTitleFa:$siteTitleFa){
            id
            }
        }`,
        variables: {
            siteTitle: inputData.siteTitle,
            siteTitleFa: inputData.siteTitleFa,
        }
    };
    return data;
};

export const getSettings = () => {
    const data = {
        query: `query getAllSettings{
            settings
        }`,
        variables: {}
    };
    return data;
};
