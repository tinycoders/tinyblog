import axios from "axios";
import Swal from "sweetalert2";

const customAxios = axios.create({
    baseURL: "/graphql"
});

customAxios.interceptors.response.use(
    res => {
        const { data } = res;
        const errors = data.errors;
        if (errors && errors.length > 0) {
            if (errors[0].message === "validation") validationError(errors);
            else if (!errors[0].message.includes("Variable"))
                customError(errors);
            else Swal.fire({ icon: "error", text: "خطایی رخ داد" });
            return Promise.reject();
        }
        return Promise.resolve(res);
    },
    err => {
        const { status } = err.response;
        if (status === 403) {
            localStorage.removeItem("tb_token");
            location.href = "/admin/login";
        } else if (status === 404) {
            location.href = "/not-found";
        }
    }
);

const validationError = errors => {
    const validation = errors[0].extensions.validation;
    let validationMessages = "";
    for (const field in validation) {
        for (const message of validation[field]) {
            validationMessages += message + "</br>";
        }
    }

    Swal.fire({
        icon: "error",
        title: "وای نههههه",
        html: validationMessages,
        confirmButtonText: "باشه"
    });
};

const customError = errors => {
    const message = errors[0].message;
    Swal.fire({ icon: "error", text: message });
};

const setAuthorizationToken = token => {
    customAxios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};
customAxios.setAuthorizationToken = setAuthorizationToken;

export default customAxios;
