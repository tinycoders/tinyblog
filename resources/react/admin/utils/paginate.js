export const paginate = (page, allPages, delta = 2) => {
    const pages = [];
    const pagesWithDots = [];
    let j;

    for (let i = 1; i <= allPages; i++) {
        if (i == 1 || i == allPages || (i - delta <= page && i + delta >= page))
            pages.push(i);
    }

    for (const i of pages) {
        if (j) {
            if (i - j == 2) {
                pagesWithDots.push(j + 1);
            } else if (i - j !== 1) {
                pagesWithDots.push("...");
            }
        }
        pagesWithDots.push(i);
        j = i;
    }
    return pagesWithDots;
};
