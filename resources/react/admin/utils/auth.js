import axios from "./httpRequest";
import jwtDecode from "jwt-decode";
import { loginUser, logOutUser } from "./queries";

const login = async (email, password) => {
    try {
        const { data } = await axios.post("", loginUser(email, password));
        const loginData = data.data.login;
        if (loginData) {
            setToken(loginData.access_token);
            return Promise.resolve(true);
        }
    } catch (e) {}
};

const checkAuth = () => {
    try {
        const data = jwtDecode(getToken());
        if (new Date(data.exp * 1000) < new Date()) return false;
        return true;
    } catch (e) {
        return false;
    }
};

const setToken = token => {
    localStorage.setItem("tb_token", token);
    axios.setAuthorizationToken(token);
};

const getUser = () => {
    if (checkAuth()) {
        const data = jwtDecode(getToken());
        return data.user;
    }
    return false;
};

const getToken = () => {
    try {
        const token = localStorage.getItem("tb_token");
        return token;
    } catch (e) {}
    return "";
};

const logout = async () => {
    try {
        await axios.post("", logOutUser());
        localStorage.removeItem("tb_token");
        axios.setAuthorizationToken("");
        location.href = "/admin/login";
    } catch (e) {}
};

export default {
    login,
    checkAuth,
    getToken,
    getUser,
    logout,
    axios: axios
};
