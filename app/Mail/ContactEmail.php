<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $name = trim($this->data['name']);
        $email = trim($this->data['email']);
        $subject = $this->data['subject'];




        return $this->view('web.email.contact_us')
            /*->from($email,$name)*/
            ->subject($subject)
            ->with([$this->data]);
    }
}
