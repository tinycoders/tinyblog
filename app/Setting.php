<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const SITE_TITLE = 'SITE_TITLE';
    const SITE_TITLE_FA = 'SITE_TITLE_FA';

    protected $guarded = ['id'];
}
