<?php


namespace App\Filters;


class ArticleView extends Filter
{

    public function applyFilter($builder)
    {
        switch (request('article_view')) {
            case 'new':
                return $builder->latest();
            case 'old':
                return $builder->oldest();
            case 'view':
                return $builder->orderBy('view_count', 'DESC');
        }

        return $builder;
    }
}
