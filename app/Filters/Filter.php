<?php


namespace App\Filters;


use Illuminate\Support\Str;

abstract class Filter
{
    public function handle($builder, \Closure $next)
    {
        if (!request()->has($this->getFilterName()))
            return $next($builder);

        return $this->applyFilter($next($builder));
    }

    public abstract function applyFilter($builder);

    protected function getFilterName()
    {
        return Str::snake(class_basename($this));
    }
}
