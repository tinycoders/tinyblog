<?php


namespace App\Filters;


class ArticleTitle extends Filter
{
    public function applyFilter($builder)
    {
        return $builder->where('title', 'LIKE', '%' . request('article_title') . '%')
            ->orWhere('abstract', 'LIKE', '%' . request('article_title') . '%');
    }
}
