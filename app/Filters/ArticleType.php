<?php


namespace App\Filters;


use App\Article;

class ArticleType extends Filter
{

    public function applyFilter($builder)
    {
        switch (request('article_type')) {
            case 'free':
                return $builder->where('is_free', Article::FREE);
            case 'pro':
                return $builder->where('is_free', Article::PREMIUM);
        }

        return $builder;
    }
}
