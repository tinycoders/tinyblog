<?php


namespace App\Filters;


class ArticleCategory extends Filter
{
    public function applyFilter($builder)
    {
        return $builder->whereHas('categories', function ($q) {
            $q->where('article_category.id', request('article_category'));
        });
    }
}
