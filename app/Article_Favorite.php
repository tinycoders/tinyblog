<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article_Favorite extends Model
{
    protected $guarded = ["id"];

    protected $table = "article_favorite";
}
