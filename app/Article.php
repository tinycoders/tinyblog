<?php

namespace App;

use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Article extends Model
{
    use Sluggable;

    const FREE = 1;
    const PREMIUM = 0;

    protected $guarded = ["id"];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userFavorites()
    {
        return $this->belongsToMany(User::class, 'article_favorite');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getCreatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getUpdatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getCreatedAtEnAttribute()
    {
        return Carbon::create($this->attributes["created_at"])->format("Y M d");
    }

    public function getUpdatedAtEnAttribute()
    {
        return Carbon::create($this->attributes["updated_at"])->format("Y M d");
    }

    public function getStatusFaAttribute($data)
    {
        return $this->attributes["status"] ? "منتشر شده" : "منتشر نشده";
    }

    private function convertDate($date)
    {
        $jalali = new Verta($date);
        return $jalali->format("d %B Y");
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function isFree()
    {
        return $this->is_free === static::FREE;
    }

    public function isPaid()
    {
        if (!auth()->check()) {
            return false;
        }
        return !! $this->transactions()->where('user_id',auth()->id())->where('status',Transaction::SUCCESS)->first();

    }
}
