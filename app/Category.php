<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    protected $guarded = ["id"];

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function getCreatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getUpdatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    private function convertDate($date)
    {
        $jalali = new Verta($date);
        return $jalali->format("j M Y");
    }
}
