<?php

namespace App\Console\Commands;

use App\Setting;
use Illuminate\Console\Command;

class GenerateSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Site Settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Setting::query()->truncate();

        Setting::query()->insert([
            'key' => Setting::SITE_TITLE,
            'value' => 'Tiny Blog'
        ]);

        Setting::query()->insert([
            'key' => Setting::SITE_TITLE_FA,
            'value' => 'تاینی بلاگ'
        ]);

    }
}
