<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $langs=['en','fa'];
        $lang=$request->cookie('lang');
        if(in_array($lang,$langs)){
            App()->setLocale($lang);
        }
        return $next($request);
    }
}
