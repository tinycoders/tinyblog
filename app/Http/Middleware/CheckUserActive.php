<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserActive
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $status_code = auth()->user()->status;
            if ($status_code != 1) {
                auth()->logout();
            }
        }
        return $next($request);
    }
}
