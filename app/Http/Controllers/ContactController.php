<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{

    public function getContact()
    {
        return view('web.contact');
    }

    public function saveContact(ContactRequest $request)
    {


        $contact = new Contact;

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->phone = $request->phone;
        $contact->message = $request->message;

        $contact->save();

        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new ContactEmail($request->except('_token')));

        return back()->with('send_contact_status', true);

    }
}
