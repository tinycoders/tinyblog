<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('web.categories', compact('categories'));
    }

    public function getArticlesCategory($slug)
    {
        $slug=htmlspecialchars($slug);

        $category = Category::where('slug', $slug)->first();
        if ($category) {
            $articles = $category->articles()->latest()->paginate(10);
            return view('web.category', [
                'articles' => $articles,
                'category' => $category
            ]);
        }
        return view('web.category', [
            'category' => $category
        ]);

    }


}
