<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Filters\ArticleCategory;
use App\Filters\ArticleTitle;
use App\Filters\ArticleType;
use App\Filters\ArticleView;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\ProSearchRequest;
use App\Http\Requests\SearchRequest;
use App\Mail\ContactUs;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $articles = Article::where('status', '=', 1)->orderBy('id', 'desc')->paginate(10);
        return view('web.index', compact('articles'));
    }

    public function lang()
    {
        $langs = ['en', 'fa'];
        $lang = request('lang');
        if (in_array($lang, $langs)) {
            cookie()->queue(cookie('lang', $lang, 60 * 24 * 24));
        }
        return back();
    }

    public function search()
    {
        $search = htmlspecialchars(request('search'));
        $articles = Article::where('title', 'LIKE', "%{$search}%")->orWhere('abstract', 'LIKE', "%{$search}%")->orderBy('id', 'desc')->paginate(10);
        return view('web.search', compact('articles'));
    }

    public function proSearch()
    {
        $articles = app(Pipeline::class)
            ->send(Article::query())
            ->through([
                ArticleTitle::class,
                ArticleType::class,
                ArticleCategory::class,
                ArticleView::class
            ])->thenReturn()->paginate(10);

        return view('web.search', compact('articles'));

    }

    public function about()
    {
        return view('web.about');
    }


    /*public function contact(ContactRequest $request)
    {
        Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(new ContactUs($request));

        return redirect()->back()->with('send_contact_status', true);
    }*/

    public function getSetting(string $key)
    {
        return Setting::query()->where('key',$key)->first();

    }
}
