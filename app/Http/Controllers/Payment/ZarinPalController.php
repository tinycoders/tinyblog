<?php

namespace App\Http\Controllers\Payment;

use App\Article;
use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class ZarinPalController extends PaymentController
{

    public function request($slug)
    {
        /**
         * @var Article $article
         */
        $article = $this->getArticle($slug);
        $user = auth()->user();

        return Payment::purchase(
            (new Invoice)->amount($article->currency),
            function ($driver, $transactionId) use ($article, $user) {
                $article->transactions()->create([
                    'user_id' => $user->id,
                    'transaction_id' => $transactionId,
                    'amount' => $article->currency,
                ]);
            }
        )->pay()->render();
    }


    public function verify(Request $request)
    {
        $transaction = Transaction::where('transaction_id', $request->Authority)->firstOrFail();

        try {
            $result = Payment::amount($transaction->amount)->transactionId($transaction->transaction_id)->verify();

            $transaction->update([
                'ref_id' => $result->getReferenceId(),
                'driver' => $result->getDriver(),
                'status' => Transaction::SUCCESS
            ]);

            Article::find($transaction->article_id);

            return redirect()->route('payment_result')->with('transaction', $transaction);

        } catch (InvalidPaymentException $exception) {
            /**
             * when payment is not verified , it throw an exception.
             * we can catch the excetion to handle invalid payments.
             * getMessage method, returns a suitable message that can be used in user interface.
             **/
            return redirect()->route('payment_result')->with([
                'transaction' => $transaction,
                'exception' => $exception->getMessage()
            ]);
        }
    }


    public function showResult()
    {
        $transaction = Session::get('transaction');
        if (!$transaction)
            return redirect('/');
        $exception = Session::get('exception');
        Session::forget(['transaction', 'exception']);
        return view('web.verify-payment', compact('transaction', 'exception'));
    }
}
