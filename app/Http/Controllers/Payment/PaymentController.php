<?php

namespace App\Http\Controllers\Payment;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('verified');
    }

    public function getArticle($slug)
    {
        $slug = htmlspecialchars($slug);

        return Article::where('slug', $slug)->where('is_free', Article::PREMIUM)->firstOrFail();
    }


}
