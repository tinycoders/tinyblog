<?php

namespace App\Http\Controllers;

use App\Article;
use App\Article_Favorite;
use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\ProfileRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }

    public function show()
    {
        $user = auth()->user();
        return view('web.profile', compact('user'));
    }


    public function update(ProfileRequest $request)
    {
        auth()->user()->update([
            'name' => $request->get("name"),
            'date_birth' => $request->get('date_birth'),
            'phone' => $request->get('phone'),
            'gender' => $request->get('gender'),
        ]);


        return redirect('/profile')->with('update_profile_status', true);
    }


    public function showFavorite()
    {
        $user = auth()->user();
        $articles = $user->favorites()->latest()->paginate(10);
        //dd($articles);
        return view('web.favorite', compact('articles'));
    }


    public function showTransactions()
    {
        $user = auth()->user();
        $transactions = $user->transactions()->latest()->paginate(10);

        return view('web.transaction', compact('transactions'));
    }


    public function deleteFavorite()
    {
        $article_id = htmlspecialchars(request('id'));
        $article = Article::where('id', $article_id)->first();
        $user_id= auth()->user()->id;

        if($article){
            $favorite = Article_Favorite::where('article_id',$article->id)->where('user_id',$user_id)->firstOrFail();
            if($favorite){
                $favorite->delete();
                return redirect()->back()->with('delete_favorite_status', true);
            }

        }
        return redirect()->back();
    }


    public function addFavorite()
    {

        if (auth()->check()) {
            $article_id = htmlspecialchars(request('article'));
            $user_id = auth()->user()->id;
            $article = Article::findOrFail($article_id)->first();

            if ($article) {
                $isExists = Article_Favorite::where('article_id', $article_id)->where('user_id', $user_id)->count();

                if ($isExists == 0) {
                    $favorite = Article_Favorite::create([
                        'article_id' => $article_id,
                        'user_id' => $user_id
                    ]);

                    if ($favorite) {
                        return redirect(route('show_favorite'));
                    }
                }
                return redirect()->back();

            }
        }

        //return redirect(route('show_favorite'));
    }

    public function addComment(CommentRequest $request)
    {
        if (auth()->check()) {
            $user_id = auth()->user()->id;
            $article_id = $request->get('article');
            $message = $request->get('message');

            $comment = Comment::create([
                'article_id' => $article_id,
                'user_id' => $user_id,
                'comment' => $message,
                'level' => '0',
            ]);

            if ($comment) {
                return redirect()->back()->with('add_favorite_status' , true);
            }



        }
        return redirect()->back();
    }



}
