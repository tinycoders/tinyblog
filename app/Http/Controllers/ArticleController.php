<?php

namespace App\Http\Controllers;

use App\Article;
use App\Transaction;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ArticleController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Application|Factory|RedirectResponse|View
     */

    public function show($slug)
    {
        $slug = htmlspecialchars($slug);

        $article = Article::where('slug', $slug)->firstOrFail();

        if (!$article->isFree() && !$this->checkPremiumArticle($article))
            return redirect()->route('request_payment', ['slug' => $article->slug]);

        $article->update([
            'view_count' => ($article->view_count) + 1
        ]);

        return view('web.post', compact('article'));
    }

    public function downloadPDF($slug)
    {
        $slug = htmlspecialchars($slug);

        $article = Article::where('slug', $slug)->firstOrFail();

        if (!$article->isFree() && !$this->checkPremiumArticle($article))
            return redirect()->route('request_payment', ['slug' => $article->slug]);

        $article->increment('download_count',1);

        $pdf = PDF::loadView('web.layouts.pdf', compact('article'));

        $pdf->SetProtection([], '', '12345678',128);
        return $pdf->download("{$slug}.pdf");

    }

    public function checkPremiumArticle(Article $article)
    {
        return !!$article->transactions()->where('user_id', auth()->id())->where('status', Transaction::SUCCESS)->first();
    }
}
