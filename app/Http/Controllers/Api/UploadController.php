<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    protected function upload($folder)
    {
        $file = request()->file("file");
        $fileName = Str::random(50) . ".{$file->getClientOriginalExtension()}";
        $fullFilePath = "{$folder}/{$fileName}";
        $uploadedFile = $file->move(public_path($folder) . "/", $fileName);
        if ($uploadedFile) {
            return $fullFilePath;
        }
        return false;
    }

    public function gatAllImages()
    {
        $rootFolder = "/uploads/images";
        $files = scandir(public_path($rootFolder));
        $allFiles = [];
        for ($i = 2; $i < count($files); $i++) {
            $allFiles[] = ["url" => "{$rootFolder}/{$files[$i]}", "name" => "{$files[$i]}"];
        }
        return $allFiles;
    }

    public function deleteImage()
    {
        $imagePath = public_path(request("src"));
        if (file_exists($imagePath))
            unlink($imagePath);
    }
    public function uploadFile()
    {
        $link = $this->upload("/uploads/files");
        if ($link) {
            return ["link" => $link];
        }
        return ["success" => false];
    }

    public function uploadImage()
    {
        $link = $this->upload("/uploads/images");
        if ($link) {
            return ["link" => $link];
        }
        return ["success" => false];
    }

    public function uploadVideo()
    {
        $link = $this->upload("/uploads/videos");
        if ($link) {
            return ["link" => $link];
        }
        return ["success" => false];
    }
}
