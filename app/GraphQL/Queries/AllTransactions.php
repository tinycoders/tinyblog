<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Transaction;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class AllTransactions extends Query
{
    protected $attributes = [
        'name' => 'allTransactions',
        'description' => 'A query to get all transactions'
    ];

    public function type(): Type
    {
        return Type::listOf(\GraphQL::type('Transaction'));
    }

    public function args(): array
    {
        return [
            "createdAtFrom" => ["Type" => Type::string()],
            "createdAtTo" => ["Type" => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $createdAtFrom = isset($args['createdAtFrom']) && !is_null($args['createdAtFrom']) ? Carbon::parse($args['createdAtFrom']) : null;
        $createdAtTo = isset($args['createdAtTo']) && !is_null($args['createdAtTo']) ? Carbon::parse($args['createdAtTo']) : null;

        $transactions = Transaction::query()
            ->when(!!$createdAtFrom, function ($q) use ($createdAtFrom) {
                $q->whereDate('created_at', '>=', $createdAtFrom->toDateString());
            })
            ->when(!!$createdAtTo, function ($q) use ($createdAtTo) {
                $q->whereDate('created_at', '<=', $createdAtTo->toDateString());
            })->get();

        return $transactions;
    }
}
