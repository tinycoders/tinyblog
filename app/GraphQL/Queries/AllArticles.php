<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Article;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class AllArticles extends Query
{
    protected $attributes = [
        'name' => 'allArticles',
        'description' => 'A query for getting all articles'
    ];

    public function type(): Type
    {
        return \GraphQL::paginate("Article");
    }

    public function args(): array
    {
        return [
            "page" => ["Type" => Type::int()],
            "limit" => ["Type" => Type::int()],
            "search" => ["Type" => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $page = $args["page"] ?? 1;
        $limit = $args["limit"] ?? 10;
        $search = $args["search"];
        if (!$search) {
            return Article::orderBy("id", "desc")->paginate($limit, ["*"], "page", $page);
        } else {
            return Article::where("title", "like", "%{$search}%")->orWhere("body", "like", "%{$search}%")->orderBy("id", "desc")->paginate($limit, ["*"], "page", $page);
        }
    }
}
