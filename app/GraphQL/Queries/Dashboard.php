<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Article;
use App\Category;
use App\Comment;
use App\User;
use Closure;
use DB;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Hekmatinasser\Verta\Verta;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class Dashboard extends Query
{
    protected $attributes = [
        'name' => 'dashboard',
        'description' => 'A query for dashboard'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Dashboard");
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $usersCount = User::all()->count();
        $articlesCount = Article::all()->count();
        $commentsCount = Comment::all()->count();
        $categoriesCount = Category::all()->count();

        $users = User::orderBy("id", "desc")->limit(5)->get();
        $articles = Article::orderBy("id", "desc")->limit(5)->get();
        $comments = Comment::orderBy("id", "desc")->limit(5)->get();

        //SELECT COUNT(id) as count,DATE(created_at) as month FROM articles GROUP BY (month) ORDER BY(id) DESC LIMIT 5 
        $articlesChart = DB::table("articles")->select(DB::raw("COUNT(id) as count,DATE(created_at) as date"))->groupBy("date")->limit(5)->get();

        //SELECT COUNT(id) as count,DATE(created_at) as month FROM articles GROUP BY (month) ORDER BY(id) DESC LIMIT 5 
        $usersChart = DB::table("users")->select(DB::raw("COUNT(id) as count,DATE(created_at) as date"))->groupBy("date")->limit(5)->get();

        $articleValues = [];
        $articleLabels = [];
        foreach ($articlesChart as $article) {
            $articleValues[] = $article->count;
            $articleLabels[] = (new Verta($article->date))->format("Y/m/d");
        }

        $userValues = [];
        $userLabels = [];
        foreach ($usersChart as $user) {
            $userValues[] = $user->count;
            $userLabels[] = (new Verta($user->date))->format("Y/m/d");
        }
        return [
            "usersCount" => $usersCount,
            "articlesCount" => $articlesCount,
            "commentsCount" => $commentsCount,
            "categoriesCount" => $categoriesCount,
            "usersChart" => ["values" => $userValues, "labels" => $userLabels],
            "articlesChart" => ["values" => $articleValues, "labels" => $articleLabels],
            "users" => $users,
            "articles" => $articles,
            "comments" => $comments,
        ];
    }
}
