<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Article;
use App\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UsersArticles extends Query
{
    protected $attributes = [
        'name' => 'usersArticles',
        'description' => 'A query gor getting all users and articles'
    ];

    public function type(): Type
    {
        return \GraphQL::type("UserArticle");
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return [
            "users" => User::all(),
            "articles" => Article::all()
        ];
    }
}
