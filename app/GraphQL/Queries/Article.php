<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Article as AppArticle;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class Article extends Query
{
    protected $attributes = [
        'name' => 'article',
        'description' => 'A query for getting a article'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Article");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $article = AppArticle::find($args["id"]);
        if ($article)
            return $article;
        throw new Error("مقاله مورد نظر یافت نشد");
    }
}
