<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Comment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class AllComments extends Query
{
    protected $attributes = [
        'name' => 'allComments',
        'description' => 'A query for getting all comments'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            "page" => ["type" => Type::int()],
            "limit" => ["type" => Type::int()],
            "search" => ["type" => Type::string()],
            "filter" => ["type" => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $page = $args["page"] ?? 1;
        $limit = $args["limit"] ?? 10;
        $filter = $args["filter"] ?? "all";

        $comments = Comment::where("comment", "LIKE", "%{$args['search']}%");
        if (!$args["search"] && $filter === "all") {
            $comments = $comments->where("level", 0);
        }
        if ($filter === "confirmed")
            $comments = $comments->where("status", 1);
        else if ($filter === "not-confirmed")
            $comments = $comments->where("status", 0);
        $comments = $comments->with("user:id,name,role", "article:id,title")->orderBy("id")->paginate($limit, ["*"], "page", $page);
        if (!$args["search"] && $filter === "all") {
            $comments = $this->getComments($comments);
        }
        return json_encode($comments);
    }

    protected function getComments($comments)
    {
        for ($i = 0; $i < count($comments); $i++) {
            $comments[$i]["inner_comment"] = $this->getInnerComments($comments[$i]->id);
        }
        return $comments;
    }

    protected function getInnerComments($id)
    {
        $comments = Comment::where("level", $id)->with("user:id,name,role")->get();
        for ($i = 0; $i < count($comments); $i++) {
            $comments[$i]["inner_comment"] = $this->getInnerComments($comments[$i]->id);
        }
        return $comments;
    }
}
