<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\User as AppUser;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class User extends Query
{
    protected $attributes = [
        'name' => 'user',
        'description' => 'A query for getting a user'
    ];

    public function type(): Type
    {
        return \GraphQL::type("User");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = AppUser::find($args["id"]);
        if ($user)
            return $user;
        throw new Error("کاربر مورد نظر یافت نشد");
    }
}
