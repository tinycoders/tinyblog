<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Category;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class AllCategories extends Query
{
    protected $attributes = [
        'name' => 'allCategories',
        'description' => 'A query for getting all categories'
    ];

    public function type(): Type
    {
        return Type::listOf(\GraphQL::type("Category"));
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return Category::all();
    }
}
