<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class AllUsers extends Query
{
    protected $attributes = [
        'name' => 'allUsers',
        'description' => 'A query for getting all users'
    ];

    public function type(): Type
    {
        return \GraphQL::paginate("User");
    }

    public function args(): array
    {
        return [
            "page" => ["Type" => Type::int()],
            "limit" => ["Type" => Type::int()],
            "search" => ["Type" => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $page = $args["page"] ?? 1;
        $limit = $args["limit"] ?? 10;
        $search = $args["search"];
        if (!$search) {
            return User::where("id", "<>", auth("api")->user()->id)->orderBy("id", "desc")->paginate($limit, ["*"], "page", $page);
        } else {
            return User::where("name", "like", "%{$search}%")->orWhere("email", "like", "%{$search}%")->where("id", "<>", auth("api")->user()->id)->orderBy("id", "desc")->paginate($limit, ["*"], "page", $page);
        }
    }
}
