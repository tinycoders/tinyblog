<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Setting;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class Settings extends Query
{
    protected $attributes = [
        'name' => 'settings',
        'description' => 'A query'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $settingsData = [];
        $settings = Setting::query()->get();

        foreach ($settings as $setting) {
            $key = Str::camel(Str::lower($setting->key));
            $settingsData[$key] = $setting->value;
        }

        return json_encode($settingsData);
    }
}
