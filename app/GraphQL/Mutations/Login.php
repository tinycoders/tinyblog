<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\User;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Hash;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class Login extends Mutation
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'A mutation for admin user to login'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Token");
    }

    public function args(): array
    {
        return [
            "email" => ["type" => Type::string()],
            "password" => ["type" => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            "email" => ["required", "email"],
            "password" => ["required", "min:8", "max:20"],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $credentials = ["email" => $args["email"], "password" => $args["password"]];
        $user = User::where("email", $args["email"])->first();

        if (!$user || $user->role !== 1) {
            throw new Error("نام کاربری یا رمز عبور نادرست است");
        }

        if ($token = auth("api")->claims(['user' => ["name" => $user->name, "email" => $user->email, "avatar" => $user->avatar]])->attempt($credentials)) {
            return [
                "access_token" => $token,
                "token_type" => "bearer",
                "expires_in" => auth("api")->factory()->getTTL() * 60 * 24 * 30
            ];
        }
        throw new Error("ورود با شکست مواجه شد");
    }
}
