<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Comments;

use App\Comment;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class ConfirmComment extends Mutation
{
    protected $attributes = [
        'name' => 'confirmComment',
        'description' => 'A mutation for comment confirmation'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return ["id" => ["type" => Type::nonNull(Type::int())]];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $comment = Comment::find($args["id"]);
        if ($comment) {
            $newStatus = !$comment->status;
            $comment->update([
                "status" => $newStatus
            ]);
            return $newStatus;
        }
        throw new Error("کامنت مورد نطر یافت نشد");
    }
}
