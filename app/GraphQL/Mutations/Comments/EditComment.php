<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Comments;

use App\Comment;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class EditComment extends Mutation
{
    protected $attributes = [
        'name' => 'editComment',
        'description' => 'A mutation for editing a comment'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())],
            "comment" => ["type" => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $comment = Comment::find($args["id"]);
        if ($comment) {
            $comment->update([
                "comment" => $args["comment"]
            ]);
            return $comment->comment;
        }
        throw new Error("کامنت مورد نظر یافت نشد");
    }
}
