<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Comments;

use App\Comment;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class ReplyComment extends Mutation
{
    protected $attributes = [
        'name' => 'replyComment',
        'description' => 'A mutation for reply to a comment'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Comment");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())],
            "comment" => ["type" => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $comment = Comment::find($args["id"]);
        if ($comment) {
            return Comment::create([
                "user_id" => auth("api")->user()->id,
                "article_id" => $comment->article()->first()->id,
                "comment" => $args["comment"],
                "level" => $comment->id,
                "status" => 1
            ]);
        }
        throw new Error("کامنت مورد نظر یافت نشد");
    }
}
