<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Comments;

use App\Comment;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class DComment extends Mutation
{
    protected $attributes = [
        'name' => 'dComment',
        'description' => 'A mutation for delete a comment'
    ];

    public function type(): Type
    {
        return  Type::boolean();
    }

    public function args(): array
    {
        return ["id" => ["type" => Type::nonNull(Type::int())]];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $comment = Comment::find($args["id"]);
        if ($comment) {
            $this->deleteInnerComments($comment->id);
            return $comment->delete();
        }
        throw new Error("کامنت مورد نظر یافت نشد");
    }

    protected function deleteInnerComments($id)
    {
        $comments = Comment::where("level", $id)->get();
        foreach ($comments as $comment) {
            $this->deleteInnerComments($comment->id);
            $comment->delete();
        }
        return;
    }
}
