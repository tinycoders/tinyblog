<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Users;

use App\User;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AEUser extends Mutation
{
    protected $attributes = [
        'name' => 'aEUser',
        'description' => 'A mutation for add and edit a user'
    ];

    public function type(): Type
    {
        return \GraphQL::type("User");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "userRole" => ["type" => Type::nonNull(Type::int())],
            "userAvatar" => ["type" => Type::string()],
            "userFullName" => ["type" => Type::nonNull(Type::string())],
            "userGender" => ["type" => Type::nonNull(Type::int())],
            "userEmail" => ["type" => Type::nonNull(Type::string())],
            "userPassword" => ["type" => Type::string()],
            "userPassword_confirmation" => ["type" => Type::string()],
            "userPhone" => ["type" => Type::string()],
            "userDateBirth" => ["type" => Type::string()],
            "userStatus" => ["type" => Type::nonNull(Type::boolean())]
        ];
    }

    protected function rules(array $args = []): array
    {
        $rules = [
            "userRole" => ["required"],
            "userFullName" => ["required", "string"],
            "userGender" => ["required"],
            "userEmail" => ["required", "email", "unique:users,email"],
            "userPassword" => ["required", "string", "min:8", "max:20", "confirmed"],
            "userStatus" => ["required", "boolean"]
        ];
        if (isset($args["id"])) {
            $rules["userEmail"] = ["required", "email"];
        }
        if (isset($args["id"]) && !isset($args["userPassword"])) {
            unset($rules["userPassword"]);
        }

        if (isset($args["userPhone"])) {
            $rules["userPhone"] = ["numeric"];
        }

        if (isset($args["userDateBirth"])) {
            $rules["userDateBirth"] = ["date"];
        }

        return $rules;
    }


    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args["id"])) {
            $user = User::find($args["id"]);
            $user->update($this->getFields($args));
            return $user;
        }
        $user = User::create($this->getFields($args));
        return $user;
    }

    protected function getFields($args)
    {
        $avatar = "";
        if (isset($args["userAvatar"])) {
            $avatar = $args["userAvatar"];
        } else {
            if ($args["userGender"] === 1) {
                $avatar = "/images/female.png";
            } else {
                $avatar = "/images/male.png";
            }
        }
        $fields = [
            "role" => $args["userRole"],
            "avatar" => $avatar,
            "name" => $args["userFullName"],
            "gender" => $args["userGender"],
            "email" => $args["userEmail"],
            "password" => bcrypt($args["userPassword"]),
            "status" => $args["userStatus"]
        ];

        if (isset($args["id"]) && !isset($args["userPassword"])) {
            unset($fields["password"]);
        }

        if (isset($args["userDateBirth"])) {
            $fields["date_birth"] = Carbon::create($args["userDateBirth"])->format("Y-m-d");
        }
        if (isset($args["userPhone"])) {
            $fields["phone"] = $args["userPhone"];
        }
        return $fields;
    }
}
