<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Users;

use App\User;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class DUser extends Mutation
{
    protected $attributes = [
        'name' => 'dUser',
        'description' => 'A mutation for delete a user'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::find($args["id"]);
        if ($user)
            return $user->delete();
        throw new Error("کاربر مورد نظر یافت نشد");
    }
}
