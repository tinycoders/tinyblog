<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Categories;

use App\Category;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AECategory extends Mutation
{
    protected $attributes = [
        'name' => 'aECategory',
        'description' => 'A mutation for create and edit a category'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Category");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "categoryName" => ["type" => Type::nonNull(Type::string())],
            "categoryNameFa" => ["type" => Type::nonNull(Type::string())],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'categoryName' => ['required', 'string', Rule::unique('categories', 'name')->ignore($args['id'] ?? null)],
            'categoryNameFa' => ['required', 'string', Rule::unique('categories', 'name_fa')->ignore($args['id'] ?? null)],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args["id"])) {
            $category = Category::find($args["id"]);
            if ($category) {
                $category->slug = null;
                $category->update([
                    "name" => $args["categoryName"],
                    "name_fa" => $args["categoryNameFa"],
                ]);
                return $category;
            }
            throw new Error("دسته بندی مورد نظر یافت نشد");
        }
        return Category::create([
            "name" => $args["categoryName"],
            "name_fa" => $args["categoryNameFa"],
        ]);
    }
}
