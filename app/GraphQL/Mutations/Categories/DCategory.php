<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Categories;

use App\Category;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class DCategory extends Mutation
{
    protected $attributes = [
        'name' => 'dCategory',
        'description' => 'A mutation for delete a category'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $category = Category::find($args["id"]);
        if ($category)
            return $category->delete();
        throw new Error("دسته بندی مورد نظر یافت نشد");
    }
}
