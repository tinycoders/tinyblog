<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Articles;

use App\Article;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AEArticle extends Mutation
{
    protected $attributes = [
        'name' => 'aEArticle',
        'description' => 'A mutation for create and edit articles'
    ];

    public function type(): Type
    {
        return \GraphQL::type("Article");
    }

    public function args(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "articleTitle" => ["type" => Type::nonNull(Type::string())],
            "articleCategory" => ["type" => Type::nonNull(Type::listOf(Type::int()))],
            "articleAbstract" => ["type" => Type::nonNull(Type::string())],
            "articleBody" => ["type" => Type::nonNull(Type::string())],
            "articleCover" => ["type" => Type::nonNull(Type::string())],
            "articleIsReleased" => ["type" => Type::nonNull(Type::boolean())],
            "articleIsFree" => ["type" => Type::boolean()],
            "articlePrice" => ["type" => Type::int()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'articleTitle' => ['required', 'string'],
            'articleCategory' => ['required', 'array', 'min:1'],
            'articleBody' => ['required', 'string'],
            'articleAbstract' => ['required', 'string'],
            'articleCover' => ['required', 'string'],
            "articlePrice" => ['nullable', 'numeric', 'min:500'],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args["id"])) {
            $id = $args["id"];
            $article = Article::find($id);
            if ($article) {
                $article->slug = null;
                $article->update([
                    "title" => $args["articleTitle"],
                    "abstract" => $args["articleAbstract"],
                    "body" => $args["articleBody"],
                    "cover" => $args["articleCover"],
                    "status" => $args["articleIsReleased"],
                    "is_free" => $args["articleIsFree"],
                    "currency" => !$args["articleIsFree"] ? $args["articlePrice"] : 0,
                ]);
                $article->categories()->sync($args["articleCategory"]);
                return $article;
            }
            throw new Error("مقاله مورد نظر یافت نشد");
        }

        $article = Article::create([
            "user_id" => auth("api")->user()->id,
            "title" => $args["articleTitle"],
            "abstract" => $args["articleAbstract"],
            "body" => $args["articleBody"],
            "cover" => $args["articleCover"],
            "status" => $args["articleIsReleased"],
            "is_free" => $args["articleIsFree"],
            "currency" => !$args["articleIsFree"] ? $args["articlePrice"] : 0,
        ]);
        $article->categories()->sync($args["articleCategory"]);
        return $article;
    }
}
