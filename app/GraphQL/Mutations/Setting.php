<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Setting as SettingModel;
use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class Setting extends Mutation
{
    protected $attributes = [
        'name' => 'setting',
        'description' => 'update site settings'
    ];

    public function type(): Type
    {
        return Type::listOf(\GraphQL::type("Setting"));
    }

    public function args(): array
    {
        return [
            "siteTitle" => ["type" => Type::nonNull(Type::string())],
            "siteTitleFa" => ["type" => Type::nonNull(Type::string())],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'siteTitle' => ['required', 'string'],
            'siteTitleFa' => ['required', 'string']
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $settingKeys = array_keys($args);

        foreach ($settingKeys as $key) {
            SettingModel::query()->updateOrCreate(
                ['key' => Str::upper(Str::snake($key))],
                ['value' => $args[$key]]
            );
        }
        return SettingModel::query()->get();
    }
}
