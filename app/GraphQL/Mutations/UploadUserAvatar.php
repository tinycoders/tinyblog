<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use Closure;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UploadUserAvatar extends Mutation
{
    protected $attributes = [
        'name' => 'uploadUserAvatar',
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            "file" => ["type" => Type::nonNull(\GraphQL::type("Upload"))]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $file = $args["file"];
        $fileName = Str::random(50) . "." . $file->getClientOriginalExtension();
        $filePath = "/uploads/avatars";
        $savedFile = $file->move(public_path($filePath) . "/", $fileName);
        $fullPath = "{$filePath}/{$fileName}";
        if ($savedFile)
            return $fullPath;
        throw new Error("آپلود آواتار با شکست مواجه شد");
    }
}
