<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Dashboard extends GraphQLType
{
    protected $attributes = [
        'name' => 'Dashboard',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            "usersCount" => ["type" => Type::int()],
            "articlesCount" => ["type" => Type::int()],
            "commentsCount" => ["type" => Type::int()],
            "categoriesCount" => ["type" => Type::int()],
            "usersChart" => ["type" => \GraphQL::type("Chart")],
            "articlesChart" => ["type" => \GraphQL::type("Chart")],
            "users" => ["type" => Type::listOf(\GraphQL::type("User"))],
            "articles" => ["type" => Type::listOf(\GraphQL::type("Article"))],
            "comments" => ["type" => Type::listOf(\GraphQL::type("Comment"))],
        ];
    }
}
