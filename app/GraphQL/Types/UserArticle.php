<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserArticle extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserArticle',
        'description' => 'A type for user article'
    ];

    public function fields(): array
    {
        return [
            "users" => ["type" => Type::listOf(\GraphQL::type("User"))],
            "articles" => ["type" => Type::listOf(\GraphQL::type("Article"))],
        ];
    }
}
