<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Token extends GraphQLType
{
    protected $attributes = [
        'name' => 'Token',
        'description' => 'A type for jwt token'
    ];

    public function fields(): array
    {
        return [
            "access_token" => ["type" => Type::string()],
            "token_type" => ["type" => Type::string()],
            "expires_in" => ["type" => Type::int()]
        ];
    }
}
