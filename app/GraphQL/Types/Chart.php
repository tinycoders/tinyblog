<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Chart extends GraphQLType
{
    protected $attributes = [
        'name' => 'Chart',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            "values" => ["type" => Type::listOf(Type::int())],
            "labels" => ["type" => Type::listOf(Type::string())]
        ];
    }
}
