<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class User extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'A type for Users'
    ];

    public function fields(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "role" => ["type" => Type::int()],
            "role_fa" => ["type" => Type::string()],
            "avatar" => ["type" => Type::string()],
            "articles" => ["type" => Type::listOf(\GraphQL::type("Article"))],
            "comments" => ["type" => Type::listOf(\GraphQL::type("Comment"))],
            "name" => ["type" => Type::string()],
            "gender" => ["type" => Type::int()],
            "phone" => ["type" => Type::string()],
            "date_birth" => ["type" => Type::string()],
            "email" => ["type" => Type::string()],
            "status" => ["type" => Type::boolean()],
            "status_fa" => ["type" => Type::string()],
            "created_at" => ["type" => Type::string()],
            "updated_at" => ["type" => Type::string()],
        ];
    }
}
