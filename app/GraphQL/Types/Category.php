<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Category extends GraphQLType
{
    protected $attributes = [
        'name' => 'Category',
        'description' => 'A type for categories'
    ];

    public function fields(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "name" => ["type" => Type::string()],
            "name_fa" => ["type" => Type::string()],
            "slug" => ["type" => Type::string()],
            "articles" => ["type" => Type::listOf(\GraphQL::type("Article"))],
            "created_at" => ["type" => Type::string()],
            "updated_at" => ["type" => Type::string()],
        ];
    }
}
