<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Transaction extends GraphQLType
{
    protected $attributes = [
        'name' => 'Transaction',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "user" => ["type" => \GraphQL::type("User")],
            "article" => ["type" => \GraphQL::type("Article")],
            "amount" => ["type" => Type::int()],
            "transaction_id" => ["type" => Type::int()],
            "ref_id" => ["type" => Type::int()],
            "driver" => ["type" => Type::string()],
            "status" => ["type" => Type::int()],
            "created_at" => ["type" => Type::string()],
            "updated_at" => ["type" => Type::string()],
        ];
    }
}
