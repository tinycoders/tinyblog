<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Article extends GraphQLType
{
    protected $attributes = [
        'name' => 'Article',
        'description' => 'A type for Articles'
    ];

    public function fields(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "user" => ["type" => \GraphQL::type("User")],
            "title" => ["type" => Type::string()],
            "slug" => ["type" => Type::string()],
            "cover" => ["type" => Type::string()],
            "body" => ["type" => Type::string()],
            "abstract" => ["type" => Type::string()],
            "is_free" => ["type" => Type::boolean()],
            "currency" => ["type" => Type::int()],
            "status" => ["type" => Type::boolean()],
            "status_fa" => ["type" => Type::string()],
            "categories" => ["type" => Type::listOf(\GraphQL::type("Category"))],
            "comments" => ["type" => Type::listOf(\GraphQL::type("Comment"))],
            "created_at" => ["type" => Type::string()],
            "updated_at" => ["type" => Type::string()],
        ];
    }
}
