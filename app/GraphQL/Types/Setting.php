<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Setting extends GraphQLType
{
    protected $attributes = [
        'name' => 'Setting',
        'description' => 'A type for settings model'
    ];

    public function fields(): array
    {
        return [
            "id" => ["type" => Type::int()],
            "key" => ["type" => Type::string()],
            "value" => ["type" => Type::string()],
            "created_at" => ["type" => Type::string()],
            "updated_at" => ["type" => Type::string()],
        ];
    }
}
