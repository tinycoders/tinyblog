<?php

namespace App;

use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $guarded = ['id'];

    const SUCCESS = 1;
    const FAIL = 0;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function getCreatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getUpdatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getCreatedAtEnAttribute()
    {
        return Carbon::create($this->attributes["created_at"])->format("Y M d H:i:s");
    }

    public function getUpdatedAtEnAttribute()
    {
        return Carbon::create($this->attributes["updated_at"])->format("Y M d H:i:s");
    }

    public function getStatusFaAttribute($data)
    {
        return $this->attributes["status"] ? "منتشر شده" : "منتشر نشده";
    }

    private function convertDate($date)
    {
        $jalali = new Verta($date);
        return $jalali->format(" H:i:s - d %B Y");
    }
}
