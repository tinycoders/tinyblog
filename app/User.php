<?php

namespace App;

use Gravatar;
use Hekmatinasser\Verta\Verta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Article::class, 'article_favorite');
    }

    public function getCreatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getUpdatedAtAttribute($data)
    {
        return $this->convertDate($data);
    }

    public function getRoleFaAttribute($data)
    {
        return $this->attributes["role"] === 1 ? "ادمین" : "کاربر";
    }

    public function getStatusFaAttribute($data)
    {
        return $this->attributes["status"] ? "فعال" : "غیر فعال";
    }

    // public function getAvatarAttribute($data)
    // {
    //     $avatar = $this->attributes["avatar"];
    //     if ($avatar == "/images/male.png" || $avatar == "/images/female.png" || $avatar == "/images/other.png") {
    //         if (Gravatar::exists($this->attributes["email"])) {
    //             $avatar = Gravatar::get($this->attributes["email"]);
    //         }
    //     }
    //     return $avatar;
    // }

    private function convertDate($date)
    {
        $jalali = new Verta($date);
        return $jalali->format("d %B Y");
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

}
